import AppState from "./context/Application/AppState";
import Page from "./Page";


const MacepWebPage = () =>{
    return(
        <>
            <AppState>
                <Page />
            </AppState>
        </>
    )
};

export default MacepWebPage;