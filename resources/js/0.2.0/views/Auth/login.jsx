import { useContext, useEffect } from "react";
import { Redirect } from "react-router";
import AppContext from "../../context/Application/AppContext";

const Login = props => {
    const {user,setUser} = useContext(AppContext);
    useEffect(()=>{
        document.title="Iniciar sesión | MACEP tu tienda en línea de confianza"
    },[]);
    return(
        <>
            {user.ready?
                <>
                    {user.auth?
                    <Redirect to="/" />
                    :
                    <>
                    Hola mundo
                    </>
                    }
                </>
                :null
            }
        </>
    )
};

export default Login;