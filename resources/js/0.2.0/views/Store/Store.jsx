import { useEffect } from "react"
import { BrowserRouter as Router, Route, Switch, useLocation } from "react-router-dom"
import Dashboard from "../../components/Account/Dashboard";
import Login from "../../components/Authenticate/Login/Login";
import Logout from "../../components/Authenticate/Logout/Logout";
import CartComponent from "../../components/CartComponent/CartComponent";
import CheckoutComponent from "../../components/Checkout/Checkout";
import Header from "../../components/Header/Header"
import VwCategories from "../../components/Views/Categories/VwCategories";
import Home from "./Home";
import Product from "./Product";
import Address from "../../components/Check/Checkout"
import Searchbox from "../../components/Header/Searchbox";
import VwSearch from "../../components/Views/Search/VwSearch";
import vwBrands from "../Brands/vwBrands";
import Contact from "../../components/Contact";
import pageErrors from "../../components/htmlComponents/pageErrors"

export default ({children})=>{
    const location=useLocation();
    useEffect(()=>{
        
    })
    return(
        <>
        <Header></Header>
            <Switch>              
                <Route exact path="/departament/:departament_name" component={VwCategories} />
                <Route exact path="/producto/detalles/:product_id-:product_slug" component={Product} />
                <Route exact path="/cart" component={CartComponent} />
                <Route exact path="/carrito" component={CartComponent} />
                <Route exact path="/checkout/:id" component={CheckoutComponent}/>
                <Route path="/mi-cuenta" component={Dashboard} />
                <Route exact path="/logout" component={Logout} />
                <Route exact path="/" component={Home} />
                <Route exact path="/check/:id" component={Address} />
                <Route exact path="/search/:name" component={VwSearch}/>
                <Route exact path="/marca/:name" component={vwBrands}/>
                <Route exact path="/contacto" component={Contact}/>
                <Route component={pageErrors}/>
            </Switch>
        </>
    )
}