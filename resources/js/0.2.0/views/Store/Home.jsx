import BrandsBar from "../../components/BrandsBar/BrandsBar"
import CatBar from "../../components/OfferBar/catBar"
import ProductsBar from "../../components/Products/ProductsBar"
import Slider from "../../components/Slider/Slider"
import Header from "../../components/Header/Header"
import PopUp from "../../components/PopUp/PopUp"
import AppContext from "../../context/Application/AppContext";
import Footer from "../../components/Footer/footer"
import {useContext} from "react";

export default () => {

    const {user} = useContext(AppContext)

    return(
        <>
            {/* {!user.auth && <PopUp></PopUp>} */}
            <Slider />
            <CatBar />
            <ProductsBar type="OFFERS" />
            <BrandsBar />
            <Footer/>
        </>
    )
}