import axios from "axios";
import { useContext, useEffect, useState } from "react"
import { useHistory } from "react-router";
import moment from 'moment';
import slug from "../../../slug";
import InputNumber from "../../components/htmlComponents/InputNumber";
import Wrapper from "../../components/Utils/Wrapper";
import AppContext from "../../context/Application/AppContext";
import styledPrices from "../../../Utils/styledPrices";
import ProductDescription from "../../components/Products/ProductDescription";
import SmallProductsBar from "../../components/Products/SmallProductsBar";


export default ({match})=>{
    const defaultProduct={
        name:'',
        brand_name:'',
        filename:'',
        net_sale_price:'',
        sku:'',
        upc:'',
        model:'',
        stock:1,
        description:'',
        full_description:'',
    };
    const [prod, setProduct] = useState({...defaultProduct});
    const [productId, setProductId] = useState(0);
    const [productSlug, setProductSlug] = useState('');
    const [productImages, setProductImages] = useState([]);
    const [productImage, setProductImage] = useState('default.jpg');
    const [productDescription, setProductDescription] = useState([]);
    const [loading, setLoading] = useState(true);
    const [startShippingDate, setStartShippingDate] = useState(moment().locale('es').add(1,'day'));
    const [endShippingDate, setEndShippingDate] = useState(moment().locale('es').add(4,'day'));
    const [quantity, setQuantity] = useState(1);
    const history=useHistory();

    const {addToCart}=useContext(AppContext);

    const loadData=()=>{
        const {product_id,product_slug}=match.params;
        setProductId(0);
        setProduct({...defaultProduct});
        setProductImages([]);
        setProductImage('default.jpg');
        setProductDescription([]);
        axios.post('product/details',{product_id,product_slug})
        .then(response=>{
            if(product_slug!==response.data.php_slug){
                history.push(`/producto/detalles/${product_id}-${response.data.php_slug}`);
                return;
            }
            console.log(response.data.product);
            setProductId(product_id);
            setProductSlug(product_slug);
            const{product,images}=response.data;
            //document.title=product.name+' | MACEP - Tienda en línea';
            setProduct(product);
            setProductImages(images);
            setProductImage(product.filename);
            setProductDescription(product.description.split('\n'));
            setLoading(false);
            
            
            
        })
        .catch(error=>{
            console.log(error);
        })
    }
    useEffect(()=>{
        loadData();
    },[match.params.product_id]);
    return(
        <>
            <Wrapper>
                <div className="w-full flex flex-wrap lg:border lg:border-t-0 bg-white pb-4">
                    <div className="w-full h-80 lg:h-88 lg:w-1/2 flex flex-wrap flex-col lg:flex-row lg:flex-row-reverse">
                        <div className="w-full h-0 flex-grow flex flex-center bg-white p-2 lg:w-0 lg:flex-grow lg:h-full">
                            <img src={`product/departaments/${productImage}`} className="h-full" alt={slug(prod.name)} />
                        </div>
                        <div className="w-full h-20 py-1 flex flex-center lg:w-24 lg:h-full lg:block">
                            <div className="h-full flex-grow lg:hidden"></div>
                            {productImages.map((item,index)=>(
                                <div key={index} className="h-16 w-16 lg:w-20 lg:h-20 border hover:shadow-xl rounded-sm mx-0.5 p-0.5 lg:mx-auto lg:mb-2" onMouseEnter={e => setProductImage(item.filename)} onClick={e=>setProductImage(item.filename)}>
                                    <img src={`product/departaments/${item.filename}`} alt={slug(item.filename)} />
                                </div>
                            ))}
                            <div className="h-full flex-grow lg:hidden"></div>
                        </div>
                    </div>
                    <div className="w-full h-auto  lg:w-1/2 py-1 px-2 lg:px-6 lg:flex lg:flex-col">
                        <h2 className="text-dark-100 text-2xl font-black">{prod.name}</h2>
                        <div className="w-full py-0.5 text-secondary text-base lg:text-sm">
                            <span className="mr-2 font-normal"><strong>Marca:</strong> {prod.brand_name}</span>
                            {(prod.model && prod.model!=='') && <span className="mr-2">Modelo: <strong>{prod.model}</strong></span>}
                            <span className="mr-2"><strong>SKU:</strong>{prod.sku}</span>
                        </div>
                        <div className="py-4">
                            {productDescription.map((item,index)=>(
                                <p key={index} className="text-sm">
                                    {item}
                                </p>
                            ))}
                        </div>
                        <div className="w-full flex-grow hidden lg:block"></div>
                        <div className="w-full mb-2">
                            {
                            prod.net_sale_price &&
                            <span className="text-3xl lg:text-2xl text-dark-100">$ {styledPrices(prod.net_sale_price)}</span>
                            }
                        </div>
                        <div className="w-full mb-2">
                            {(startShippingDate && startShippingDate!=="") &&
                            <span className="text-xs inline-flex flex-start bg-green-500 text-gray-50 rounded-full px-4 mr-2">
                                <i className="las la-shipping-fast text-xl mr-2"></i>
                                {
                                    (startShippingDate.format('MM')===endShippingDate.format('MM'))?
                                    `Recíbelo entre el ${startShippingDate.format('DD')} y el ${endShippingDate.format('DD [de] MMMM')}`
                                    :
                                    `Recíbelo entre el ${startShippingDate.format('DD [de] MMMM')} y el ${endShippingDate.format('DD [de] MMMM')}`
                                }
                            </span>}
                            {prod.stock < 1 ?
                                <span className="text-xs inline-flex flex-start bg-red-500 text-gray-50 rounded-full px-4 py-0">
                                <i className="las la-times text-xl mr-2"></i>
                                Poco inventario
                            </span> :
                            <span className="text-xs inline-flex flex-start bg-green-500 text-gray-50 rounded-full px-4 py-0">
                                <i className="las la-check text-xl mr-2"></i>
                                Disponible
                            </span>}
                            
                        </div>
                        <div className="w-full h-12 flex flex-center lg:flex-start">
                            <InputNumber value={quantity} setValue={setQuantity} max={prod.stock} className="h-full text-center w-12" />
                            <button className="btn-primary h-10 w-0 flex-grow rounded-sm lg:w-72 lg:flex-grow-0 text-sm"
                                onClick={e=>addToCart(productId,quantity)}
                            >
                                <i className="las la-luggage-cart text-2xl lg:text-xl mr-2"></i>
                                Añadir al carrito
                            </button>
                        </div>
                    </div>
                    {(productId>0 && prod.description!=="") &&
                        <ProductDescription productId={productId} description={prod.full_description || prod.description}></ProductDescription>
                    }
                </div>
            </Wrapper>
            <Wrapper>
                <div className="w-full py-4">
                    {(productId>0) &&
                        <SmallProductsBar type="RELATED" view="mini" productId={productId}></SmallProductsBar>
                    }
                </div>
            </Wrapper>
        </>
    )
}