import axios from "axios";
import { useContext, useEffect, useState ,useRef} from "react";
import ProductCard from "../../components/Products/ProductCard"
import Product from "../../components/Products/ProductsCardMod"
import LoadingSpinner from "../../components/htmlComponents/loading"
const vwBrands = ({match}) =>{

    const [productsList,setProductsList] = useState([]);
    const [loading,setloading] = useState(true);
    const getProductsBrand=()=>{

        let brandID = match.params.name;
        axios.post('listProductsBrand', {brandID:match.params.name}).then(response=>{
            console.log(response.data.products.data)
            setProductsList(response.data.products.data);
                setloading(false);
            //console.log(brandID)
        }).catch(error =>{
            console.log(error.response.data)
        })
    }

    useEffect(() => {
        if(loading){
            getProductsBrand();
        }
        
    }, [match.params.name])
    return (
        
        <>
        {loading ?
            <LoadingSpinner loading={loading}/>
        : <div className="w-full 2xl:px-0 max-w-design mx-auto">
                <div className="container w-full pt-12 " >
                    <div className="w-full flex flex-wrap bg-white py-2 px-2">
                        <div className="w-full bg-white lg:w-0 lg:flex-grow overflow-hidden whitespace-nowrap">
                            <div className="w-full h-auto flex flex-wrap">
                                {productsList.map((item,index)=>(                 
                                    <Product key={index} product={item} />
                                ))}
                            </div>

                        </div>
                    </div>
                </div>
            </div>}
            
        </>
    );
}
export default vwBrands;