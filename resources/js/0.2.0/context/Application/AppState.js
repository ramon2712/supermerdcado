import React, { useReducer, useState } from 'react';
import AppReducer from './AppReducer';
import AppContext from './AppContext';
import {SET_CART_COUNT, SET_CART_LIST, SET_USER} from './AppTypes';
import axios from 'axios';
import swal from "sweetalert"

const AppState = ({ children }) => {
    const user = {
        ready: false,
        auth:false,
        name: '',
        email: '',
        id: 0
    };
    const [departaments, setDepartaments] = useState({
        abarrotes:{
            name:'Abarrotes',
            id: 2
        },
        tecnologia:{
            name:'Tecnologia',
            id: 1
        },
        frutas:{
            name:'Frutas',
            id: 3
        },
        electrodomesticos:{
            name:'Linea Blanca',
            id: 4
        },
        carnes:{
            name:'Carnes',
            id: 5
        }
    });
    const departamentList={departaments,setDepartaments};
    const searchText= '';
    const cart={
        list:[],
        counter:0,
        id:0
    };

    const [state, dispatch] = useReducer(AppReducer, {user,searchText,cart,departamentList});
    const setUser=(u)=>{
        dispatch({
            payload:u,
            type:SET_USER
        })
    };
    const setCountCart = (counter) => {
        dispatch({
            payload:counter,
            type:SET_CART_COUNT
        });
    }
    const setCartList = list => {
        dispatch({
            payload:list,
            type:SET_CART_LIST
        })
    }
    const addToCart = (product_id,quantity=1) => {        
        //alert("agregando al carrito")
        const route=state.user.auth?'api/cart' : '/cookie/cart';
        const item = {
            id_product: product_id,
            id: product_id,
            quantity,
            id_user:state.user.auth?state.user.id : 0,
            status: 0
        }
        console.log(route);
        console.log(item)
        axios.post(route, item).then(res=>{
            console.log("Carrito",res);
            //alert("agregado al carrito")
            setCountCart(res.data.length);
            setCartList(res.data);
            swal({
                title: "¡Agregado con éxito!",
                text: 'El producto se agregó a tu carrito',
                icon: "success",
                button: "Aceptar"
            });
        }).catch(erros=>{
            console.log(erros);
        })
    }
    const initCart=(auth)=>{
        const route=auth?('api/cart/' + state.user.id):'cookie/cart';
        axios.get(route).then((res) => {
            //console.log("tamaño: "+res.data.length);
            setCountCart(res.data.length);
            setCartList(res.data);
            //console.log("El count: "+countCart);
        }).catch(errors =>{
            console.log(errors.response);
        });
    }
    return ( 
        <AppContext.Provider value = {{
                user: state.user,
                setUser,
                cart:state.cart,
                addToCart,
                initCart,
                departamentList:state.departamentList
            }}> 
            { children } 
        </AppContext.Provider>
    )
};

export default AppState;