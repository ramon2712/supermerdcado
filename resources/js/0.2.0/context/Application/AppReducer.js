import {SET_CART_COUNT, SET_CART_LIST, SET_CART_UID, SET_USER} from './AppTypes';

export default ( state , action )=>{
    const {payload,type}=action;

    switch(type){
        case SET_USER: return {...state,user:payload}
        case SET_CART_COUNT: return {...state,cart:{...state.cart,counter:payload}}
        case SET_CART_LIST: return {...state,cart:{...state.cart,list:payload}}
        case SET_CART_UID: return {...state,cart:{...state.cart,id:payload}}
        default: return state
    }
};