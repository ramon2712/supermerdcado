import banner from '../assets/banner_login_amb.svg';
import {StyledForm} from '../../forms/StyledForm';
import { useFormik } from 'formik';
import *as Yup from 'yup';
import axios from 'axios';
import { useContext, useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import AppContext from '../../../context/Application/AppContext';

const Register = props => {
    const [localUser, setLocalUser] = useState(true);
    const {user} = useContext(AppContext);
    //onsole.log(user);
    const handleSubmit= (values, {setSubmitting})=>{
        setSubmitting(false);
        axios.post('/register', {...values}).then(response=>{
            //console.log(response.data);
            alert("Se registró con éxito")
            
        }).catch(error=>{
            if(error.status === 422){
                if(error.response.data.errors.email){
                    alert("El correo ingresado ya fue registrado")
                }
            }
            console.log(error.response)
            alert("Error");
        })
    }
    const validateSchema = Yup.object().shape({
        name:Yup.string('Nombre no válido').required('Necesitas ingresar tu nombre'),
        lastname:Yup.string('Apellido no valido').required('Necesitas ingresar tu apellido'),
        email:Yup.string().email('Correo no válido').required('Necesitas ingresar tu correo'),
        phone:Yup.string('telefono no valido').required('necesitas ingresar un número telefónico'),
        password:Yup.string().required('Es necesario ingresar la contraseña').min(8,'Contraseña muy corta')
    })
    const formik = useFormik({
        initialValues: {
            name:'',
            lastname:'',
            email:'',
            phone:'',
            password:'',
        },
        onSubmit:handleSubmit, validationSchema:validateSchema
    })
    useEffect(()=>{
        setLocalUser(user.auth);
    }, [])
    return(
        <>
        {!user.auth ? <div className="w-full h-screen flex flex-wrap flex-center">
                <div className=" flex flex-wrap-reverse md:flex-wrap flex-center w-full md:w-2/3 md:h-4/6">
                    <div className="w-full bg-white md:w-1/2 h-34 md:h-full flex flex-center">
                        <img src={banner} alt="" className="h-full"/>
                    </div>
                    <div className="w-full md:w-1/2 bg-white p-8 h-3/4 md:h-full">
                        <h1 className="text-3xl font-bold text-primary text-center">¡Bienvenido (a)!</h1>
                        <StyledForm onSubmit={formik.handleSubmit} className="mt-8">
                            <div className="">
                                <label htmlFor="name" className="text-sm pl-4 text-secondary">Nombre</label>
                                <div className="pl-8 pr-8">
                                    <input name="name" type="text" value={formik.values.name} onChange={formik.handleChange} className="w-full"></input>
                                    {formik.errors.name && formik.touched.name && <small>{formik.errors.name}</small>}
                                </div>
                            </div>
                            <div className="">
                                <label htmlFor="email" className="text-sm pl-4 text-secondary">Apellido</label>
                                <div className="pl-8 pr-8">
                                    <input name="lastname" type="text" value={formik.values.lastname} onChange={formik.handleChange} className="w-full"></input>
                                    {formik.errors.lastname && formik.touched.lastname && <small>{formik.errors.lastname}</small>}
                                </div>
                            </div>
                            <div className="">
                                <label htmlFor="email" className="text-sm pl-4 text-secondary">Correo electrónico</label>
                                <div className="pl-8 pr-8">
                                    <input name="email" type="email" value={formik.values.email} onChange={formik.handleChange} className="w-full"></input>
                                    {formik.errors.email && formik.touched.email && <small>{formik.errors.email}</small>}
                                </div>
                            </div>
                            <div className="">
                                <label htmlFor="email" className="text-sm pl-4 text-secondary">Teléfono</label>
                                <div className="pl-8 pr-8">
                                    <input name="phone" type="tel" value={formik.values.phone} onChange={formik.handleChange} className="w-full" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"></input>
                                    {formik.errors.phone && formik.touched.phone && <small>{formik.errors.phone}</small>}
                                </div>
                            </div>
                            <div className="">
                                <label htmlFor="email" className="text-sm pl-4 text-secondary">Contraseña</label>
                                <div className="pl-8 pr-8">
                                    <input name="password" type="password" value={formik.values.password} onChange={formik.handleChange} className="w-full"></input>
                                    {formik.errors.password && formik.touched.password && <small>{formik.errors.password}</small>}
                                    <input name="password" type="hidden" value={formik.values.password} className="w-full hidden"/>
                                </div>
                            </div>
                                
                            <div className="mt-5 flex flex-col mx-4">
                                <button type="submit" className="btn-primary w-full mb-2">Registrarme</button>
                                <a href="/login" className="text-primary text-sm ">Ya tengo una cuenta, Ingresar</a>
                            </div>
                        </StyledForm>
                    </div>
                </div>                
            </div> : <Redirect to="/" />}
        </>
    )
};

export default Register;