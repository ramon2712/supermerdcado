import axios from "axios"
import swal from "sweetalert"
import {useHistory} from "react-router-dom"

const Logout =() =>{

    const history = useHistory();
    swal("Estas cerrando sesión, vuelve pronto.",{
        buttons:false,
        timer:3000
    });
    setTimeout(function () {
        axios.get("/logout").then((res) =>{
            console.log (res);
            history.push("/");
            window.location.reload();
        });
    }, 3000);

    return(null);
}

export default Logout;