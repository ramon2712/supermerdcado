import banner from '../assets/banner_login_amb.svg';
import {StyledForm} from '../../forms/StyledForm';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import { useContext, useEffect, useState } from 'react';
import { Redirect, useHistory } from 'react-router-dom';
import AppContext from '../../../context/Application/AppContext';
import swal from "sweetalert";

const Login = (props) => {
    const {user,setUser} = useContext(AppContext);
    const [loading,setLoading]=useState(true);
    const history = useHistory();
    const handleSubmit= (values, {setSubmitting})=>{
        //console.log(values);
        setLoading(true);
        setSubmitting(false);
        axios.post('/login', {...values}).then(response=>{
            console.log("Login",response);
            //Validar si el usuario está autentificado
            axios.post('authenticate/check').then(res=>{
                console.log("Check",res);
                axios.post('/cookie/addcart', {'id_user':res.data.user.id}).then(resp=>{
                    console.log("Cart",resp.data);
                }).catch(errors=>{
                    console.log(errors);
                })
                setLoading(false)
            });   
            //console.log("Entró");  
            //console.log({...values});       
            //console.log(user)
            window.location.reload();
            //history .push('/');    
           //document.location.href = "";     
        }).catch(e=>{
            console.log(e);
        });
    }    
    const validateSchema = Yup.object().shape({
        email:Yup.string().email('Correo no válido').required('Necesitas ingresar tu correo'),
        password:Yup.string().required('Es necesario ingresar la contraseña')
    })
    const formik = useFormik({
        initialValues: {
            email:'',
            password:''
        },
        onSubmit:handleSubmit, validationSchema:validateSchema
    })
    useEffect(()=>{
        if(loading){
            //console.log("Cargando, aqui va el usuario",user)
            axios.post('authenticate/check').then(res=>{
                //console.log(res);
                setLoading(false)
            });
        }
    }, [loading])
    return (
        <>  
            {!loading &&
                <>
                {(!user.auth)?
                <>
                <div className="w-full h-screen flex flex-wrap flex-center">
                    <div className=" flex flex-wrap-reverse md:flex-wrap flex-center w-full md:w-2/3 md:h-4/6">
                        <div className="w-full bg-white md:w-1/2 h-34 md:h-full flex flex-center">
                            <img src={banner} alt="Imagen Logo" className="h-full"/>
                        </div>
                        <div className="w-full md:w-1/2 bg-white p-8 h-3/4 md:h-full">
                            <h1 className="text-4xl font-bold text-primary text-center">¡Bienvenido (a)!</h1>
                            <h4 className="mt-8 text-secondary">Te invitamos a registrarte para comenzar la experiencia</h4>
                            <StyledForm onSubmit={formik.handleSubmit} className="mt-8">
                                <div className="">
                                    <label htmlFor="email" className="">Correo electrónico</label>
                                    <div className="pl-8 pr-8">
                                        <input name="email" type="email" value={formik.values.email} onChange={formik.handleChange} className="w-full"></input>
                                        {formik.errors.email && formik.touched.email && <small>{formik.errors.email}</small>}
                                    </div>
                                </div>
                                <div className="">
                                    <label htmlFor="email" className="">Contraseña</label>
                                    <div className="pl-8 pr-8">
                                        <input name="password" type="password" value={formik.values.password} onChange={formik.handleChange} className="w-full"></input>
                                        {formik.errors.password && formik.touched.password && <small>{formik.errors.password}</small>}
                                    </div>
                                </div>
                                <div className="mt-5">
                                    <button type="submit" className="btn-primary w-full">Iniciar sesión</button>
                                </div>
                                <div className="mt-5">
                                    <a href="/password-reset" className="text-primary text-sm mr-8">Olvidé mi contraseña</a>
                                    <a href="/register" className="text-primary text-sm">Regístrate</a>
                                </div>
                            </StyledForm>
                        </div>
                    </div>                
                </div>
                </>
                :
                <Redirect to="/" />
                }
                </>
            }
        </>
    );
};

export default Login;
