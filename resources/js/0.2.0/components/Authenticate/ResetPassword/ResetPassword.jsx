import banner from '../assets/banner_login_login.svg';
import {StyledForm} from '../../forms/StyledForm';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import { useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import AppContext from "../../../context/Application/AppContext";

const ResetPassword = (props) => {
    const {setUser} = useContext(AppContext);
    const history = useHistory();

    const handleSubmit= (values, {setSubmitting})=>{
        setSubmitting(false);
        axios.post('/password/email', {...values}).then(response=>{
            console.log(response);  
            setUser(response.data);
            //history.goBack();//push('/');    
           //document.location.href = "";     
        }).catch(error=>{
            console.log(error.response)
        })
    }
    const validateSchema = Yup.object().shape({
        email:Yup.string().email('Correo no válido').required('Necesitas ingresar tu correo')
    })
    const formik = useFormik({
        initialValues: {
            email:''
        },
        onSubmit:handleSubmit, validationSchema:validateSchema
    })
    useEffect(()=>{
        axios.post('authenticate/check').then(res=>{
            console.log(res);
        })
    }, [])
    return (
        <>
            <div className="w-full h-screen flex flex-wrap flex-center">
                <div className=" flex flex-wrap-reverse md:flex-wrap flex-center w-full md:w-2/3 md:h-4/6">
                    <div className="w-full md:w-1/2 bg-white p-8 rounded-md">
                        <h1 className="text-4xl font-bold text-primary">Restauración de contraseña</h1>
                        <h4 className="mt-8 text-secondary">Ingresa tu correo electrónico y recibirás el link para restablecer tu contraseña</h4>
                        <StyledForm onSubmit={formik.handleSubmit} className="mt-8">
                            <div className="">
                                <label htmlFor="email" className="text-secondary p-2">Correo electrónico</label>
                                <div className="pl-8 pr-8">
                                    <input name="email" type="email" value={formik.values.email} onChange={formik.handleChange} className="w-full"></input>
                                    {formik.errors.email && formik.touched.email && <small>{formik.errors.email}</small>}
                                </div>
                            </div>
                            <div className="mt-5 pl-4">
                                <button type="submit">Enviar link de restauración</button>
                            </div>
                        </StyledForm>
                    </div>
                </div>                
            </div>
            
        </>
    );
};

export default ResetPassword;
