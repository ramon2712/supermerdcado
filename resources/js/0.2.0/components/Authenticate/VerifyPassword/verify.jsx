import banner from '../assets/banner_login_login.svg';
import {StyledForm} from '../../forms/StyledForm';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import { useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Context } from '../..';
const VerifyPassword = ({match}) => {
    //const { match: { params } } = this.props;
    const {setUser} = useContext(Context);
    const handleSubmit= (values, {setSubmitting})=>{
        setSubmitting(false);
        console.log(values)
        axios.post('/password/reset', {...values}).then(response=>{
            console.log(response);  
            setUser(response.data);
            history.push('/');    
           //document.location.href = "";     
        }).catch(errors=>{
            console.log(errors.response);
        })
    }
    const history = useHistory();
    const validateSchema = Yup.object().shape({
        email:Yup.string().email('Correo no válido').required('Necesitas ingresar tu correo')
    })
    const formik = useFormik({
        initialValues: {
            token: match.params.token,
            email:match.params.email,
            password:'',
            password_confirmation:''
        },
        onSubmit:handleSubmit, validationSchema:validateSchema
    })
    useEffect(()=>{
        axios.post('authenticate/check').then(res=>{
            console.log(res);
        })
    }, [])
    return (
        <>
            <div className="w-full h-screen flex flex-wrap flex-center">
                <div className=" flex flex-wrap-reverse md:flex-wrap flex-center w-full md:w-2/3 md:h-4/6">
                    <div className="w-full md:w-1/2 bg-white p-8 rounded-md">
                        <h1 className="text-4xl font-bold text-primary">Restauración de contraseña</h1>
                        <h4 className="mt-8">Ingresa una nueva contraseña</h4>
                        <StyledForm onSubmit={formik.handleSubmit} className="mt-8">
                            <div className="">
                                <label htmlFor="email" className="text-secondary p-2">Correo electrónico</label>
                                <div className="pl-8 pr-8">
                                    <input name="email" type="email" value={formik.values.email} onChange={formik.handleChange} className="w-full"></input>
                                    {formik.errors.email && formik.touched.email && <small>{formik.errors.email}</small>}
                                </div>
                            </div>
                            <div className="">
                                <label htmlFor="email" className="text-secondary p-2">Nueva contraseña</label>
                                <div className="pl-8 pr-8">
                                    <input name="password" type="password" value={formik.values.password} onChange={formik.handleChange} className="w-full"></input>
                                    {formik.errors.password && formik.touched.password && <small>{formik.errors.password}</small>}
                                </div>
                            </div>
                            <div className="">
                                <label htmlFor="email" className="text-secondary p-2">Confirma contraseña</label>
                                <div className="pl-8 pr-8">
                                    <input name="password_confirmation" type="password" value={formik.values.password_confirmation} onChange={formik.handleChange} className="w-full"></input>
                                    {formik.errors.password_confirmation && formik.touched.password_confirmation && <small>{formik.errors.password_confirmation}</small>}
                                </div>
                            </div>
                            <div className="mt-5 pl-4">
                                <button type="submit">Cambiar contraseña</button>
                            </div>
                        </StyledForm>
                    </div>
                </div>                
            </div>
            
        </>
    );
};

export default VerifyPassword;
