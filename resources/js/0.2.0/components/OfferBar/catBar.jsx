import { Link, NavLink } from 'react-router-dom';
import "./style.css";
const CatBar = props => {

    return(
        <div className="my-10 w-full max-w-design mx-auto flex overflow-hidden">
            <div className="w-full lg:w-1/4 md:w-1/2 sm:w-1/2 lg:h-34 flex flex-center rounded-xl mr-3 h-36 overflow-hidden">
                        <NavLink to="/departament/tecnologia" activeClassName="current">
                            <img src="images/departaments/tecnologia.png" alt="Tecnologia" className="mx-auto h-36 flex flex-center" />
                        </NavLink>
            </div>
            <div className="w-full lg:w-1/4 md:w-1/2 sm:w-1/2 lg:h-34 h-34 flex flex-center bg-white rounded-xl mr-3 h-36 overflow-hidden">
                        <NavLink to="/departament/abarrotes" activeClassName="current" >
                            <img src="images/departaments/ABARROTES.png" alt="Abarrotes" className="mx-auto h-36 flex flex-center" />
                        </NavLink>
            </div>
            <div className="w-full lg:w-1/4 md:w-1/2 sm:w-1/2 lg:h-34 h-34 flex flex-center bg-white rounded-xl mr-3 h-36 overflow-hidden">
                        <NavLink to="/departament/frutas" activeClassName="current" >
                            <img src="images/departaments/frutas.png" alt="Hogar" className="mx-auto h-36 flex flex-center" />
                        </NavLink>
            </div>
            <div className="w-full lg:w-1/4 md:w-1/2 sm:w-1/2 lg:h-34 h-34 flex flex-center bg-white rounded-xl mr-3 h-36 overflow-hidden">
                        <NavLink to="/departament/electrodomesticos" activeClassName="current" >
                            <img src="images/departaments/linea-blanca.png" alt="Linea Blanca" className="mx-auto h-36 flex flex-center" />
                        </NavLink>
            </div>
            <div className="w-full lg:w-1/4 md:w-1/2 sm:w-1/2 lg:h-34 h-34 flex flex-center bg-red rounded-xl h-36 overflow-hidden">
                        <NavLink to="/departament/carnes" activeClassName="current" >
                            <img src="images/departaments/carnes.png" alt="Carnes" className="mx-auto h-36 flex flex-center" />
                        </NavLink>
            </div>
        </div>
    )
}

export default CatBar;