import { Link, NavLink } from 'react-router-dom';
import "./style.css";
const OfferBar = props => {

    return(
        <div className="my-12 w-full lg:h-28 bg-white max-w-design mx-auto flex flex-start flex-wrap">
            <div className="w-full lg:w-1/4 md:w-1/2 sm:w-1/2 h-24 bg-white flex flex-center px-1">
                <div className="w-full flex flex-col">
                    <div className="w-full h-6 text-center">
                        <strong>Ferretería</strong>
                    </div>
                    <div className="w-full flex-grow text-center">
                        <NavLink to="/categoria/ferreteria" activeClassName="current" >
                            <img src="images/envios-gratis.svg" alt="Macep 26 años de experiencia" className="mx-auto h-10" />
                        </NavLink>
                    </div>
                    <div>
                        <p className="text-free-shipping">Envio gratis en compras mayores a $1000.00 MXN</p>
                    </div>
                </div>
            </div>
            <div className="w-full lg:w-1/4 md:w-1/2 sm:w-1/2 h-24 bg-white md:border-l flex flex-center px-1">
                <div className="w-full flex flex-col">
                    <div className="w-full h-6 text-center">
                    <strong>Construcción</strong>
                    </div>
                    <div className="w-full flex-grow text-center">
                        <NavLink to="/categoria/construccion" activeClassName="current" >
                            <img src="images/envios-gratis.svg" alt="Envíos gratis" className="mx-auto h-10" />
                        </NavLink>
                        
                    </div>
                    <div>
                        <p className="text-free-shipping">Envio gratis en compras mayores a $10000.00 MXN</p>
                    </div>
                </div>
            </div>
            <div className="w-full lg:w-1/4 md:w-1/2 h-24 sm:w-1/2 bg-white md:border-l flex flex-center px-1">
                <div className="w-full flex flex-col">
                    <div className="w-full h-6 text-center">
                    <strong>Minería</strong>
                    </div>
                    <div className="w-full flex-grow text-center">
                        <NavLink to="/categoria/mineria" activeClassName="current" >
                            <img src="images/envios-gratis.svg" alt="Envíos gratis" className="mx-auto h-10" />
                        </NavLink>
                        
                    </div>
                    <div>
                        <p className="text-free-shipping">Envio gratis en compras mayores a $50000.00 MXN</p>
                    </div>
                </div>
            </div>
            <div className="w-full lg:w-1/4 md:w-1/2 sm:w-1/2 h-24 bg-white md:border-l flex flex-center px-1">
                <div className="w-full text-center">
                    <div className="w-full h-6 text-center">
                        <small className="">Paga de manera segura con</small>
                    </div>
                    <div className="w-full flex-grow">
                        <a href="https://conekta.com/" target="_blank">
                            <img src="images/logo_conekta.svg" alt="logo conekta" className="mx-auto h-6" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default OfferBar