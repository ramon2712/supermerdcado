import axios from "axios";
import React, { Component, useEffect, useState } from "react";

const Products = ({ setTotal, id, nextStep, user, setItemD, setOpenModal}) => {
    const [items, setItems] = useState([]);
    const [inputs, setInputs] = useState({});
    const getItems = () => {
        axios.get("api/orders/products/" + id).then((response) => {
            let inp = {};
            let count = 0;
            response.data.forEach((element) => {
                inp = {
                    ...inp,
                    ["quantity_" + element.id]: element.quantity,
                };
                count = count + element.sale_price * element.quantity;
            });
            setInputs(inp);
            setItems(response.data);
            setTotal(count);
        });
    };
    useEffect(() => {
        getItems();
    }, [user]);

    return (
        <div className="h-4/5">
            <div className="w-full flex flex-wrap flex-center">
                {items.map((item, index) => (
                    <div
                        key={index}
                        className="flex p-8 w-full md:w-4/5 flex-wrap border-b content-"
                    >
                        <img
                            src={
                                "http://macep.com/api/files/images/thumbnails/" +
                                item.filename
                            }
                            alt=""
                            className="bg-green-500 h-1/8 w-1/8 md:w-12 md:h-12"
                        />
                        <div className="flex-grow ml-8">
                                <h3>{item.name}</h3>
                            <button
                                className="text-primary p-4 w-5/8"
                                onClick={(e) => {
                                    setItemD(item);
                                    setOpenModal(!openModal);
                                }}
                            >
                                Eliminar del carrito
                            </button>
                        </div>

                        <div className="w-24 h-10 flex flex-center">
                            <input
                                type="number"
                                name={"quantity_" + item.id}
                                step="1"
                                min="1"
                                value={parseInt(
                                    inputs["quantity_" + item.id],
                                    10
                                )}
                                className="w-10 h-10 border text-center"
                                onChange={(e) => handleChange(item, e)}
                            />
                            <div className="h-full w-6 border border-l-0 flex flex-wrap">
                                <button
                                    className="w-full h-1/2 flex flex-center"
                                    onClick={() => editQuantity(item, 1)}
                                >
                                    +
                                </button>
                                <button
                                    className="w-full border-t h-1/2 flex flex-center"
                                    onClick={() => editQuantity(item, -1)}
                                >
                                    -
                                </button>
                            </div>
                        </div>
                        <div className="w-32 flex flex-end content-center">
                            <h4 className="text-xl text-black">
                                {new Intl.NumberFormat("ES-MX", {
                                    style: "currency",
                                    currency: "MXN",
                                }).format(item.sale_price * item.quantity)}
                            </h4>
                        </div>
                    </div>
                ))}
            </div>
            <div className="w-full flex flex-end">
                <button
                    className="bg-primary h-10 p-2 text-white rounded m-8"
                    onClick={nextStep}
                >
                    Continuar
                </button>
            </div>
        </div>
    );
};

export default Products;
