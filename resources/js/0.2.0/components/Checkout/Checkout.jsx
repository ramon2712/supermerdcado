import { useContext, useEffect, useState } from "react";
import axios from "axios";
import { Link, withRouter, useHistory } from "react-router-dom";
import CardPayment from "../ConektaAPI/cardpayment";
import OxxoPay from "../ConektaAPI/oxxopay";
import DepositoPay from "../ConektaAPI/deposito";
import AddAddress from "../htmlComponents/addAddress";
import AppContext from "../../context/Application/AppContext";
import { useTinyModalDialog } from "../Utils/tinymodalmx/tinymodal";
import Products from "./steps/products";
import OxxoImage from "../../assets/images/oxxopay.svg"

const CheckoutComponent = ({ match }) => {
    const [step, setStep] = useState(1);
    const [order, setOrder] = useState([]);
    const [id] = useState(match.params.id);
    const [items, setItems] = useState([]);
    const [total, setTotal] = useState(0);
    const [inputs, setInputs] = useState({});
    const [address, setAddress] = useState([]);
    const [payments, setPayments] = useState([]);
    const [userAddress, setUserAddress] = useState([]);
    const [payMethod, setPayMethod] = useState([]);
    const { user } = useContext(AppContext);
    const Modal = useTinyModalDialog();
    const [openModal, setOpenModal] = useState(false);
    const [addressModal, setAddressModal] = useState(false);
    const [token_id, setToken] = useState("");
    const [itemD, setItemD] = useState([]);
    const [modalE, setModalE] = useState(false);
    const [errorM, setErrorM] = useState("");
    const history=useHistory();


    const getItems = () => {
        console.log(user);
        axios.get("api/orders/" + id).then((response) => {
            setOrder(response.data);
        });
        axios.get("api/orders/products/" + id).then((response) => {
            let inp = {};
            let count = 0;
            response.data.forEach((element) => {
                inp = {
                    ...inp,
                    ["quantity_" + element.id]: element.quantity,
                };
                count = count + element.sale_price * element.quantity;
            });
            setInputs(inp);
            setItems(response.data);
            setTotal(count);
        });
    };
    const getPayments = () => {
        axios.get("api/checkout/payments").then((response) => {
            console.log("payments: ", response.data);
            setPayMethod(response.data[0]);
            setPayments(response.data);
        });
    };
    const getAddress = () => {
        if (user.auth) {
            axios
                .get("/api/address/" + user.id)
                .then((response) => {
                    console.log(response.data);
                    response.data.forEach((element) => {
                        if (element.main) {
                            setUserAddress(element);
                            console.log("El main address es: ", element);
                        }
                    });
                    setAddress(response.data);
                })
                .catch((errors) => {
                    console.log(errors.response);
                });
        }
    };
    const nextStep = () => {
        var next = step + 1;
        setStep(next);
    };
    const prevStep = () => {
        if (step > 1) {
            var prev = step - 1;
            setStep(prev);
        }
    };
    const editQuantity = (prod, quantity) => {
        if (parseInt(prod.quantity, 10) + quantity > 0) {
            let item = {
                id_product: prod.id_product,
                order_id: id,
                quantity: parseInt(prod.quantity, 10) + quantity,
                sale_price: prod.sale_price,
                amount: prod.amount,
                ship_quantity: prod.ship_quantity,
            };

            axios
                .put("api/orders/products/" + prod.id, item)
                .then((response) => {
                    console.log(response);
                    getItems();
                })
                .catch((errors) => {
                    console.log(errors.response);
                });
        }
    };
    const handleChange = (prod, e) => {
        const value = e.target.value;
        if (value > 0) {
            setInputs({ ...inputs, ["quantity_" + prod.id]: value });
            let item = {
                id_product: prod.id_product,
                order_id: id,
                quantity: value,
                sale_price: prod.sale_price,
                amount: prod.amount,
                ship_quantity: prod.ship_quantity,
            };
            if ((value < 0 && prod.quantity > 0) || value > 0) {
                axios
                    .put("api/orders/products/" + prod.id, item)
                    .then((response) => {
                        console.log(response);
                        getItems();
                    })
                    .catch((errors) => {
                        console.log(errors.response);
                    });
            }
        }
    };
    const deleteItem = (item) => {
        console.log("item a eliminar: ", itemD);
        axios.delete("api/orders/products/" + itemD.id).then((response) => {
            getItems();
            setOpenModal(!openModal);
        });
    };
    const onChangeRadio = (item, e) => {
        if (e.target.checked) {
            console.log("dirección pred: ", item);
            setUserAddress(item);
        }
    };
    const onChangePayment = (item, e) => {
        if (e.target.checked) {
            console.log("Método de pago: ", item);
            setPayMethod(item);
        }
    };
    const closeModalA = () => {
        console.log("cerrando modal");
        setAddressModal(false);
    };
    const tokenizer = (e) => {
        e.preventDefault();
        Conekta.setPublicKey("key_I8sSK27V5isvxXxqCRsxThA");
        console.log("hola we");
        const conektaSuccessResponseHandler = (token) => {
            console.log(token.id);
            setToken(token.id);
            console.log("el token es: ", token_id);
            completeOrder(token.id);
            /*  */
        };
        const conektaErrorResponseHandler = (response) => {
            console.log(response);
            console.log("hola");
        };
        Conekta.Token.create(
            e.target,
            conektaSuccessResponseHandler,
            conektaErrorResponseHandler
        );
    };
    const completeOrder = async (token) => {
        let total = 0;
        let count = 0;
        items.forEach((element) => {
            count = count + parseInt(element.quantity, 10);
            total = total + element.sale_price * parseInt(element.quantity, 10);
        });
        let subTotal = total - total * 0.16;
        let orderCheck = {
            user_id: user.id,
            order_status_id: 1,
            order_delivery_periods_id: 1,
            order_types_id: 1,
            user_address_id: userAddress.id,
            products_count: count,
            amount: total,
            taxes: 0.16,
            net_taxes: total * 0.16,
            subtotal: subTotal,
        };
        await axios.put("api/orders/" + id, orderCheck).then((response) => {
            console.log(response);
        });
        if (payMethod.id === 2) {
            let cardpay = {
                token_id: token,
                order_id: id,
            };
            console.log("datos a enviar a card: ", cardpay);

            await axios.post("/api/card", cardpay).then((response) => {
                //console.log(response);
                if (!response.data.errorStack) {
                    const conektaid = response.data[0].charges.data[0].order_id;
                    console.log("El order_id de conekta es: ", conektaid);
                    let schedule = {
                        order_id: id,
                        payment_type_id: payMethod.id,
                        payment_status_id: 4,
                        amount: total,
                        subtotal: subTotal,
                        taxes: total * 0.16,
                    };
                    axios
                        .post("/api/checkout/schedules/", schedule)
                        .then((response) => {
                            console.log(response.data);
                            let payment = {
                                order_payment_schedules_id: response.data.id,
                                payment_status_id: 3,
                                payment_type_id: payMethod.id,
                                paid_amount: total,
                                receipt: conektaid,
                            };
                            axios
                                .post("/api/checkout/order/payments", payment)
                                .then((response) => {
                                    console.log(
                                        "payment realzado: ",
                                        response.data
                                    );
                                });
                                history.push("/");
                        });
                } else {
                    console.log("error: ", response);
                    const errors = JSON.parse(response.data.errorStack);
                    setErrorM(errors.details[0].message);
                    console.log("mensaje error: ", errorM);
                    setModalE(true);
                }
            });
        } else if (payMethod.id === 4) {
            let oxxopay = {
                order_id: id,
            };
            axios.post("api/oxxo", oxxopay).then((response) => {
                console.log(response);
                const conektaid = response.data.charges.data[0].order_id;
                let schedule = {
                    order_id: id,
                    payment_type_id: payMethod.id,
                    payment_status_id: 2,
                    amount: total,
                    subtotal: subTotal,
                    taxes: total * 0.16,
                };
                axios
                    .post("/api/checkout/schedules/", schedule)
                    .then((response) => {
                        console.log("Schedule: ", response.data);
                        let payment = {
                            order_payment_schedules_id: response.data.id,
                            payment_status_id: 2,
                            payment_type_id: payMethod.id,
                            paid_amount: total,
                            receipt: conektaid,
                        };
                        axios
                            .post("/api/checkout/order/payments", payment)
                            .then((response) => {
                                console.log(
                                    "payment realzado: ",
                                    response.data
                                );
                            });
                    });
            });
        } else if (payMethod.id === 1) {
            let schedule = {
                order_id: id,
                payment_type_id: payMethod.id,
                payment_status_id: 1,
                amount: total,
                subtotal: subTotal,
                taxes: total * 0.16,
            };
            axios
                .post("/api/checkout/schedules/", schedule)
                .then((response) => {
                    console.log("Schedule: ", response.data);
                    let payment = {
                        order_payment_schedules_id: response.data.id,
                        payment_status_id: 1,
                        payment_type_id: payMethod.id,
                        paid_amount: total,
                    };
                    axios
                        .post("/api/checkout/order/payments", payment)
                        .then((response) => {
                            console.log("payment realzado: ", response.data);
                        });
                });
        }
        //  console.log("orden modificada: ",orderCheck)
    };
    useEffect(() => {
        getItems();
        getAddress();
        getPayments();
        console.log(order);
    }, [user]);
    return (
        <>
            <div className="w-full px-0.5 lg:px-24 2xl:px-0 max-w-design mx-auto p-8 bg-white">
                <div className="w-full pl-8">
                    <h1 className="text-4xl">Finaliza tu compra*</h1>
                </div>
                {step == 1 && (
                    <div className="h-4/5">
                        <div className="w-full flex flex-wrap flex-center">
                            {items.map((item, index) => (
                                <div
                                    key={index}
                                    className="flex p-8 w-full md:w-4/5 flex-wrap border-b content-"
                                >
                                    <img
                                        src={
                                            "product/images/thumbnails/" +
                                            item.filename
                                        }
                                        alt=""
                                        className="bg-green-500 h-1/8 w-1/8 md:w-12 md:h-12"
                                    />
                                    <div className="flex-grow ml-8">
                                        <Link to="">
                                            <h3>{item.name}</h3>
                                        </Link>
                                        <button
                                            className="text-primary p-4 w-5/8"
                                            onClick={(e) => {
                                                setItemD(item);
                                                setOpenModal(!openModal);
                                            }}
                                        >
                                            Eliminar del carrito
                                        </button>
                                    </div>

                                    <div className="w-24 h-10 flex flex-center">
                                        <input
                                            type="number"
                                            name={"quantity_" + item.id}
                                            step="1"
                                            min="1"
                                            value={parseInt(
                                                inputs["quantity_" + item.id],
                                                10
                                            )}
                                            className="w-10 h-10 border text-center"
                                            onChange={(e) =>
                                                handleChange(item, e)
                                            }
                                        />
                                        <div className="h-full w-6 border border-l-0 flex flex-wrap">
                                            <button
                                                className="w-full h-1/2 flex flex-center"
                                                onClick={() =>
                                                    editQuantity(item, 1)
                                                }
                                            >
                                                +
                                            </button>
                                            <button
                                                className="w-full border-t h-1/2 flex flex-center"
                                                onClick={() =>
                                                    editQuantity(item, -1)
                                                }
                                            >
                                                -
                                            </button>
                                        </div>
                                    </div>
                                    <div className="w-32 flex flex-end content-center">
                                        <h4 className="text-xl text-black">
                                            {new Intl.NumberFormat("ES-MX", {
                                                style: "currency",
                                                currency: "MXN",
                                            }).format(
                                                item.sale_price * item.quantity
                                            )}
                                        </h4>
                                    </div>
                                </div>
                            ))}
                        </div>
                        <div className="w-full flex flex-end">
                            <button
                                className="bg-primary h-10 p-2 text-white rounded m-8"
                                onClick={nextStep}
                            >
                                Continuar
                            </button>
                        </div>
                    </div>
                )}
                {step == 2 && (
                    <div className="w-full md:pr-16 md:pl-16 pr-1 pl-1 address md:h-4/5">
                        <h1 className="pl-8 text-2xl text-secondary">Dirección de envío*</h1>
                        <div className="w-full flex flex-end md:pb-16 pb-4 pr-4">
                            <button
                                className="rounded-sm bg-primary h-10 p-2 text-white"
                                onClick={(e) => setAddressModal(!addressModal)}
                            >
                                Agregar dirección
                            </button>
                        </div>
                        <div className="w-full md:pr-16 md:pl-16 pr-1 pl-1  md:overflow-y-scroll md:h-3/5">
                            {address.map((item_address, index) => (
                                <div
                                    key={index}
                                    className="flex flex-wrap items-center pr-4 pl-4 w-full rounded-sm"
                                >
                                    <input
                                        type="radio"
                                        name="address"
                                        value={item_address}
                                        id={"radio_" + item_address.id}
                                        checked={
                                            userAddress.id === item_address.id
                                        }
                                        onChange={(e) =>
                                            onChangeRadio(item_address, e)
                                        }
                                    />{" "}
                                    <label
                                        className="p-4 flex-grow hover:bg-gray-400 checked:bg-gray-200 hover:text-white"
                                        htmlFor={"radio_" + item_address.id}
                                    >
                                        <h3 className="text-primary text-xl font-bold">
                                            {item_address.street},{" "}
                                            {item_address.mx_zip_code_cp}
                                        </h3>

                                        <span className="text-secondary">
                                            {item_address.colonia} <br />
                                        </span>
                                        <p>{item_address.description}</p>
                                        <span className="text-secondary">
                                            {item_address.full_name},{" "}
                                            {item_address.phone}
                                        </span>
                                    </label>
                                </div>
                            ))}
                        </div>
                        <div className="w-full flex flex-end space-x-4 p-8">
                            <button
                                className="h-10 p-2 text-primary border rouded-sm"
                                onClick={prevStep}
                            >
                                Atrás
                            </button>
                            <button
                                className="bg-primary h-10 p-2 text-white rounded-sm"
                                onClick={nextStep}
                            >
                                Continuar
                            </button>
                        </div>
                    </div>
                )}
                {step == 3 && (
                    <div className="w-full md:pr-16 md:pl-16 pr-4 pl-4 address h-3/5 md:h-4/5">
                        <h1 className="pl-8 text-2xl text-secondary">Método de pago*</h1>
                        <div className="md:p-16 md:p-16">
                            {payments.map((element, index) => (
                                <div
                                    key={index}
                                    className="flex flex-wrap md:pl-8 md:pr-8 pt-1 w-full h-14 md:h-16"
                                >
                                    <input
                                        type="radio"
                                        name="address"
                                        value={element}
                                        checked={payMethod.id === element.id}
                                        onChange={(e) =>
                                            onChangePayment(element, e)
                                        }
                                        id={"pay_radio_" + element.id}
                                        className="checked:bg-blue-600"
                                    />
                                    <label
                                        htmlFor={"pay_radio_" + element.id}
                                        className="flex flex-wrap items-center hover:bg-secondary w-full hover:text-white"
                                    >
                                        <h1 className=" w-full md:w-1/4 text-xl">{element.name}</h1>
                                        {element.name === "Efectivo" && (
                                            <img src={OxxoImage} alt="" className="h-11"/>
                                        )}
                                        
                                    </label>
                                </div>
                            ))}
                        </div>
                        <div className="w-full flex flex-end space-x-4 md:p-8 p-4">
                            <button
                                className="h-10 p-2 text-primary border rouded-sm"
                                onClick={prevStep}
                            >
                                Atrás
                            </button>
                            <button
                                className="bg-primary h-10 p-2 text-white rounded-sm"
                                onClick={nextStep}
                            >
                                Continuar
                            </button>
                        </div>
                    </div>
                )}
                {step == 4 && (
                    <div>
                        <div className="p-4 md:p-16">
                            <h1 className="text-secondary text-2xl">Dirección de envío:</h1>
                            <h2 className="text-primary text-xl pl-4">
                                {userAddress.street}
                            </h2>
                            <p className="pl-4">
                                C.P. {userAddress.mx_zip_code_cp}
                            </p>
                        </div>
                        <div className="w-full flex flex-end">
                            <h2 className="text-4xl text-primary">
                                Total:{" "}
                                <span className="text-secondary">
                                    {new Intl.NumberFormat("ES-MX", {
                                        style: "currency",
                                        currency: "MXN",
                                    }).format(total)}
                                </span>
                            </h2>
                        </div>
                        {payMethod.id === 2 && (
                            <CardPayment
                                setToken={setToken}
                                tokenizer={tokenizer}
                                prevStep={prevStep}
                                completeOrder={completeOrder}
                            ></CardPayment>
                        )}
                        {payMethod.id === 4 && (
                            <OxxoPay
                                prevStep={prevStep}
                                completeOrder={completeOrder}
                            ></OxxoPay>
                        )}
                        {payMethod.id === 1 && (
                            <DepositoPay
                                prevStep={prevStep}
                                completeOrder={completeOrder}
                            ></DepositoPay>
                        )}
                    </div>
                )}
            </div>
            <Modal.TinyModalFrame on={openModal}>
                <h1 className="text-2xl text-primary">Eliminar del ordena:</h1>
                <p>{itemD.name}</p>
                <div className="w-full flex flex-end space-x-4">
                    <button
                        onClick={(e) => setOpenModal(!openModal)}
                        type="button"
                        className="bg-white h-12 rounded-sm text-primary border-gray-200 p-3"
                    >
                        Cancelar
                    </button>
                    <button
                        onClick={deleteItem}
                        type="button"
                        className="bg-primary h-12 rounded-sm text-white p-3"
                    >
                        Eliminar
                    </button>
                </div>
            </Modal.TinyModalFrame>
            <Modal.TinyModalFrame on={addressModal}>
                <AddAddress
                    getAddress={getAddress}
                    closeModalA={closeModalA}
                    user={user}
                ></AddAddress>
            </Modal.TinyModalFrame>
            <Modal.TinyModalFrame on={modalE}>
                <h2>Error </h2>
            </Modal.TinyModalFrame>
        </>
    );
};
export default withRouter(CheckoutComponent);
