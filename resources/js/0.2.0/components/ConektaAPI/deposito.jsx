import React from 'react';

const DepositoPay = ({prevStep, completeOrder}) => {
    return (
        <div>
            <h2 className="text-primary text-2xl">Método de pago</h2>
            Depósito-transferencia bancaria
            Recibirás por correo la información para realizar tu transferencia
            <div className="w-full flex flex-end space-x-4">
                <button
                    className="h-10 p-2 text-primary border rouded-sm"
                    onClick={prevStep}
                >
                    Atrás
                </button>
                <button
                    className="bg-primary h-10 p-2 text-white rounded-sm"
                    onClick={completeOrder}
                >
                    Continuar
                </button>
            </div>
        </div>
    );
}

export default DepositoPay;