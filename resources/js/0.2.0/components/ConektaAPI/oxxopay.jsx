import React from 'react';
import OxxoImage from "../../assets/images/oxxopay.svg"

const OxxoPay = ({prevStep, completeOrder}) => {
    return (
        <div className="p-4 md:p-16">
            <h2 className="text-primary text-2xl">Método de pago</h2>
            <p>Puedes pagar en cualquier tienda Oxxo </p>
            <p>Recibirás la información para hacer tu pago en tu correo electrónico</p>
            <img src={OxxoImage} alt="" className="h-16"/>
            <div className="w-full flex flex-end space-x-4 mt-8">
                <button
                    className="h-10 p-2 text-primary border rouded-sm"
                    onClick={prevStep}
                >
                    Atrás
                </button>
                <button
                    className="bg-primary h-10 p-2 text-white rounded-sm"
                    onClick={completeOrder}
                >
                    Continuar
                </button>
            </div>
        </div>
    );
}

export default OxxoPay;