import React, { useEffect, useState } from "react";
import { StyledForm } from "../forms/StyledForm";

const CardPayment = ({ setToken, tokenizer, prevStep, completeOrder }) => {
    const [ctlD, setCtld] = useState(false);
    
    const onChangeCard = (e) => {
        console.log(e.target.value);
        if (!e.target.value.match(/^[0-9\-]{0,20}$/)) {
            e.target.value = "";
        }
    };
    const onKeyUpCard = (e) => {
        //console.log(e.keyCode);
        if (
            e.which === 8 ||
            e.which === 9 ||
            e.which === 37 ||
            e.which === 39
        ) {
            return;
        }

        if (ctlD && e.which === 86) {
            console.log("pegando");
        }
        const x = e.target.value.split("-").join("");
        //e.target.value = x.split(/^[0-9]{4}/).join("-");
        let valor = "";
        for (let i = 0; i < x.length; i++) {
            valor += x[i];
            if ((i + 1) % 4 === 0 && i < 15) {
                valor += "-";
            }
        }
        e.target.value = valor;
    };
    const onkeydown = (e) => {
        if (
            e.which === 8 ||
            e.which === 9 ||
            e.which === 37 ||
            e.which === 39 ||
            e.which === 116 ||
            (e.which === 86 && ctlD)
        ) {
            return;
        }
        if (!(e.key + "").match(/^[0-9]$/)) {
            e.preventDefault();
        }
        if (e.target.value.length > 18) {
            e.preventDefault(); // target.value = e.target.value.
        }
    };
    /*const tokenizer = (e) => {
        e.preventDefault();
        Conekta.setPublicKey("key_EbZM3eUBtKL86H7qc3ZxtmQ");
        const conektaSuccessResponseHandler = (token) => {
            console.log(token)
            setToken(token.id);
        };
        const conektaErrorResponseHandler = (response) => {
            console.log(response);
        };
        Conekta.Token.create(
            e.target,
            conektaSuccessResponseHandler,
            conektaErrorResponseHandler
        );
    };*/
    useEffect(() => {
        document.onkeydown = (e) => {
            if (e.key === "Control") {
                console.log("control activo");
                setCtld(true);
            }
        };
        document.onkeyup = (e) => {
            if (e.key === "Control") {
                setCtld(false);
            }
        };
    }, []);
    const cardPayment = () => {};
    return (
        <div className="w-full md:pl-8">
            <h2 className="text-primary text-2xl">Método de pago</h2>
            <h3 className="text-secondary text-xl">Pago con tarjeta de crédito o débito</h3>
            <StyledForm className="mt-8 w-full pl-8 pr-8 md:p-8" onSubmit={tokenizer}>
                <div className="w-full md:w-1/3 lg:w-1/3">
                    <div className="flex flex-col">
                        <label className="text-secondary">
                            Nombre del titular
                        </label>
                        <input
                            type="text"
                            size="20"
                            data-conekta="card[name]"
                            required
                        />
                    </div>
                    <div className="flex flex-col">
                        <label className="text-secondary">
                            Número de tarjeta de crédito
                        </label>
                        <input
                            type="text"
                            size="20"
                            data-conekta="card[number]"
                            onKeyDown={onkeydown}
                            onKeyUp={onKeyUpCard}
                            onChange={onChangeCard}
                            required
                        />
                    </div>
                    <div className="">
                        <label className="text-secondary">CVC</label>
                        <input
                            type="text"
                            size="4"
                            maxLength="4"
                            data-conekta="card[cvc]"
                            required
                        />
                    </div>
                    <div className=" flex-wrap  ">
                        <label className="text-secondary">
                            Fecha de expiración
                        </label>
                        <input
                            type="text"
                            size="2"
                            maxLength="2"
                            placeholder="MM"
                            data-conekta="card[exp_month]"
                            required
                        />
                        <span>/</span>
                        <input
                            type="text"
                            size="4"
                            maxLength="4"
                            placeholder="AAAA"
                            data-conekta="card[exp_year]"
                            required
                        />
                    </div>
                </div>
                <div className="w-full flex flex-end space-x-4">
                    <button
                        className="h-12 p-2 text-secondary border rouded-sm modal"
                        onClick={prevStep}
                    >
                        Atrás
                    </button>
                    <button
                        className="bg-primary h-12 p-2 text-white rounded-sm"
                        type="submit"
                    >
                        Continuar
                    </button>
                </div>
            </StyledForm>
        </div>
    );
};

export default CardPayment;
