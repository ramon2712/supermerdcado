import styled from 'styled-components';
import {useState} from "react"
import "./styles.css"

const Div=styled.div`
    opacity: ${({open})=>open?1:0};
    visibility: ${({open})=>open?'visible':'collapse'};
    transition: all 200ms ease-in;
`;

const PopUpSection = props => {
    const[open,setOpen]=useState(false)

    useState(()=>{
        setTimeout(setOpen,2000,true)
    },[])

    return(
        <>
            <Div open={open?1:0} className="w-full md:w-96 h-screen md:h-3/4 bg-black bg-opacity-20 fixed bottom-0 md:bottom-16 right-0 z-40 flex flex-center">
                <div className="w-10/12 p-4 rounded bg-white">
                    <h2 className="text-center mb-1">¡OFERTAS!</h2>
                    <p className="mb-2 md:text-sm">Agradecemos tu visita a nuestro sitio.</p>
                    <p className="mb-2 md:text-sm">Regístrate en nuestra página y obtiene ofertas que solo encontrarás en <strong>www.macep.mx</strong></p>
                    <div className="text-center">
                        <button className="bg-orange text-white rounded px-4 py-1 hover:bg-orange-500" onClick={e=>setOpen(false)}>Cerrar</button>
                    </div>
                </div>
            </Div>
        </>
    )
}

export default PopUpSection