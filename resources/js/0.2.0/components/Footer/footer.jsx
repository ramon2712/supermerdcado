import { Fragment } from "react";


const Footer = props =>{

    return(
        <>
            <div className="relative h-12 w-full bg-white shadow-md">
                <div className="px-8 py-2 flex flex-center h-12 bg-white">
                    <div className="w-full absolute bottom-0 left-0 flex flex-center">
                        <p className="text-xl w-full">Supermercado Almasbarato ©2021</p>
                    </div>
                    {/* <div className="fb-page w-full bg-white lg:w-1/3" data-href="https://www.facebook.com/acerosmacep" data-tabs="timeline" data-height="450" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div> */}
                </div>
            </div>
       </> 
    );
}

export default Footer;