import axios from "axios";
import { useFormik } from "formik";
import { useState } from "react";
import * as Yup from "yup";
import { StyledForm } from "../forms/StyledForm";
const AddAddress = ({ getAddress,  closeModalA, user}) => {
    const [colonias, setColonias] = useState([]);
    const handleSubmit = (values, { setSubmitting }) => {
        setSubmitting(false);
        console.log("Agregando dirección...", values);
        axios
            .post("/api/address", { ...values })
            .then((response) => {
                console.log("nueva dirección: ", response.data);
                getAddress();
                closeModalA();
            })
            .catch((erros) => {
                console.log(erros.response);
            });
    };
    const validateSchema = Yup.object().shape({
        full_name: Yup.string().required(
            "Ingresa el nombre de la persona que recibirá el paquete"
        ),
        email: Yup.string()
            .email("Correo no válido")
            .required("Necesitas ingresar tu correo"),
        mx_zip_code_cp: Yup.string().required(
            "Necesitas ingresar tu código postal"
        ),
        colonia: Yup.string().required("Selecciona tu colonia"),
        street: Yup.string().required(
            "Necesitas ingresar tu dirección con calle y número"
        ),
        description: Yup.string().required(
            "Ingresa la descripción de tu domicilio"
        ),
        phone: Yup.string().required("Necesitas ingresar tu teléfono"),
    });
    const formik = useFormik({
        initialValues: {
            full_name: "",
            email: "",
            zip: "",
            municipio: "",
            state: "",
            colonia: "",
            street: "",
            description: "",
            phone: "",
            main: 0,
            user_id: user.id
        },
        onSubmit: handleSubmit,
        validationSchema: validateSchema,
    });

    const searchZip = (e) => {
        console.log(e.target.value.length);
        if (e.target.value.length === 5) {
            formik.setFieldValue("mx_zip_code_cp", e.target.value);
            axios.get("/api/cp/colonias/" + e.target.value).then((response) => {
                console.log("Colonias: ", response);
                const colonias = response.data[0].colonia.split(";");
                setColonias(colonias.length ? colonias : []);
                colonias.length && formik.setFieldValue("colonia", colonias[0]);
                formik.setFieldValue("municipio", response.data[0].municipio);
                formik.setFieldValue("state", response.data[0].estado);
            });
        }
        
    };

    return (
        <div>
            <h2 className="text-primary text-xl">Agregar dirección</h2>
            <StyledForm
                onSubmit={formik.handleSubmit}
                className="mt-8 w-full pl-8 pr-8"
            >
                <div className="w-full">
                    <label htmlFor="email" className="text-secondary">
                        Nombre completo
                    </label>
                    <div className="pl-8 pr-8">
                        <input
                            name="full_name"
                            type="text"
                            value={formik.values.full_name}
                            onChange={formik.handleChange}
                            className="w-full"
                        ></input>
                        {formik.errors.full_name &&
                            formik.touched.full_name && (
                                <small>{formik.errors.full_name}</small>
                            )}
                    </div>
                    <label htmlFor="email" className="text-secondary">
                        Correo electrónico
                    </label>
                    <div className="pl-8 pr-8">
                        <input
                            name="email"
                            type="email"
                            value={formik.values.email}
                            onChange={formik.handleChange}
                            className="w-full"
                        ></input>
                        {formik.errors.email && formik.touched.email && (
                            <small>{formik.errors.email}</small>
                        )}
                    </div>
                    <div className="flex flex-wrap w-full">
                        <div className="pl-8 pr-8 w-1/5">
                            <label
                                htmlFor="mx_zip_code_cp"
                                className="text-secondary"
                            >
                                C.P.
                            </label>
                            <input
                                name="mx_zip_code_cp"
                                type="text"
                                onChange={searchZip}
                                className="w-full"
                                maxLength="5"
                            ></input>
                            {formik.errors.mx_zip_code_cp &&
                                formik.touched.mx_zip_code_cp && (
                                    <small>
                                        {formik.errors.mx_zip_code_cp}
                                    </small>
                                )}
                        </div>

                        <div className="pl-8 pr-8 w-2/5">
                            <label
                                htmlFor="municipio"
                                className="text-secondary"
                            >
                                Municipio
                            </label>
                            <input
                                name="municipio"
                                type="text"
                                value={formik.values.municipio}
                                onChange={formik.handleChange}
                                className="w-full"
                                readOnly
                            ></input>
                            {formik.errors.municipio &&
                                formik.touched.municipio && (
                                    <small>{formik.errors.municipio}</small>
                                )}
                        </div>
                        <div className="pl-8 pr-8 w-2/5">
                            <label htmlFor="state" className="text-secondary">
                                Estado
                            </label>
                            <input
                                name="estado"
                                type="text"
                                value={formik.values.state}
                                onChange={formik.handleChange}
                                className="w-full"
                                readOnly
                            ></input>
                            {formik.errors.state && formik.touched.state && (
                                <small>{formik.errors.state}</small>
                            )}
                        </div>
                    </div>
                    <label htmlFor="colonia" className="text-secondary">
                        Colonia
                    </label>
                    <div className="pl-8 pr-8">
                        <select
                            name="colonia"
                            type="text"
                            value={formik.values.colonia}
                            onChange={formik.handleChange}
                            className="w-full"
                        >
                            <option disabled>Selecciona la colonia</option>
                            {colonias.map((element, index) => (
                                <option key={index}>{element}</option>
                            ))}
                        </select>
                        {formik.errors.colonia && formik.touched.colonia && (
                            <small>{formik.errors.colonia}</small>
                        )}
                    </div>
                    <label htmlFor="street" className="text-secondary">
                        Dirección de envío
                    </label>
                    <div className="pl-8 pr-8">
                        <input
                            name="street"
                            type="text"
                            value={formik.values.street}
                            onChange={formik.handleChange}
                            className="w-full"
                        ></input>
                        {formik.errors.street && formik.touched.street && (
                            <small>{formik.errors.street}</small>
                        )}
                    </div>
                    <label htmlFor="description" className="text-secondary">
                        Descripción del domicilio
                    </label>
                    <div className="pl-8 pr-8">
                        <input
                            name="description"
                            type="text"
                            value={formik.values.description}
                            onChange={formik.handleChange}
                            className="w-full"
                        ></input>
                        {formik.errors.description &&
                            formik.touched.description && (
                                <small>{formik.errors.description}</small>
                            )}
                    </div>
                    <label htmlFor="phone" className="text-secondary">
                        Teléfono de contacto
                    </label>
                    <div className="pl-8 pr-8">
                        <input
                            name="phone"
                            type="text"
                            value={formik.values.phone}
                            onChange={formik.handleChange}
                            className="w-full"
                        ></input>
                        {formik.errors.phone && formik.touched.phone && (
                            <small>{formik.errors.phone}</small>
                        )}
                    </div>
                </div>
                <div className="w-full flex flex-end space-x-4">
                    <button
                        onClick={closeModalA}
                        className="modal border-sm border-gray-200"
                        type="button"
                    >
                        Cancelar
                    </button>
                    <button
                        type="submit"
                        className="bg-primary h-12 rounded-sm text-white p-3"
                        onClick={(e) => console.log("Clickazo", formik.values)}
                    >
                        Aceptar
                    </button>
                </div>
            </StyledForm>
        </div>
    );
};

export default AddAddress
