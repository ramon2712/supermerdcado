import { useEffect } from "react";

export default ({value,setValue,onFocus,className,max}) => {

    const handleBlur = event => {
        const val=event.target.value;
        if(val.trim()===""){
            setValue(1);
        }
    };
    const add = event => {
        if(value>=max){
            setValue(parseInt(max));
            return
        }
        setValue(parseInt(value)+1);
    };
    const minus = event => {
        if(value===1){
            return;
        }
        setValue(parseInt(value)-1);
    };
    const handleChange = event => {
        const val=(+event.target.value);
        event.preventDefault();
        if(isNaN(val) || (val<=0)){
            setValue(1);
            return;
        }
        if(val>max){
            setValue(parseInt(max));
            return;
        }
        console.log(val)
        setValue(val);
    }

    return(
        <div className="h-8 flex flex-start mr-2">
            <button className="w-8 h-8 flex flex-center rounded-l-full border" onClick={minus}>-</button>
            <input type="number" name="quantity" step="1" min="1" max={parseInt(max)}  value={value} onChange={handleChange} onFocus={onFocus} onBlur={handleBlur} className={'border-t border-b '+className} />
            <button className="w-8 h-8 flex flex-center rounded-r-full border" onClick={add}>+</button>
        </div>
    );
};