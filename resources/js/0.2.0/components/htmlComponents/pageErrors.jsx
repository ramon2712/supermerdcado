import { Link, useHistory } from "react-router-dom"

export default ()=>{

    const history=useHistory()

    return(
        <div className="w-full h-full flex flex-wrap flex-center">
            <div>
                <h2 className="text-3xl text-gray-500 text-center w-full">
                    Error 404 | El recurso solicitado no existe
                </h2>
                <p  className="text-center py-4">
                    <button onClick={()=>{ history.goBack() }} className="text-white bg-primary h-12 rounded-xl p-2 hover:bg-red-400">Regresar</button>
                </p>
            </div>
        </div>
    )
}