import PulseLoader from "react-spinners/PulseLoader";
export default ({loading})=>{
    return(
        <div className="w-full h-full flex flex-wrap flex-center">
            <PulseLoader color={`EE0809`} loading={loading} size={20} />
        </div> 
    );
}