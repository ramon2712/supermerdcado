import styled, { keyframes } from "styled-components";
import { secondaryAltDark, midGray, secondaryAlt, white, greenAlt, focus, lightGray, danger, secondary, focusAlt, greenAltDark, ultralightGray } from "./colorPallete";

export const shadow=`rgba(0, 0, 0, 0) 0px 0px 0px 0px, rgba(0, 0, 0, 0) 0px 0px 0px 0px, rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px`
export const shadowMd=`rgba(0, 0, 0, 0) 0px 0px 0px 0px, rgba(0, 0, 0, 0) 0px 0px 0px 0px, rgba(0, 0, 0, 0.1) 0px 4px 6px -1px,rgba(0, 0, 0, 0.2) 0px 4px 6px -1px `

export const OrderFragmentDiv=styled.div`
    border: solid 1px ${lightGray};
    box-shadow: ${shadow};
    margin-top: 2rem;
    padding: 0 1rem;
    width:100%;
    &:last-child{
        margin-bottom: 2rem;
    }
    & .infoContainer{
        display: ${({isCollapsed})=>(isCollapsed?'none':'flex')};
        max-height: 30rem;
    }
    max-height: ${({isCollapsed})=>(isCollapsed?'4rem':'600px')};
    overflow: ${({isCollapsed})=>(isCollapsed?'hidden':'auto')};
    transition: max-height 300ms ease-in-out;
    .titleContainer{
        height: 2rem;
    }
    .dataContainer{
        max-height: ${({isCollapsed})=>(isCollapsed?0:'30rem')};
        overflow: auto;
        width: 100%;
        transition: max-height 300ms ease-in-out;
    }
    & .buttonContainer{
        display: ${({isCollapsed})=>(isCollapsed?'none':'flex')};
    }

    & .button-collapse{
        &:focus{
            box-shadow:none;
        }
        transform: rotate(${({isCollapsed})=>(isCollapsed?'180deg':'0deg')});
        transition: transform 300ms ease-in-out;
    }
`
export const XButton=styled.button`
    font-size: 0.8rem;
    height: 2rem;
    padding: 0 1.5rem;
    position: relative;
    &.big{
        height: 2.5rem;
        padding: 0 2.5rem;
    }
    &.h-big{
        height: 2.5rem;
    }
    &.forPopUp{
        border-left: none  !important;
    }
    &.absolute{
        position:absolute;
    }
    & span{
        align-items: center;
        display: flex;
        justify-content: center;
        position: relative;
        z-index: 2;  
    }
    & .material-icons-outlined{
        font-size: 1.25rem;
        margin-right: 0.25rem;
    }
    &:before{
        background-color: transparent;
        border-radius: 0.125rem;
        content: "";
        height: 100%;
        left: 0;
        position:absolute;
        top:0;
        width: 0;
        transition: width 200ms ease-in-out;
    }
    &:hover:before{
        width: 100%;
    }
    &.secondary{
        background-color: ${secondaryAlt};
        color: ${white};
    }
    &.secondary:hover:before{
        background-color: ${secondaryAltDark};
    }
    &.alt-blue{
        background-color: ${focus};
        color: ${white};
    }
    &.alt-blue:hover:before{
        background-color: ${focusAlt};
    }
    &.alt-green{
        background-color: ${greenAlt};
        color: ${white};
        &:hover:before{
            background-color: ${greenAltDark};
        }
    }
    &.alt-green-outlined{
        border: solid 1px ${greenAlt};
        color: ${greenAlt};
        &:hover{
            color: ${white};
            &:before{
            background-color: ${greenAlt};
        }}
    }
    &.alt-blue-outlined{
        border: solid 1px ${focus};
        color: ${focus};
        &:hover{
            color: ${white};
            &:before{
            background-color: ${focus};
        }}
    }
    &.alt-gray-outlined{
        border: solid 1px ${lightGray};
        color: ${secondary};
        &:hover{
            color: ${white};
            &:before{
            background-color: ${focus};
        }}
    }
    &.alt-danger-text{
        border: solid 1px transparent;
        color: ${danger};
        &:hover{
            &:before{
                background-color: ${ultralightGray};
            }
        }
    }
    &.secondary-alt-outlined{
        border: solid 1px ${secondaryAlt};
        color: ${secondaryAlt};
        &:hover{
            color: ${white};
            &:before{
            background-color: ${secondaryAlt};
        }}
    }
    &.only-icon{
        &:hover{
            color: ${focus};
        }
        &.material-icons{
            font-size: 1.25rem;
        }
    }
    &.cancel{
        color: ${secondary};
    }
    &.secondary-blank{
        color: ${focus}
    }
    &.invisible{
        margin:0;
        max-height: 0;
        max-width:0;
        overflow: hidden;
        padding:0;
    }
    &:disabled{
        background-color: ${lightGray};
        border-color:${lightGray};
        color: ${white};
        cursor: default;
        &:hover:before{
            background-color: ${lightGray};
        }
    }
`

export const DragDiv=styled.div`
    position: relative;
    & .uploadDiv{
        border: dashed 3px transparent;
        border-radius: 0.5rem;
        color: ${focus};
        transition: border-color 200ms ease,background-color 200ms ease;
    }
    &.drag .uploadDiv{
        border: dashed 3px ${focus};
        background-color: rgba(37,183,204,0.1);
    }
`

export const ImageThumbDiv=styled.div`
    & .deleteImage{
        background-color: ${danger};
        border-radius: 50%;
        color: white;
        height: 1.25rem;
        opacity:0;
        position: absolute;
        right: -0.5rem;
        top: -0.5rem;
        visibility:collapse;
        transition: opacity 200ms ease;
        width: 1.25rem;
        & .material-icons{
            font-size: 1rem;
        }
    }
    &:hover{
        & .deleteImage{
            opacity:1;
            visibility: visible;
        }
    }
    &.addImage{
        border-radius: 0.5rem;
        border: solid 3px ${midGray};
        &:hover{
            border-color: ${focus};
            &> .addImage{
                color: ${focus};
            }
        }
    }
    &> .addImage{
        align-items: center;
        box-shadow: none;
        color: ${midGray};
        display: flex;
        height: 100%;
        justify-content: center;
        width: 100%;
        z-index: 2;
        & .material-icons{
            font-size: 2rem;
        }
    }
`

export const PopupDiv=styled.div`
    visibility: ${({on})=>on?'visible':'hidden'};
    opacity: ${({on})=>on?'1':'0'};
    transition: opacity 200ms ease-in-out;
`