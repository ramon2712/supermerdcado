

export const primary='#ec661c';
export const grayObscure='#64666a';
export const secondary='#696969';
export const light='#e0e0e0';
export const carbon='#333333';
export const midnightBlue='#000033'
export const naviBlue='#000066'
export const focus='#25b7cc';
export const focusAlt='#079eb6';
export const danger='#f5365c';
export const lightGray= '#e9e9e9';
export const midGray= '#cdcdcd';
export const ultralightGray= '#f5f6f7';

export const primaryAlt='#1a3263';
export const secondaryAlt='#f36c0d';
export const secondaryAltDark='#e4670d';
export const greenAlt='#0ab168';
export const greenAltDark='#0d8f57';
export const yellowAlt='#fab95b';
export const white='#ffffff';
export const bgGray100='rgba(243,244,246,1)';