import { useEffect, useRef, useState } from "react"
import styled from "styled-components"
import rollingImage from '../../resources/images/rolling.svg'

const Div=styled.div`
    height: 100%;
    position: relative;
    width: 100%;
    &:focus{
        box-shadow: none;
        outline:none;
    }
    & input{
        background-image:${({refresh})=>(refresh ? `url(${rollingImage})`:'')};
        background-position: calc(100% - 0.5rem) center;
        background-repeat: no-repeat;
        background-size: 1rem 1rem;
    }
    & .dropDownList{
        background-color: white;
        box-shadow: rgba(0, 0, 0, 0) 0px 0px 0px 0px, rgba(0, 0, 0, 0) 0px 0px 0px 0px, rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px;
        left: 0;
        max-height: ${({openDiv})=>(openDiv?'15rem':'0rem')};
        position: absolute;
        overflow: ${({openDiv})=>(openDiv?'auto':'hidden')};
        top: 100%;
        width: 100%;
        z-index: ${({zIndex})=>(zIndex || 10)};
        & ul{
            list-style: none;
        }
        & li{
            cursor: pointer;
            display: block;
            font-size: .8rem;
            padding: 0.25rem 0.5rem;
            width: 100%;
            &:nth-child(odd){
                background-color: rgb(243,244,246)
            }
            &:hover{
                text-decoration: underline;
            }
        }
    }
`

const DataList = ({name,placeholder,className,onChange,inputClassName,labeltext,labelClassName,asyncData,nextTab,refreshPing,setRefreshPing,data=[]}) => {
    const[inputValue,setInputValue]=useState('')
    const[openDiv,setOpenDiv]=useState(false)
    const[loading,setLoading]=useState(false)
    const[init,setInit]=useState(true)
    const[dataLi,setDataLi]=useState([])
    const[st,setSt]=useState(null)
    const listRef=useRef(null)

    const initialize = () =>{
        if(!asyncData){
            setDataLi(data)
        }
        setInit(false)
    }

    const handleChange = event =>{
        setDataLi([])
        setLoading(true)
        if(!asyncData){
            const arr=data.filter(item=>(item.text.search(event.target.value)>=0))
            setDataLi(arr)
            return
        }
        clearTimeout(st)
        setInputValue(event.target.value)
        setSt(setTimeout(async () => {
            const arr=await data(event.target.value)
            setDataLi(arr)
            if(arr){
                setLoading(false)
            }
        }, 500))
    }
    const handleFocus = () =>{
        setOpenDiv(true)
    }
    const handleBlur = () =>{
        setOpenDiv(false)
    }
    const handleSelectClick = (event,id) => {
        event.preventDefault()
        event.stopPropagation()
        setInputValue(event.target.textContent)
        onChange(id)
        if(nextTab.current){
            nextTab.current.focus()
        }
        setOpenDiv(false)
    }

    const handleKeyUp = (event,id=0) => {
        const item=event.target
        if(event.key==='ArrowDown' || event.key==='ArrowUp'){
            const tabIndex=item.tabIndex
            const tabElements=listRef.current.querySelectorAll('.selectWithTab')
            if(tabIndex===0 && event.code==='ArrowUp'){ return }
            tabElements.forEach(t=>{
                if(event.key==='ArrowDown'){
                    if(t.tabIndex===tabIndex+1) t.focus()
                }
                if(event.key==='ArrowUp'){
                    if(t.tabIndex===tabIndex-1) t.focus()
                }
            })
            return
        }
        if(event.key==='Enter'){
            handleSelectClick(event,id)
            return
        }

    }

    useEffect(()=>{
        if(init){
            initialize()
        }
        if(refreshPing){
            setInputValue('')
            setDataLi([])
            setRefreshPing(false)
        }
    },[init,refreshPing])

    return(
        <Div ref={listRef} className={className} openDiv={openDiv} tabIndex="0" onFocus={handleFocus} refresh={loading}
        onBlur={handleBlur}>
            <label className={labelClassName}>{labeltext}</label>
            <input 
                type="text" name={name} 
                value={inputValue} 
                onChange={handleChange} 
                placeholder={placeholder} 
                className={(inputClassName)+" selectWithTab"}
                onKeyUp={handleKeyUp}
                tabIndex="0"
                
            />
            <div className="dropDownList">
                <ul>
                    {dataLi.map((item,index)=>(
                        <li key={index} onClick={event=>handleSelectClick(event,item.id)} onKeyUp={event=>(handleKeyUp(event,item.id))} className="selectWithTab" tabIndex={index+1}>{item.text}</li>
                    ))}
                </ul>
            </div>
        </Div>
    )
}

export default DataList