import styled from "styled-components";
import { bgGray100, carbon, focus, lightGray, primary, primaryAlt, secondary, secondaryAlt } from "./colorPallete";


const AsideComponent=styled.aside`

    transition: width 600ms linear;
    width: ${({pinned})=>(pinned?'4rem':'15rem')};

    & .asideContainer{
        background-color: ${bgGray100};
        height: 100%;
        left: 0;
        overflow: hidden;
        position: absolute;
        top: 0;
        transition: width 300ms ease-in-out;
        width: ${({pinned})=>(pinned?'4rem':'15rem')};
        z-index: 99;
        &:hover{
            width: 15rem;
        }
    }
    & .collapseContainer{
        color: ${({pinned})=>(pinned?'white':secondary)};
    }
    & .menuLi{
        border-left: solid 4px transparent;
        color: ${secondary};
        max-height: 2.5rem;
        overflow: hidden;
        transition: max-height 300ms ease-in;
        &.active{
            border-left: solid 4px ${secondaryAlt};
            color: ${secondaryAlt};
            max-height: 100rem;
            overflow:visible;
            & li{
                color: ${secondary};
                font-size: 0.8rem;
                max-height: 1.5rem;
                padding-bottom: 0.25rem;
                padding-top: 0.25rem; 
                transition: max-height 300ms ease-in;
                &.active a{
                    color: ${primaryAlt};
                    font-weight: 500;
                }
            }
        }
        & li{
            max-height: 0;
            overflow: hidden;
            transition: max-height 300ms ease-in;
        }
    }
`

export default AsideComponent