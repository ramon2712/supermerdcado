import styled, { css, keyframes } from 'styled-components'
import {secondary,danger,focus,focusAlt, light, lightGray, white, carbon, ultralightGray, secondaryAlt,secondaryAltDark} from './colorPallete'
import rollingImage from '../../resources/images/rolling.svg'

const flexCenter=css`align-items:center;justify-content:center;`
const flexCenterBottom=css`align-items:flex-end;justify-content:center;`

/* KEYFRAMES */
const scaling=keyframes`
    0%{
        opacity: 1;
        transform: scale(1);
    }
    50%{
        opacity: 0.2;
        transform: scale(2.5);
    }
    100%{
        opacity: 1;
        transform: scale(1);
    }
`

/* COMPONENTS */
export const StyledForm=styled.form`
    width: 100%;
    & .form-group{
        ${flexCenter}
        display: flex;
        flex-wrap: wrap;
        min-height: 2.5rem;
        margin-bottom: 0.75rem;
        width: 100%;
        &>div{
            ${flexCenter}
            display: flex;
            flex-direction: row-reverse;
            flex-wrap: wrap;
            height :2.5rem;
            border-bottom: solid 1px #cdcdcd;
            width: 100%;
            &:focus-within{
                border-bottom-color: ${focus};
            }
            &.error,&.error:focus-within{
                border-bottom-color: ${danger};
            }
        }
        & input{
            background-color: transparent;
            border: none;
            border-radius: 0;
            color: ${secondary};
            flex-grow: 1;
            height: 2.5rem;
            padding: 0 0.5rem;
            &:focus{
                border-bottom-color: ${focus};
                &+label{
                    color: ${focus};
                }
            }
            &+label{
                color:#cdcdcd;
                ${flexCenter}
                display: flex;
                height: 2.5rem;
                margin: 0;
                width: 2.5rem;
            }
            &.dark{
                color: #f1f2f3;
                &:focus{
                color: #f1f2f3;
                }
            }
            &.loading{
                background-image: url(${rollingImage});
                background-position: calc(100% - 0.5rem) center;
                background-repeat: no-repeat;
                background-size: 1rem 1rem;
            }
        }
        & .signin{
            background-color: ${secondaryAlt};
            color: white;
            ${flexCenter};
            display: flex;
            height:2.5rem;
            text-transform: uppercase;
            width: 100%;
            transition: background-color 200ms ease-in-out;
            &:hover{
                background-color: ${secondaryAltDark};
            }
            & .dot{
                background-color: white;
                border-radius: 50%;
                display: ${({isValidating})=>(isValidating?'inline-block':'none')};
                height: 0.5rem;
                margin: 0 0.5rem;
                width: 0.5rem;
                animation: ${scaling} 1200ms ease infinite;

            }
            & span{
                display: ${({isValidating})=>(!isValidating?'inline-block':'none')};
            }
        }
        & p{
            background-color: rgba(1,1,1,0.2);
            border-radius: 0.25rem;
            ${flexCenter}
            color: rgba(220,38,38,1);
            display:flex;
            margin-top: 0.5rem;
            padding: 0.125rem 0;
            width: 100%;
        }
    }
`;

export const FormStyled=styled.form`
    display: block;
    width: 100%;
    & label{
        color: ${carbon};
        display: block;
        font-size: 0.875rem;
        text-transform: uppercase;
        width: 100%;
        &.small{
            font-size: 0.8rem;
        }
    }
    & div.form-group{
        display: flex;
        flex-wrap: wrap;
        margin-bottom: 1rem;
        width: 100%;
        & input,& textarea{
            border: solid 1px ${lightGray};
            border-radius: 0;
            flex-grow: 1;
            height: 2.5rem;
            margin-right: 0.5rem;
            padding: 0 0.5rem;
            &:last-child, &.no-margin{
                margin-right: 0;
            }
            &:focus{
                border: solid 1px ${focus};
            }
            &:read-only{
                background-color: ${lightGray};
            }
            &.error{
                border-color: ${danger}
            }
            &.flex-grow-0{
                flex-grow: 0;
            }
            &::placeholder{
                color: ${secondary}
            }
        }
        & input{
            &.loading{
                background-image: url(${rollingImage});
                background-position: calc(100% - 0.5rem) center;
                background-repeat: no-repeat;
                background-size: 1rem 1rem;
            }
            &:read-only{
                background-color: ${ultralightGray}
            }
        }
        & textarea{
            height: 10rem;
            &.middle{
                height: 5rem;
            }
        }
        & select{
            background-color: ${white};
            border: solid 1px ${lightGray};
            border-radius: 0;
            flex-grow: 1;
            height: 2.5rem;
            margin-right: 0.5rem;
            padding: 0 0.5rem;
            &:last-child{
                margin-right: 0;
            }
            &.error{
                border-color: ${danger}
            }
        }
        & datalist{
            width: 100%;
        }
    }
`