import {useState, useContext, useEffect} from 'react';
import { useFormik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import AddresForm from './addAddress';
import AppContext from "../../context/Application/AppContext";

const AddressForm = ({address,ShipAddres,setAddress, getAddress, user, deliveryForm, handleNext, handleBack}) => {

  //const {usuario} = useContext(AppContext)
  const [addAddress,setAddAddress] = useState(false);
  const [loading, setLoading] = useState(false);
  const [userAddress, setUserAddress] = useState([]);
  const [colonias, setColonias] = useState([]);
  

    //Funcion para enviar datos del formulario a formik
    const handleSubmit = (values, { setSubmitting }) => {
        setLoading(false);
        setAddAddress(false); 
        setSubmitting(false);
        console.log("Agregando dirección...", values);
        axios
            .post("/api/address", { ...values })
            .then((response) => {
                console.log("nueva dirección: ", response.data);
                getAddress();
            })
            .catch((erros) => {
                console.log(erros.response);
            });
    };

    const validateSchema = Yup.object().shape({
      full_name: Yup.string().required(
          "Ingresa el nombre de la persona que recibirá el paquete"
      ),
      email: Yup.string()
          .email("Correo no válido")
          .required("Necesitas ingresar tu correo"),
      mx_zip_code_cp: Yup.string().required(
          "Necesitas ingresar tu código postal"
      ),
      colonia: Yup.string().required("Selecciona tu colonia"),
      street: Yup.string().required(
          "Necesitas ingresar tu dirección con calle y número"
      ),
      description: Yup.string().required(
          "Ingresa la descripción de tu domicilio"
      ),
      phone: Yup.string().required("Necesitas ingresar tu teléfono"),
    });
    const formik = useFormik({
        initialValues: {
            full_name: "",
            email: "",
            mx_zip_code_cp: "",
            municipio: "",
            state: "",
            colonia: '',
            street: "",
            description: "",
            phone: "",
            main: 0,
            user_id: user.id
        },
        onSubmit: handleSubmit,
        validationSchema: validateSchema,
    });

  const handleAddressChangeButton =() =>{
    setAddAddress(true);
    setLoading(true);
}

const onChangeRadio = (item, e) => {
  if (e.target.checked) {
      console.log("dirección pred: ", item);
      setUserAddress(item);
      ShipAddres(item);
  }
};

const searchZip = (e) => {
  console.log(e.target.value.length);
  if (e.target.value.length === 5) {
      formik.setFieldValue("mx_zip_code_cp", e.target.value);
      axios.get("/api/cp/colonias/" + e.target.value).then((response) => {
          console.log("Colonias: ", response);
          const colonias = response.data[0].colonia.split(";");
          setColonias(colonias.length ? colonias : []);
          colonias.length && formik.setFieldValue("colonia", colonias[0]);
          formik.setFieldValue("municipio", response.data[0].municipio);
          formik.setFieldValue("state", response.data[0].estado);
      });
  }
  console.log(formik.values.colonia);
};

useEffect(() => {

    console.log(deliveryForm);
}, [])

  return (
    <>
    {deliveryForm === '1' ?(
        <Grid container spacing={3}>     
                  {address.map((item_address, index) => (
                    <Grid item xs={6} sm={6}>
                      <div
                          key={index}
                          className="flex flex-wrap items-center pr-4 pl-4 w-full rounded-sm"
                      >
                          <input
                              type="radio"
                              name="address"
                              value={item_address}
                              id={"radio_" + item_address.id}
                              checked={
                                  userAddress.id === item_address.id
                              }
                              onChange={(e) =>
                                  onChangeRadio(item_address, e)
                              }
                          />{" "}
                          <label
                              className="p-4 flex-grow hover:bg-gray-400 checked:bg-gray-200"
                              htmlFor={"radio_" + item_address.id}
                          >
                              <h3 className="text-secondary">
                                  {item_address.street},{" "}
                                  {item_address.mx_zip_code_cp}
                              </h3>

                              <span className="text-secondary">
                                  {item_address.colonia} <br />
                              </span>
                              <p>{item_address.description}</p>
                              <span className="text-secondary">
                                  {item_address.full_name},{" "}
                                  {item_address.phone}
                              </span>
                          </label>
                      </div>
                      </Grid>
                  ))}
        
        <Grid item xs={12} sm={12}>
        {!loading && (
          <Button
                variant="contained"
                color="primary"
                onClick={handleAddressChangeButton}
                className="bg-primary"
                >
                Agregar dirección
            </Button>
        )}
        </Grid>
        {addAddress &&(
            <>
            <form  onSubmit={formik.handleSubmit}
                   className="mt-8 pl-8 pr-8 w-full flex flex-wrap">
              <Grid item xs={12} sm={12}>
                  <TextField
                      value={formik.values.full_name}
                      onChange={formik.handleChange}
                      name="full_name"
                      label="Nombre Completo"
                      fullWidth
                      autoComplete="given-name"
                  />
                  {formik.errors.full_name &&
                    formik.touched.full_name && (
                        <small>{formik.errors.full_name}</small>
                    )}
              </Grid>
              <Grid item xs={12} sm={12}>
                  <TextField
                      value={formik.values.email}
                      onChange={formik.handleChange}
                      name="email"
                      label="Correo Electrónico"
                      fullWidth
                      autoComplete="family-name"
                  />
                  {formik.errors.email && formik.touched.email && (
                      <small>{formik.errors.email}</small>
                  )}
              </Grid>
              <Grid item xs={4} sm={4}>
                  <TextField
                      className="px-2"
                      onChange={searchZip}
                      name="mx_zip_code_cp"
                      label="C.P"
                      fullWidth
                      autoComplete="shipping postal-code"
                  />
                  {formik.errors.cp && formik.touched.cp && (
                      <small>{formik.errors.cp}</small>
                  )}
              </Grid>
              <Grid item xs={4} sm={4}>
                  <TextField
                      value={formik.values.municipio}
                      onChange={formik.handleChange}
                      name="municipio"
                      label="Municipio"
                      fullWidth
                      autoComplete="shipping cp"
                      disabled
                  />
                  {formik.errors.municipio && formik.touched.municipio && (
                      <small>{formik.errors.municipio}</small>
                  )}
              </Grid>
              <Grid item xs={4} sm={4}>
                  <TextField
                      value={formik.values.state}
                      onChange={formik.handleChange}
                      name="state"
                      label="Estado"
                      fullWidth
                      autoComplete="shipping state"
                      disabled
                  />
                  {formik.errors.state && formik.touched.state && (
                      <small>{formik.errors.state}</small>
                  )}
              </Grid>
              <Grid item xs={12} sm={6}>
                <select
                  name="colonia"
                  value={formik.values.colonia}
                  onChange={formik.handleChange}
                  className="pt-4 w-full"
                >
                <option disabled value="">Selecciona una colonia</option>
                  {colonias.map((element, key) => (
                      <option key={key}>{element}</option>
                  ))}
                </select>
                {formik.errors.colonia && formik.touched.colonia && (
                    <small>{formik.errors.colonia}</small>
                )}
              </Grid>

              <Grid item xs={12} sm={6}>
                  <TextField
                      value={formik.values.street}
                      onChange={formik.handleChange}
                      name="street"
                      label="Calle"
                      fullWidth
                  />
                  {formik.errors.street && formik.touched.street && (
                      <small>{formik.errors.street}</small>
                  )}
              </Grid>

              <Grid item xs={12} sm={6}>
                  <TextField
                      value={formik.values.description}
                      onChange={formik.handleChange}
                      name="description"
                      label="Descripción del domicilio"
                      fullWidth
                  />
                  {formik.errors.description &&
                  formik.touched.description && (
                      <small>{formik.errors.description}</small>
                  )}
              </Grid>
              <Grid item xs={12} sm={6}>
                  <TextField
                      value={formik.values.phone}
                      onChange={formik.handleChange}
                      name="phone"
                      label="Teléfono de Contacto"
                      fullWidth
                      autoComplete="shipping phone"
                  />
                  {formik.errors.phone && formik.touched.phone && (
                      <small>{formik.errors.phone}</small>
                  )}
              </Grid>

              <button
                  type="submit"
                  className="bg-primary hover:bg-red-500 h-10 rounded-md px-2 mt-2 text-white shadow-xl"
                  onClick={(e) => console.log("Clickazo", formik.values)}
              >
                  GUARDAR DIRECCIÓN
              </button>
              </form>
            </>
        )}
      </Grid>

    ):
    (
        <Grid container>
            <div>Puedes pasar a reoger el pedido en la siguiente dirección</div>
            <Grid item xs={12} sm={12}>
                <div id="ubicacion" className="my-10">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14682.790234344593!2d-102.7643874!3d23.0715471!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf9c6602792092e9e!2sMACEP%20Aceros!5e0!3m2!1ses!2smx!4v1614015024736!5m2!1ses!2smx" className="border-0 w-full h-64" allowFullScreen="" loading="lazy"></iframe>         
                </div>
            </Grid>
        </Grid>
    )}
    <div className="flex flex-end">
        <button onClick={handleBack} className="bg-gray-400 h-12 px-4 text-white rounded-lg hover:bg-gray-500">
                        ATRÁS
        </button>
        <button
            className="bg-primary h-12 px-4 text-white rounded-lg hover:bg-red-500"
            type="submit"
            onClick={handleNext}
        >
            SIGUIENTE
        </button>
    </div>
     
    </>
  );
}

export default AddressForm;