import { useContext, useEffect, useState, Fragment } from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';


const deliveryForm =({ShipForm, handleNext}) =>{

const useStyles = makeStyles((theme) => ({
    listItem: {
        padding: theme.spacing(1, 0),
    },
    total: {
        fontWeight: 700,
    },
    title: {
        marginTop: theme.spacing(2),
    },
    table: {
        minWidth: 650,
    },
    }));

    const [deliveryForm,setDeliveryForm] = useState('1');
    //console.log(handleNext)

    const handleRadioDelivery = (value) =>{
        setDeliveryForm(value);
        ShipForm(value);
        console.log(value);
    }
    return(
        <>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                    <div className="flex flex-wrap items-center w-auto h-32 rounded-lg shadow-lg hover:bg-gray-100 checked:bg-gray-200">
                        <input type="radio" 
                               name="delivery"
                               id="radioDomicilio" 
                               value='1'
                               checked={deliveryForm==='1'}
                               onChange={(e)=>handleRadioDelivery(e.target.value)}>

                        </input>
                        <label htmlFor="radioDomicilio" className="">
                            <i className="las la-truck text-2xl mx-2"></i>
                            <span>Entrega a domicilio</span>
                        </label>
                        
                    </div>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <div className="flex flex-wrap items-center w-auto h-32 rounded-lg shadow-lg hover:bg-gray-100 ">
                        <input type="radio" 
                               name="delivery" 
                               id="radioTienda"
                               value='2'
                               checked={deliveryForm==='2'}
                               onChange={(e)=>handleRadioDelivery(e.target.value)}>

                        </input>
                        <label htmlFor="radioTienda" className="checked:border-2">
                            <i className="las la-truck text-2xl mx-2"></i>
                            <span>Recoger en tienda</span>
                        </label>
                    </div>
                </Grid>
                {deliveryForm === '2' ?
                
                    (<Grid item xs={12} sm={12}>
                        <span className="font-semi-bold mb-4">Puedes pasar 2 horas después de que se acredite tu pago</span>
                        <h1>Horario de atención</h1>
                        <h1>Lunes a Viernes: 7:00am a 5:00pm  Sábado: 7:00am a 4:00pm</h1>
                        <h1>Teléfono de contacto: 493-134-6545</h1>
                        <h1>Correo electrónico: macepaceros@gmail.com</h1>
                    </Grid>)
                : deliveryForm === '1' &&
                    (<Grid item xs={12} sm={12}>
                        <div className="w-full rounded-lg border-red-400 bg-red-200">
                            El envío será cotizado de acuerdo al área de entrega
                        </div>
                    </Grid>)
                }
                <Grid item xs={12} sm={12}>
                    <button
                        className="bg-primary h-12 px-4 text-white rounded-lg flex flex-end"
                        type="submit"
                        onClick={handleNext}
                    >
                        SIGUIENTE
                    </button>
                </Grid>
            </Grid>
        </>
    )
}

export default deliveryForm;