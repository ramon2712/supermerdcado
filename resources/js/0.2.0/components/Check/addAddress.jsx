import axios from "axios";
import { useFormik } from "formik";
import { useState } from "react";
import * as Yup from "yup";
import Grid from '@material-ui/core/Grid';
import { StyledForm } from "../forms/StyledForm";
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';

const addAddress = ({loading}) => {


    const handleSubmit = () =>{
        
    }
    return(

        <>
            <Grid item xs={12} sm={12}>
                <TextField
                    required
                    id="addressPred"
                    name="addressPred"
                    label="Nombre Completo"
                    fullWidth
                    autoComplete="given-name"
                />
            </Grid>
            <Grid item xs={12} sm={12}>
                <TextField
                    required
                    id="email"
                    name="email"
                    label="Correo Electrónico"
                    fullWidth
                    autoComplete="family-name"
                />
            </Grid>
            <Grid item xs={4} sm={4}>
                <TextField
                    required
                    id="cp"
                    name="cp"
                    label="C.P"
                    fullWidth
                    autoComplete="shipping postal-code"
                />
            </Grid>
            <Grid item xs={4} sm={4}>
                <TextField
                    id="municipio"
                    name="municipio"
                    label="Municipio"
                    fullWidth
                    autoComplete="shipping cp"
                    disabled
                />
            </Grid>
            <Grid item xs={4} sm={4}>
                <TextField
                    id="address1"
                    name="address1"
                    label="Estado"
                    fullWidth
                    autoComplete="shipping state"
                    disabled
                />
            </Grid>
            <Grid item xs={12} sm={12}>
                <TextField
                    required
                    id="street"
                    name="street"
                    label="Calle"
                    fullWidth
                    autoComplete="shipping address-line1"
                />
            </Grid>

            <Grid item xs={12} sm={6}>
                <TextField
                    required
                    id="description"
                    name="description"
                    label="Descripción del domicilio"
                    fullWidth
                    autoComplete="shipping address-level2"
                />
            </Grid>
            <Grid item xs={12} sm={6}>
                <TextField
                    required
                    id="tel"
                    name="tel"
                    label="Teléfono de Contacto"
                    fullWidth
                    autoComplete="shipping celular"
                />
            </Grid>

            <Button
                variant="contained"
                color="primary"
                onClick={handleSubmit}
                >
                Guardar Dirección
            </Button>
        </>
    );
}

export default addAddress;