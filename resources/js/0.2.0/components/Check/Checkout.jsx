import { useContext, useEffect, useState, Fragment } from "react";
import axios from "axios";
import { Link, withRouter, useHistory } from "react-router-dom";
import { Redirect } from "react-router";
import { makeStyles } from '@material-ui/core/styles';
import swal from 'sweetalert';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Typography from '@material-ui/core/Typography';
import AddressForm from "./AddressForm";
import PayForm from './payForm'
import OrderReview from './orderReview'
import AppContext from "../../context/Application/AppContext";
import DeliveryForm from "./deliveryForm"

const Checkout=({match}) => {

  const useStyles = makeStyles((theme) => ({
    appBar: {
      position: 'relative',
    },
    layout: {
      width: 'auto',
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(2),
      [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
        width: 900,
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    paper: {
      marginTop: theme.spacing(3),
      marginBottom: theme.spacing(3),
      padding: theme.spacing(2),
      [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
        marginTop: theme.spacing(6),
        marginBottom: theme.spacing(6),
        padding: theme.spacing(3),
      },
    },
    stepper: {
      padding: theme.spacing(3, 0, 5),
    },
    buttons: {
      display: 'flex',
      justifyContent: 'flex-end',
    },
    button: {
      marginTop: theme.spacing(3),
      marginLeft: theme.spacing(1),
    },
    root: {
      "& .MuiStepIcon-root.MuiStepIcon-active":{
        color: "orange"
    }
    }
  }));

    const classes = useStyles();
    const [errorM,setErrorM] = useState("");
    const history = useHistory();
    const [items, setItems] = useState([]);
    const [total, setTotal] = useState(0);
    const [addAddress,setAddAddress] = useState(false);
    const {user} = useContext(AppContext)
    const [id] = useState(match.params.id);
    const [order, setOrder] = useState([]);
    const [address, setAddress] = useState([]);
    const [payments, setPayments] = useState([]);
    const [userAddress, setUserAddress] = useState([]);
    const [activeStep, setActiveStep] = useState(0);
    const [deliveryForm,setDeliveryForm] = useState('1');
    //const { usuario } = useContext(AppContext);
    const [payMethod, setPayMethod] = useState([]);
    const [inputs, setInputs] = useState({});
    const [token_id, setToken] = useState("");
    const steps = ['Forma de envío','Dirección de envío', 'Forma de pago', 'Revisa tu orden'];

    const tokenizer = (e) => {
      e.preventDefault();
      Conekta.setPublicKey("key_I8sSK27V5isvxXxqCRsxThA");
      const conektaSuccessResponseHandler = (token) => {
          console.log(token.id);
          setToken(token.id);
          console.log("el token es: ", token_id);
          setActiveStep(activeStep + 1);
          //completeOrder(token.id);
          console.log(activeStep)
          /*  */
      };
      const conektaErrorResponseHandler = (response) => {
          swal({
            title: "¡Error al procesar tu tarjeta!",
            text: response.message_to_purchaser,
            icon: "error",
            button: "Aceptar"
          });
          //console.log(response.message_to_purchaser);
          //console.log("Hola");
      };
      Conekta.Token.create(
          e.target,
          conektaSuccessResponseHandler,
          conektaErrorResponseHandler
      );
  };

    const getItems = () => {
      console.log(id)
      //console.log(user);
      console.log(user)
      axios.get("api/orders/" + id).then((response) => {
          setOrder(response.data);
      });
      axios.get("api/orders/products/" + id).then((response) => {
          let inp = {};
          let count = 0;
          response.data.forEach((element) => {
              inp = {
                  ...inp,
                  ["quantity_" + element.id]: element.quantity,
              };
              count = count + element.sale_price * element.quantity;
          });
          setInputs(inp);
          setItems(response.data);
          setTotal(count);
      });
  };
  const getPayments = () => {
      axios.get("api/checkout/payments").then((response) => {
          console.log("payments: ", response.data);
          setPayMethod(response.data[0]);
          setPayments(response.data);
      });
  };
  const getAddress = () => {
      if (user.auth) {
          axios
              .get("/api/address/" + user.id)
              .then((response) => {
                  console.log(response.data);
                  response.data.forEach((element) => {
                      if (element.main) {
                          setUserAddress(element);
                          console.log("El main address es: ", element);
                      }
                  });
                  setAddress(response.data);
              })
              .catch((errors) => {
                  console.log(errors.response);
              });
      }
  };

  const completeOrder = async (token) => {
    let total = 0;
    let count = 0;
    items.forEach((element) => {
        count = count + parseInt(element.quantity, 10);
        total = total + element.sale_price * parseInt(element.quantity, 10);
    });
    let subTotal = total - total * 0.16;
    let orderCheck = {
        user_id: user.id,
        order_status_id: 1,
        order_delivery_periods_id: 1,
        order_types_id: 1,
        user_address_id: userAddress.id,
        products_count: count,
        amount: total,
        taxes: 0.16,
        net_taxes: total * 0.16,
        subtotal: subTotal,
    };
    await axios.put("api/orders/" + id, orderCheck).then((response) => {
        console.log(response);
    });
    if (payMethod.id === 2) {//tarjeta de credito
        let cardpay = {
            token_id: token_id,
            order_id: id,
        };
        console.log("datos a enviar a card: ", cardpay);

        await axios.post("/api/card", cardpay).then((response) => {
            console.log(response);
            if (!response.data.errorStack) {
                console.log(response)
                const conektaid = response.data[0].charges.data[0].order_id;
                console.log("El order_id de conekta es: ", conektaid);
                let schedule = {
                    order_id: id,
                    payment_types_id: 2,
                    payment_status_id: 4,
                    amount: total,
                    subtotal: subTotal,
                    taxes: total * 0.16,
                };
                axios
                    .post("/api/checkout/schedules/", schedule)
                    .then((response) => {
                        console.log(response.data);
                        let payment = {
                            order_payment_schedules_id: response.data.id,
                            payment_status_id: 3,
                            payment_types_id: payMethod.id,
                            paid_amount: total,
                            receipt: conektaid,
                        };
                        axios
                            .post("/api/checkout/order/payments", payment)
                            .then((response) => {
                                console.log(
                                    "payment realzado: ",
                                    response.data
                                );
                            });
                            swal({
                              title: "¡Éxito!",
                              text: 'Se realizó la compra con éxito, puedes ver tus compras en la sección de compras de tu perfil',
                              icon: "success",
                              buttons: false,
                              timer: 6000,
                            });
                            setTimeout(() => {
                              history.push("/mi-cuenta/compras"); 
                            }, 6000);
                    });
            } else {
                console.log("error: ", response);
                const errors = JSON.parse(response.data.errorStack);
                setErrorM(errors.details[0].message);
                console.log("mensaje error: ", errorM);
            }
        });
    } else if (payMethod.id === 4) {
        let oxxopay = {
            order_id: id,
        };
        axios.post("api/oxxo", oxxopay).then((response) => {
            console.log(response);
            const conektaid = response.data.charges.data[0].order_id;
            let reference= response.data.charges.data[0].payment_method.reference;
            let schedule = {
                order_id: id,
                payment_types_id: payMethod.id,
                payment_status_id: 1,
                amount: total,
                subtotal: subTotal,
                taxes: total * 0.16,
            };
            axios
                .post("/api/checkout/schedules/", schedule)
                .then((response) => {
                    console.log("Schedule: ", response.data);
                    let payment = {
                        order_payment_schedules_id: response.data.id,
                        payment_status_id: 1,
                        payment_types_id: 4,
                        paid_amount: total,
                        receipt: conektaid,
                    };
                    axios
                        .post("/api/checkout/order/payments", payment)
                        .then((response) => {
                            console.log(
                                "payment realzado: ",
                                response.data
                            );
                            axios.post('sendScheduleOxxo', {reference,total}).then(response=>{
                              console.log(response.data.products)
                            }).catch(error =>{
                                console.log(error.response.data)
                            })
                            swal({
                              title: "¡Éxito!",
                              text: 'Se realizó la compra con éxito. Revisa tu correo con las instrucciones, puedes ver el estado de tu compra en la sección de compras de tu perfil',
                              icon: "success",
                              buttons: false,
                              timer: 6000,
                            });
                            setTimeout(() => {
                              history.push("/mi-cuenta/compras"); 
                            }, 6000);
                        });
                });
        });
    } else if (payMethod.id === 1) 
    { //Transferencia Bancaria
      let speipay = {
        order_id: id,
      };

      await axios.post("/api/spei", speipay).then((response) => {
        console.log("SpeiPay",response);
        console.log("SpeiPay Clabe: ",response.data[0].charges.data[0].payment_method.clabe);
        console.log("SpeiPay Bank: ",response.data[0].charges.data[0].payment_method.bank);
        let clabe= response.data[0].charges.data[0].payment_method.clabe;
        let bank= response.data[0].charges.data[0].payment_method.bank;

        let schedule = {
          order_id: id,
          payment_types_id: 1,
          payment_status_id: 1,
          amount: total,
          subtotal: subTotal,
          taxes: total * 0.16,
      };

        axios.post("/api/checkout/schedules/", schedule).then((response) => {
          console.log("Schedule: ", response.data);
          let payment = {
              order_payment_schedules_id: response.data.id,
              payment_status_id: 1,
              payment_types_id: 1,
              paid_amount: total,
          };
          axios
              .post("/api/checkout/order/payments", payment)
              .then((response) => {
                  console.log("payment realzado: ", response.data);
              });

              axios.post('sendScheduleSpei', {clabe,total,bank}).then(response=>{
                console.log(response)
              }).catch(error =>{
                  console.log(error.response)
              })  
              swal({
                title: "¡Éxito!",
                text: 'Se realizó la compra con éxito. Revisa tu correo con las instrucciones, puedes ver el estado de tu compra en la sección de compras de tu perfil',
                icon: "success",
                buttons: false,
                timer: 6000,
              });
              setTimeout(() => {
                history.push("/mi-cuenta/compras"); 
              }, 6000);
      });
      });
        
      }
    //  console.log("orden modificada: ",orderCheck)
};

//Este metodo recibe la direccion seleccionada del componente AddresForm e inicializa userAddress
const ShipAddres =(item)=>{
    setUserAddress(item);
}
//Este metodo recibe el tipo de pago seleccionado del componente payForm e inicializa payMethod
const ShipPay =(shipPayMethod)=>{
  setPayMethod(shipPayMethod);
}

//Este metodo recibe el tipo de envío que se realizará y lo inicializa en deliveryForm
const ShipForm =(shipFormMethod)=>{
  setDeliveryForm(shipFormMethod);
}

useEffect(() => {
  ShipForm(deliveryForm);
}, [])
  function getStepContent(step) {
    switch (step) {
      case 0:
        return <DeliveryForm ShipForm={ShipForm}
                             handleNext={handleNext}
                             handleBack={handleBack}
        />
      case 1: 
        return <AddressForm address={address} 
                            ShipAddres={ShipAddres}
                            setAddress={setAddress} 
                            getAddress={getAddress}
                            user={user}
                            deliveryForm={deliveryForm}
                            handleNext={handleNext} 
                            handleBack={handleBack}
                            /> ;
      case 2:
        return <PayForm shipPay={ShipPay}
                        payments={payments} 
                        pays={payMethod} 
                        tokenizer={tokenizer}
                        completeOrder={completeOrder}
                        activeStep={setActiveStep}
                        handleNext={handleNext}
                        handleBack={handleBack}
                        />;
      case 3:
        return <OrderReview 
                            pays={payMethod} 
                            setToken={setToken}
                            tokenizer={tokenizer}
                            completeOrder={completeOrder}
                            items={items}
                            userAddress={userAddress}
                            ShipForm={deliveryForm}
                            activeStep={setActiveStep} 
                            handleBack={handleBack}
                            handleNext={handleNext} 
                            />;
      default:
        throw new Error('Unknown step');
    }
  }

  const handleNext = () => {
    setActiveStep(activeStep + 1);
  };

  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };
  


  useEffect(() => {
    getItems();
    getAddress();
    getPayments();
    console.log("direccion "+userAddress);
},[user]);

//Se verifica que la orden no se encuentre en la tabla de las órdenes
useEffect(() => {

  axios.post("orderSchedule", {id:match.params.id}).then((response) => {
    //console.log(response.data);
    if(response.data.id === "1"){
      console.log("ya existe la compra");
      swal({
        title: "Redirigiendo...",
        text: 'Está orden ya se encuentra en proceso de pago, se redigirá a tus compras',
        icon: "error",
        buttons: false,
        timer: 5000,
      });
      setTimeout(() => {
        history.push("/mi-cuenta/compras");
      }, 5000);

    } else{
      console.log("No existe la compra");
    }
  })
}, [match.params.id])

  return (
    <Fragment>
      <main className={classes.layout}>
        <Paper className={classes.paper}>
          <Typography component="h1" variant="h4" align="center">
            Continua con tu compra
          </Typography>
          <Stepper activeStep={activeStep} className={classes.stepper}>
            {steps.map((label) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
          <Fragment>
            {activeStep === steps.length ? (
              <Fragment>
                <Typography variant="h5" gutterBottom>
                  ¡Gracias por su compra!
                </Typography>
                <Typography variant="subtitle1">
                  Your order number is #2001539. We have emailed your order confirmation, and will
                  send you an update when your order has shipped.
                </Typography>
              </Fragment>
            ) : (
              <Fragment>
                {getStepContent(activeStep)}
              </Fragment>
            )}
          </Fragment>
        </Paper>
      </main>
    </Fragment>
  );
}

export default withRouter(Checkout);