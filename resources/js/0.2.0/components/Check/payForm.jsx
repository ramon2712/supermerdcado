import {useState, useContext, useEffect} from 'react';
import {StyledForm} from "../forms/StyledForm"
import Grid from '@material-ui/core/Grid';

const payForm = ({shipPay,payments, pays, tokenizer, completeOrder, handleBack, handleNext}) =>{

    const [payMethod, setPayMethod] = useState(pays);
    const [ctlD, setCtld] = useState(false);
    const [showCard,setShowCard] = useState(true);
    
    //console.log(pays)
    //console.log(payMethod);
    //console.log(payments)
    const handleSubmitCard = () =>{
        setShowCard(false);
    }
    const onChangePayment = (item, e) => {
        if (e.target.checked) {
            console.log("Método de pago: ", item);
            setPayMethod(item);
            shipPay(item);
        }
    };

    //Aqui empiezan los metodos para el formulario de la tarjeta
    const onChangeCard = (e) => {
        console.log(e.target.value);
        if (!e.target.value.match(/^[0-9\-]{0,20}$/)) {
            e.target.value = "";
        }
    };
    const onKeyUpCard = (e) => {
        //console.log(e.keyCode);
        if (
            e.which === 8 ||
            e.which === 9 ||
            e.which === 37 ||
            e.which === 39
        ) {
            return;
        }

        if (ctlD && e.which === 86) {
            console.log("pegando");
        }
        const x = e.target.value.split("-").join("");
        //e.target.value = x.split(/^[0-9]{4}/).join("-");
        let valor = "";
        for (let i = 0; i < x.length; i++) {
            valor += x[i];
            if ((i + 1) % 4 === 0 && i < 15) {
                valor += "-";
            }
        }
        e.target.value = valor;
    };
    const onkeydown = (e) => {
        if (
            e.which === 8 ||
            e.which === 9 ||
            e.which === 37 ||
            e.which === 39 ||
            e.which === 116 ||
            (e.which === 86 && ctlD)
        ) {
            return;
        }
        if (!(e.key + "").match(/^[0-9]$/)) {
            e.preventDefault();
        }
        if (e.target.value.length > 18) {
            e.preventDefault(); // target.value = e.target.value.
        }
    };

    return(
        <>
        <Grid container spacing={3}>
        
            {payments.map((element, index) => (
                <Grid item xs={6} sm={12}>
                <div key={index} >
                    <input
                        type="radio"
                        name="address"
                        value={element}
                        id={"radio_" + element.id}
                        checked={payMethod.id === element.id}
                        onChange={(e) =>
                            onChangePayment(element, e)
                        }
                        id={"pay_radio_" + element.id}
                        className="checked:bg-blue-600"
                    />
                    <label
                        htmlFor={"pay_radio_" + element.id}
                        className="items-center hover:bg-secondary w-full"
                    >
                        {element.id === 2 ? (<i className="pl-2 las la-credit-card"></i>)
                                          : <i className="pl-2 las la-money-check-alt"></i>}   
                        <span className="pl-2 md:w-1/4">{element.name}</span>
                        
                    </label>

                </div>
                </Grid>
            ))}
            <Grid container spacing={3}>
                {showCard && (        
                payMethod.id === 2 ? (
                    <>
                    <StyledForm className="mt-5 w-full pl-8 pr-8 md:p-8" onSubmit={tokenizer}>
                        <Grid item xs={12} sm={6}>
                            <div className="flex flex-col">
                                <label className="text-secondary">
                                    Nombre del titular
                                </label>
                                <input
                                    type="text"
                                    size="20"
                                    data-conekta="card[name]"
                                    required
                                />
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <div className="flex flex-col mt-2">
                                <label className="text-secondary">
                                    Número de tarjeta de crédito
                                </label>
                                <input
                                    type="text"
                                    size="20"
                                    data-conekta="card[number]"
                                    onKeyDown={onkeydown}
                                    onKeyUp={onKeyUpCard}
                                    onChange={onChangeCard}
                                    required
                                />
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Grid item xs={12} sm={3}>
                            <div className="inline-flex mt-2">
                                <div className="flex flex-col mr-5 w-1/2">
                                    <label className="text-secondary">CVC</label>
                                    <input
                                        type="text"
                                        size="4"
                                        maxLength="4"
                                        data-conekta="card[cvc]"
                                        required
                                    />  
                                </div>
                                <div className="flex flex-col">
                                    <label className="text-secondary">
                                        Fecha de expiración
                                    </label>
                                    <div className="inline-flex">
                                        <input
                                            type="text"
                                            size="6"
                                            maxLength="2"
                                            placeholder="MM"
                                            data-conekta="card[exp_month]"
                                            required
                                        />
                                        <span className="mx-3 mt-2">/</span>
                                        <input
                                            type="text"
                                            size="8"
                                            maxLength="4"
                                            placeholder="AAAA"
                                            data-conekta="card[exp_year]"
                                            required
                                        />
                                    </div>
                                </div>
                            </div>
                            </Grid>
                        </Grid>    
                        <div className="flex flex-end">
                            <button onClick={handleBack} className="bg-gray-400 h-12 px-4 text-white rounded-lg hover:bg-gray-500">
                                            ATRÁS
                            </button>
                            <button
                                className="bg-primary h-12 px-4 text-white rounded-lg hover:bg-red-500"
                                type="submit"
                                onClick={()=>handleNext}
                            >
                                SIGUIENTE
                            </button>
                        </div>
                    </StyledForm>
                    </>
                ): <>
                    <Grid item xs={12} sm={6}>
                        <h1>Se mandarán a tu correo los pasos a seguir para el pago</h1>
                    </Grid>
                    
                    <div className="flex flex-end space-x-4">
                        <button onClick={handleBack} className="bg-gray-400 h-12 px-4 text-white rounded-lg hover:bg-gray-500">
                                        ATRÁS
                        </button>
                        <button
                            className="bg-primary h-12 px-4 text-white rounded-lg hover:bg-red-500"
                            type="button"
                            onClick={handleNext}
                        >
                            SIGUIENTE
                        </button>
                    </div>
                    </>)} 

                    
            </Grid>
        </Grid>
        
    </>
    );
}

export default payForm;
