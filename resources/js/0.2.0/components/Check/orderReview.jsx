import React from 'react';
import {useState, useContext, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

const useStyles = makeStyles((theme) => ({
  listItem: {
    padding: theme.spacing(1, 0),
  },
  total: {
    fontWeight: 700,
  },
  title: {
    marginTop: theme.spacing(2),
  },
  table: {
    minWidth: 650,
  },
}));

const orderReview=({pays, setToken, tokenizer, completeOrder, items, userAddress, ShipForm,handleBack,handleNext}) =>{
  
    const classes = useStyles();
    const [reciept,setReciept] = useState(false);

    console.log(items);
    console.log(userAddress);
    console.log(pays);
    console.log(ShipForm);

    const handleChangeReciept =value=>{
      setReciept(value);
      //console.log(e.target.checked);
    }

    const validarRFC = (e) => {
      console.log(e.target.value)
      const rfc_pattern_pm = "^(([A-ZÑ&]{3})([0-9]{2})([0][13578]|[1][02])(([0][1-9]|[12][\\d])|[3][01])([A-Z0-9]{3}))|" +
      "(([A-ZÑ&]{3})([0-9]{2})([0][13456789]|[1][012])(([0][1-9]|[12][\\d])|[3][0])([A-Z0-9]{3}))|" +
      "(([A-ZÑ&]{3})([02468][048]|[13579][26])[0][2]([0][1-9]|[12][\\d])([A-Z0-9]{3}))|" +
      "(([A-ZÑ&]{3})([0-9]{2})[0][2]([0][1-9]|[1][0-9]|[2][0-8])([A-Z0-9]{3}))$";
      // patron del RFC, persona fisica
      const rfc_pattern_pf = "^(([A-ZÑ&]{4})([0-9]{2})([0][13578]|[1][02])(([0][1-9]|[12][\\d])|[3][01])([A-Z0-9]{3}))|" +
          "(([A-ZÑ&]{4})([0-9]{2})([0][13456789]|[1][012])(([0][1-9]|[12][\\d])|[3][0])([A-Z0-9]{3}))|" +
          "(([A-ZÑ&]{4})([02468][048]|[13579][26])[0][2]([0][1-9]|[12][\\d])([A-Z0-9]{3}))|" +
          "(([A-ZÑ&]{4})([0-9]{2})[0][2]([0][1-9]|[1][0-9]|[2][0-8])([A-Z0-9]{3}))$";

      if (e.target.value.match(rfc_pattern_pm) || e.target.value.match(rfc_pattern_pf))
        console.log("RFC Correcto")

    }

    useEffect(() => {
      console.log(reciept)
    }, [reciept])
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Detalles de tu compra
      </Typography>
      <TableContainer component={Paper}>
      <Table className={classes.table} size="small" >
        <TableHead>
          <TableRow>
            <TableCell>Nombre del producto</TableCell>
            <TableCell align="right">Cant</TableCell>
            <TableCell align="right">Subtotal</TableCell>
            <TableCell align="right">Total</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {items.map((row,index) => (
            <TableRow key={index}>
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.quantity}</TableCell>
              <TableCell align="right">${row.sale_price}</TableCell>
              <TableCell align="right">${(row.quantity)*(row.sale_price)}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
      {/* <List >
        {items.map((product,index) => (
          <ListItem className={classes.listItem} key={index}>
            <Typography variant="body2">{product.name}</Typography>
            <Typography variant="body2">${product.sale_price}</Typography>
          </ListItem>
        ))}
        <ListItem className={classes.listItem}>
          <ListItemText primary="Total" />
          <Typography variant="subtitle1" className={classes.total}>
            $34.06
          </Typography>
        </ListItem>
      </List> */}
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6}>
          <Typography variant="h6" gutterBottom className={classes.title}>
            Dirección a enviar
          </Typography>
          {ShipForm === '1' ?
            <>
            <Typography variant="body2" gutterBottom>{userAddress.full_name}</Typography>
            <Typography variant="body2" gutterBottom>{userAddress.colonia}{", "}{userAddress.mx_zip_code_cp}</Typography>
            <Typography variant="body2" gutterBottom>{userAddress.street}</Typography> 
            </>
          : ShipForm === '2' &&
            <Typography variant="body2" gutterBottom>El pedido será recogido en tienda</Typography> }
        </Grid>
        <Grid item xs={12} sm={6}>
          <Typography variant="h6" gutterBottom className={classes.title}>
            Detalles del pago
          </Typography>
          <Typography variant="body2"  gutterBottom>Tipo: {pays.name}</Typography>
          <Typography variant="body2"  gutterBottom>Descripción: {pays.description}</Typography>
        </Grid>
        <Grid item xs={12} sm={6}>
          <FormControlLabel
            control={
              <Switch
                checked={reciept}
                onChange={(e)=>handleChangeReciept(e.target.checked)}
                name="checkedB"
                color="primary"
              />
            }
            label="Requiero Factura"
          />
        </Grid>
        {reciept && 
          <Grid item xs={12} sm={6}>
            <TextField id="reciept" label="Introduce tu RFC" onChange={validarRFC}/>
          </Grid>}
          <Grid item xs={12} sm={12}>
          <div className="flex flex-end">
            <button onClick={handleBack} className="bg-gray-400 h-12 px-4 text-white rounded-lg hover:bg-gray-500">
                            ATRÁS
            </button>
            <button
                className="bg-primary h-12 px-4 text-white rounded-lg hover:bg-red-500"
                type="submit"
                onClick={completeOrder}
            >
                FINALIZAR
            </button>
          </div>
          </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default orderReview;