import styled, { keyframes } from "styled-components";

const spin=keyframes`
    100%{
        transform: rotate(360deg);
    }
`;

export const StyledForm=styled.form`
    & .form-group{
        align-items:center;
        display: flex;
        flex-wrap: wrap;
        justify-content: flex-start;
        margin-bottom: 0.5rem;
        & input{
            flex-grow: 1;
        }
        & button{
            height: 2.125rem;
            margin-top: 0;
        }
    }
    & input{
        border-radius: 0.125rem;
        border-width:0;
        border-bottom: solid 1px #e0e0e0;
        font-size: 0.85rem;
        height: 2.125rem;
        flex-grow: 1;
        &:read-only{
            background-color: #efefef;
        }
        &[type="checkbox"]{
            display:none;
            & + label{
                align-items: center;
                display: flex;
                justify-content: flex-start;
                & span{
                    align-items: center;
                    background-color: rgba(240,240,240,1);
                    border-radius: 0.1rem;
                    color: rgba(240,240,240,1);
                    display: inline-flex;
                    height: 1rem;
                    justify-content: center;
                    margin-right: 0.25rem;
                    transition: background-color 200ms ease-in-out,color 200ms ease-in-out;
                    width: 1rem;
                    &::after{
                        content: "\\2713";
                        color: rgba(240,240,240,1);
                        transition: color 200ms ease-in-out;
                    }
                }
            }
            &:checked{
                & + label span{
                    background-color: #ee0809;
                    color: white;
                    &::after{
                        color: white;
                    }
                }
            }
        }
    }
    & label{
        font-size: 0.75rem;
        margin: 0;
    }
    & button{
        background: #ffda00;
        height: 2.5rem;
        padding: 5px;
        margin-top: 0.5rem;
        border-radius: 0.125rem;
        color: #ee0809;
        &.modal{
            border-radius:0.125rem;
            background-color:white;
            padding:5px;
            color:#ee0809;
            border-color: rgba(229, 231, 235, var(1));
        }
        &.dashButton{
            font-size: 0.75rem;
            padding: 0 1rem;
            &.cancel{
                background-color: #efefef;
                color: #646464;
                margin-right: 0.5rem;
            }
            &.outlined{
                background-color: transparent;
                border: solid 1px #ee0809;
                color: #ee0809;
            }
        }
        &.simple{
            background-color: #efefef;
            color: #646464;
            padding: 0 1rem;
        }
    }
    & small{
        color: red;
        &.border-0{
            border:none;
        }
    }
    & select{
        border: solid 1px #e0e0e0;
        height: 2.5rem;
        flex-grow: 1;
        border-radius: 0.25rem;
        background-color: #ffffff;
    }
    & .loading-spin{
        border: solid 2px #ee0809;
        border-left-color: #efefef;
        border-radius: 50%;
        height: 2rem;
        margin-top: 0.5rem;
        width: 2rem;
        animation: ${spin} 1.5s linear infinite;
    }
`