import { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom"
import slug from "../../../slug"
import styledPrices from "../../../Utils/styledPrices";
import AppContext from "../../context/Application/AppContext";
import InputNumber from "../htmlComponents/InputNumber"

export default ({product,tam}) => {
    const [description, setDescription] = useState('');

    const {addToCart}=useContext(AppContext);
    const [quantity,setQuantity]=useState(1);
    const [disabled,setDisabled] = useState(false);

    useEffect(()=>{
        //console.log(tam);
        setDescription(product.description.split(' ').filter((word,index)=>index<21).join(' '));
        if(product.stock <1) setDisabled(true);
    },[]);
    return (  
        <>
        <div className={`bg-white rounded-xl inline-flex mt-2 sm:w-1/2 md:w-1/2 lg:${tam} align-top shadow-sm hover:shadow-xl h-108 overflow-hidden`}>
            <div className="w-full flex flex-col rounded-sm whitespace-normal">
                <div className="h-2/6 w-full flex flex-center relative">
                    <Link to={`/producto/detalles/${product.id}-${slug(product.name)}`} className="w-full h-full block absolute left-0 top-0" ></Link>
                        <img src={`product/departaments/${product.filename}`} className="h-full" />
                </div>
                <div className="w-full h-1/6 flex flex-col mt-3 p-3">
                    <p className="w-full h-auto max-h-30 text-dark-100 font-bold">
                        <Link to={`/producto/detalles/${product.id}-${slug(product.name)}`} className="" >
                            {product.name}
                        </Link>
                    </p>
                    <span className="text-sm lg:text-sm text-green-500 ml-2">{product.brand_name}</span> 
                </div>
                    
                <div className="w-full h-1/6 flex flex-col mt-11 px-3">
                    <div className="w-full h-10 flex flex-center ">
                            <span className="text-2xl lg:text-xl">$ {styledPrices(product.net_sale_price)} <span className="text-sm">{product.unit_name}</span></span>                     
                    </div>
                </div>
                <div className="w-full h-1/6 inline-flex flex flex-center">
                    <InputNumber value={quantity} setValue={setQuantity} max={product.stock} min={0} className="h-full text-center w-12" />
                    <strong className="mx-2 text-sm flex-center">{disabled ? "" : `$`+quantity*product.net_sale_price}</strong>
                </div>
                <div className="w-full h-1/6 flex flex-col p-2">
                        <button className={`rounded-xl text white btn w-full h-32 text-sm ${disabled? "bg-secondary text-white" : "btn-primary"}`} onClick={e=>{addToCart(product.id,quantity)}} disabled={disabled}>
                            <i className="las la-luggage-cart text-2xl mr-2"></i>
                            {disabled ? "No disponible" :"Agregar al carrito "}
                        </button>                           
                </div>   
            </div> 
        </div>  
        </>
    )
}