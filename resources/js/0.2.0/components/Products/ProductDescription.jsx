import axios from "axios";
import { useEffect, useState } from "react"

export default ({productId,description})=> {
    const [datasheet,setDatasheet]=useState([]);
    const [localDescription,setLocalDescription]=useState([]);
    
    const[loading,setLoading]=useState(true);

    const loadData = () =>{
        axios.post('product/datasheet',{productId})
        .then(response=>{
            setLoading(false);
            setDatasheet(response.data.datasheet);
        })
        .catch(error=>{
            setLoading(false);/*  */
            console.log(error)
        });
    };

    useEffect(()=>{
        if(productId !== 0 && loading){
            loadData();
        }
        setLocalDescription(description.split('\n'));
    },[productId,description]);

    return(
        <div className="w-full flex flex-wrap px-2 mt-12">
            <div className="w-full lg:w-1/2 lg:pr-2 mg-2">
                <h2 className="text-dark-900 text-xl font-normal">Descripción</h2>
                <div className="text-gray-600">
                    {localDescription.map((line,index)=>(
                        <p key={index} className="mb-2">{line}</p>
                    ))}
                </div>
            </div>
            <div className="w-full lg:w-1/2 lg:pl-2">
            {(datasheet.length>0) &&
                <table className="border-collapse w-full">
                    <thead>
                        <tr>
                            <th colSpan="2" className="text-left text-gray-100 text-dark-100 text-xl">Especificaciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {datasheet.map((item,index)=>(
                            <tr key={index}>
                                <td className="w-1/2 border text-sm py-3 pl-2 bg-gray-100">{item.name}</td>
                                <td className="w-1/2 border text-sm py-3 pl-2">{item.value}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                }
            </div>
        </div>
    )
}