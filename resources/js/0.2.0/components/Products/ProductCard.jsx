import { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom"
import slug from "../../../slug"
import styledPrices from "../../../Utils/styledPrices";
import AppContext from "../../context/Application/AppContext";
import "./style.css";
export default ({product}) => {
    const [description, setDescription] = useState('');

    const {addToCart}=useContext(AppContext);

    useEffect(()=>{
        setDescription(product.description.split(' ').filter((word,index)=>index<21).join(' '));
    },[]);
    return (  
        <>
        <div className="product bg-white rounded-lg inline-flex mt-2 md:w-1/2 lg:w-1/4 align-top shadow-md h-108 overflow-hidden">
            <div className="w-full h-full flex flex-col rounded-sm whitespace-normal">
                <div className="h-1/2 w-full flex flex-center relative">
                <Link to={`/producto/detalles/${product.id}-${slug(product.name)}`} className="w-full h-full block absolute left-0 top-0" ></Link>
                    <img src={`product/images/512x512/${product.filename}`} className="h-5/6" />
                </div>
                <div className="w-full h-1/2 px-2 flex flex-col">
                    <p className="w-full h-auto max-h-32 overflow-hidden overflow-ellipsis text-2xl lg:text-lg text-dark-100 font-black">
                    <Link to={`/producto/detalles/${product.id}-${slug(product.name)}`} className="" >
                        {product.name}
                    </Link>
                    </p>
                    <div className="w-full flex-grow">
                        <p className="text-secondary text-base lg:text-xs">
                            {description}
                        </p>
                    </div>
                    <div className="w-full h-14 flex flex-start">
                        <div className="h-full flex-grow flex flex-start">
                            <span className="text-3xl lg:text-xl">$ {styledPrices(product.net_sale_price)}</span>
                        </div>
                        <button className="btn btn-primary h-10 px-10 flex flex-center text-sm rounded-sm" onClick={e=>{addToCart(product.id)}}>
                            <i className="las la-luggage-cart text-xl mr-2"></i>
                            <span>Añadir</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}