import { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom"
import slug from "../../../slug"
import styledPrices from "../../../Utils/styledPrices";
import AppContext from "../../context/Application/AppContext";
import ReactTooltip from 'react-tooltip';

export default ({product}) => {
    const [description, setDescription] = useState('');

    const {addToCart}=useContext(AppContext);

    useEffect(()=>{
        setDescription(product.description.split(' ').filter((word,index)=>index<21).join(' '));
    },[]);
    return (
        <>
        <div className="inline-flex h-full w-full md:w-1/2 lg:w-1/4 p-2 align-top">
            <div className="w-full h-full flex flex-start bg-white border-b rounded-sm shadow-md whitespace-normal">
                <div className="h-28 w-28 flex flex-center relative">
                    <Link to={`/producto/detalles/${product.id}-${slug(product.name)}`} className="w-full h-full block absolute left-0 top-0" ></Link>
                    <img src={`product/departaments/${product.filename}`} className="h-5/6" />
                </div>
                <div className="w-0 flex-grow h-full px-2 pt-1 flex flex-col">
                    <p className="w-full h-auto max-h-32 overflow-hidden overflow-ellipsis text-base lg:text-sm text-dark-100 font-semibold">
                        <Link to={`/producto/detalles/${product.id}-${slug(product.name)}`} className="" >
                            {product.name}
                        </Link>
                    </p>
                    <div className="w-full h-0 flex-grow"></div>
                    <div className="w-full h-14 flex flex-start flex-between">
                        <div className="h-full flex-grow flex flex-start">
                            <span className="text-2xl lg:text-base">$ {styledPrices(product.net_sale_price)}</span>
                        </div>
                        <button className="h-auto flex flex-center px-1 btn-primary rounded-full text-sm" onClick={e=>{addToCart(product.id)}}>
                            <i className="las la-luggage-cart text-xl p-2" data-tip="Añadir este producto al carrito."></i>
                        </button>
                        <ReactTooltip/>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}