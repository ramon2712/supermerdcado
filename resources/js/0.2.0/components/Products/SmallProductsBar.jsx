import axios from "axios";
import { useEffect, useRef, useState } from "react";
import SmallProductCard from "./SmallProductCard";

const SmallProductsBar = ({type,productId=0}) => {
    const[productList,setProductList]=useState([]);
    const[loading,setLoading]=useState(true);
    const[title,setTitle]=useState('Cargando . . .');

    const[touchPosition,setTouchPosition]=useState(0);
    const[position,setPosition]=useState(0);
    const productsRef=useRef();

    const titles={
        ['RELATED']:'También podrías necesitar',
        ['TOP']:'Más vendidos',
        ['OFFERS']:'Aprovecha las ofertas'
    };

    const init = () => {
        setTitle(titles[type] || 'Productos');
        setLoading(false);

    }

    const loadData = () => {
        axios.post('loadProductsList',{type,productId})
        .then(response => {
            //console.log(response)
            setProductList(response.data.products);
            init();
        })
        .catch(error=>{
            //console.log(error.response);
        });
    }

    const touchStart = event => {
        setTouchPosition(event.touches[0].clientX);
    }
    const touchEnd = event => {
        const endX=event.changedTouches[0].clientX;
        const touchSize=(touchPosition-endX);
        if(touchSize<50 && touchSize>0){
            return
        }
        if(touchSize<0 && touchSize>-50){
            return
        }
        if(productsRef.current){
            const list=productsRef.current;
            const products=list.querySelectorAll('.product');
            if(products.length){
                const productWidth = products.item(0).offsetWidth;
                const pos=position;
                if(endX<touchPosition){
                    if(pos>=products.length-1){
                        return
                    }
                    products.item(0).style.marginLeft=`-${(pos+1)*productWidth}`;
                    setPosition(position+1);
                }
                if(endX>touchPosition){
                    if(pos===0){
                        return
                    }
                    products.item(0).style.marginLeft=`-${(pos-1)*productWidth}`;
                    setPosition(position-1);
                }
            }
        }
        
    }

    useEffect(()=>{
        if(loading){
            loadData();
        }
    },[loading]);
    return(
        <div className="w-full max-w-design mx-auto mb-12 px-2 lg:px-0 relative">
            <div className="mb-2 p-1">
                <h2 className="text-xl font-normal">{title}</h2>
            </div>
            <div ref={productsRef} className="w-full h-40 overflow-hidden whitespace-nowrap" onTouchStart={touchStart} onTouchEnd={touchEnd}>
                {productList.map((item,index)=>(
                    <SmallProductCard key={index} product={item} />
                ))}
            </div>
            <div className="indicators lg:hidden w-full text-center">
                    {productList.map((item,index)=>(
                        <div key={index} className="inline-flex flex-center h-3 w-3 mx-0.5">
                            <div className="w-full h-full rounded-full p-0.5 border border-transparent">
                                <div className="w-full h-full rounded bg-gray-300"></div>
                            </div>
                        </div>
                    ))}
            </div>
        </div>
    );
};

export default SmallProductsBar;