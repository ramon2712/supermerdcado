import { useState } from "react"
import { Link } from "react-router-dom";

const BrandsBar = (props) => {

    const[title,setTitle]=useState('Las mejores marcas');
    return(
        <div className="w-full max-w-design mx-auto mb-12">
            <div className="mb-2 p-1">
                <h2 className="text-xl font-normal">{title}</h2>
            </div>
            <div className="w-full py-4 bg-white flex flex-wrap flex-center">
                <Link to="marca/1">
                    <img src="images/brands/dell.png" alt="Dell" className="h-12 mx-6 my-4 " />
                </Link>
                <Link to="marca/foset">
                    <img src="images/brands/hp.png" alt="HP" className="h-12 mx-6 my-4 "  />
                </Link> 
                <Link to="marca/hermex">
                    <img src="images/brands/philips.png" alt="Marca hermex cerraduras" className="h-12 mx-6 my-4"  />
                </Link> 
                {/* <Link to="marca/volteck">
                    <img src="images/brands/volteck.svg" alt="Marca volteck material eléctrico" className="h-12 mx-6 my-4"  />
                </Link>{/*  */}
                <Link to="marca/4">
                    <img src="images/brands/lg.png" alt="Marca DeWalt Herramintas eléctricas" className="h-12 mx-6 my-4"  />
                </Link>{/*  */}
                {/*  <Link to="marca/black-decker">
                    <img src="images/brands/black-decker.png" alt="Marca Balck+Decker Herramientas eléctricas" className="h-12 mx-6 my-4"  />
                </Link>*/}
                <Link to="marca/2">
                    <img src="images/brands/oster.png" alt="Marca Craftsman Herramientas eléctricas y manuales" className="h-12 mx-6 my-4"  />
                </Link>{/*  */}
                {/* <Link to="marca/condulac">
                    <img src="images/brands/condulac.png" alt="Marca Condulac Cables eléctricos" className="h-12 mx-6 my-4"  />
                </Link> 
                <Link to="marca/evans">
                    <img src="images/brands/evans.png" alt="Marca Evans Hidráulicos" className="h-12 mx-6 my-4"  />
                </Link>{/*  
                <Link to="marca/pretul">
                    <img src="images/brands/pretul.svg" alt="Marca Pretul Herramientas eléctricas y manuales" className="h-8 mx-6 my-4"  />
                </Link>{/*  
                <Link to="marca/klintek">
                    <img src="images/brands/klintek.svg" alt="Marca Klintek Herramientas de limpieza" className="h-12 mx-6 my-4"  />
                </Link>{/*  */}
            </div>
        </div>
);
};

export default BrandsBar;