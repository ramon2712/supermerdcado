import { useEffect, useRef, useState } from 'react';

import './slider.css';

const Slider = props => {

    const sliderRef=useRef()
    const[slides,setSlides]=useState({length:0});
    //const[slideTimeOut,setSlideTimeOut]=useState(null);
    const[sliderValues,setSliderValues]=useState({
        sleep:4500,
        transition:800
    });

    const initSlider= () => {
        if(sliderRef.current){
            const slider=sliderRef.current;
            const s=slider.querySelectorAll('.slide');
            const pointers=slider.querySelector('.pointers');
            let slideTimeOut=null;
            setSlides(s);

            const animateSlider=({current})=>{
                const slider=sliderRef.current;
                if(!slider){
                    return;
                }
                clearTimeout(slideTimeOut);
                const s=slider.querySelectorAll('.slide');
                const w=slider.offsetWidth;
                s.item(0).style.marginLeft=`-${(current*w)}`;
                const pointers=slider.querySelector('.pointers');
                const points=pointers.querySelectorAll('.pointer');
                points.forEach((item)=>item.classList.remove('current'));
                points.item(current).classList.add('current');
                slideTimeOut=(setTimeout(animateSlider,sliderValues.sleep,{
                    current:(current+1<(s.length))?(current+1):0
                }));
            }

            slideTimeOut=(setTimeout(animateSlider,1,{current:0}));
            s.forEach((item,index)=>{
                item.style.transitionDuration=`${sliderValues.transition}ms`;
                const point=document.createElement('div');
                point.classList.add('pointer');
                if(index===0){
                    point.classList.add('current');
                }
                pointers.append(point);
                point.addEventListener('click',(event)=>{
                    event.preventDefault();
                    pointers.querySelectorAll('.pointer').forEach(item=>item.classList.remove('current'));
                    event.target.classList.add('current');
                    clearTimeout(slideTimeOut);
                    slideTimeOut=(setTimeout(animateSlider,1,{current:index,}));
                });
            });
        }
    };

    useEffect(()=>{
        initSlider();
    },[]);

    return(
        <div ref={sliderRef} className=" w-full max-w-design lg:h-auto bg-gray-300 mx-auto whitespace-nowrap overflow-hidden relative">
            <div className="slide w-full inline-flex flex-center overflow-hidden h-48 md:h-56 lg:h-auto">
                <img src="images/slider/slider-1-8.png" alt="Slider-1" className="slide-background h-full w-auto lg:w-full lg:h-auto" />
            </div>
            <div className="slide w-full inline-flex flex-center overflow-hidden h-48 md:h-56 lg:h-auto">
                <img src="images/slider/slider-2-8.png" alt="Slider-2" className="slide-background h-full w-auto lg:w-full lg:h-auto" />
            </div>
            <div className="slide w-full inline-flex flex-center overflow-hidden h-48 md:h-56 lg:h-auto">
                <img src="images/slider/slider-3-8.png" alt="Slider-3" className="slide-background h-full w-auto lg:w-full lg:h-auto" />
            </div>
            <div className="slide w-full inline-flex flex-center overflow-hidden h-48 md:h-56 lg:h-auto">
                <img src="images/slider/slider-4-8.png" alt="Slider-4" className="slide-background h-full w-auto lg:w-full lg:h-auto" />
            </div>
            <div className="slide w-full inline-flex flex-center overflow-hidden h-48 md:h-56 lg:h-auto">
                <img src="images/slider/slider-5-8.png" alt="Buchanans" className="slide-background h-full w-auto lg:w-full lg:h-auto" />
            </div>
            {/* <div className="slide w-full inline-flex flex-center overflow-hidden h-48 md:h-56 lg:h-auto">
                <img src="images/slider/BANNER_3.png" alt="Buchanans" className="slide-background h-full w-auto lg:w-full lg:h-auto" />
            </div>*/}
            <div className="pointers w-full h-10 absolute left-0 bottom-0 flex flex-center z-10"></div> 
        </div>
    )
};

export default Slider;