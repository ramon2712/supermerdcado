import axios from "axios";
import { useContext, useEffect, useState } from "react";
import Product from "../../Products/ProductsCardMod";

export default ({match})=>{
    //return match;
    //const [description, setDescription] = useState('');
    //const {addToCart}=useContext(AppContext);
    const [productsList,setProductsList] = useState([]);

    useEffect(() => {
         //console.log("Busqueda: "+search)
         let q=match.params.name;
         //console.log("search "+search);
         axios.post("/search",{q:match.params.name}).then
         (response => {
             //console.log("Response")
             console.log(response.data)
             setProductsList(response.data.products.data)

         }).catch(e =>{
          console.log(e.response)
       }) 
        
    }, [match.params.name]);

    return(
        <>
            <div>
                <div className="w-full 2xl:px-0 max-w-design mx-auto">
                     <div className="container w-full pt-12 " >
                     <div className="w-full flex flex-wrap bg-white py-2 px-2">
                        <div className="w-full bg-white lg:w-0 lg:flex-grow overflow-hidden whitespace-nowrap">
                            <div className="w-full h-auto flex flex-wrap">
                                {productsList.length>0 ? productsList.map((item,index)=>(                 
                                            <Product key={index} product={item} tam="w-1/4"/>
                                        )):
                                        <div className="w-full flex flex-col flex-center">
                                        <small>No se encontraron resultados para tu búsqueda.</small>
                                            <img src="/images/clear.png" className=""/>
                                        </div>} 
                            </div>

                        </div>
                        </div>
                     </div>    
                </div>
            </div>
        </>
    );
};