import axios from "axios";
import { useContext, useEffect, useState ,useRef} from "react";
import { Link } from "react-router-dom";
import AppContext from "../../../context/Application/AppContext";
import Product from "../../Products/ProductsCardMod";
import Pagination from '@material-ui/lab/Pagination';
import LoadingSpinner from "../../htmlComponents/loading";

export default ({match})=>{
    const {departamentList} = useContext(AppContext);
    const [paginator, setPaginator] = useState({
        offset:0,
        limit:20
    });
    const [bannerImage, setBannerImage] = useState('')
    const [minPrice, setMinPrice] = useState(0);
    const [maxPrice, setMaxPrice] = useState(50000);
    const [departament_id, setDepartament_id] = useState(departamentList.departaments[match.params.departament_name].id);
    const [subDepartaments, setSubDepartaments] = useState([]);
    const [departaments, setDepartaments] = useState([]);
    const [productsList,setProductsList] = useState([]);
    const productsRef=useRef();
    const [touchPosition,setTouchPosition]=useState(0);
    const [position,setPosition]=useState(0);
    const [orden,setOrden] = useState("asc");
    const [subOrden,setSubOrden] = useState();
    const [subName,setSubName] = useState("Todos");
    const [loading,setLoading] = useState(true);
 
    const touchStart = event => {
        setTouchPosition(event.touches[0].clientX);
    }
    const touchEnd = event => {
        const endX=event.changedTouches[0].clientX;
        const touchSize=(touchPosition-endX);
        if(touchSize<50 && touchSize>0){
            return
        }
        if(touchSize<0 && touchSize>-50){
            return
        }
        if(productsRef.current){
            const list=productsRef.current;
            const products=list.querySelectorAll('.product');
            if(products.length){
                const productWidth = products.item(0).offsetWidth;
                const pos=position;
                if(endX<touchPosition){
                    if(pos>=products.length-1){
                        return
                    }
                    products.item(0).style.marginLeft=`-${(pos+1)*productWidth}`;
                    setPosition(position+1);
                }
                if(endX>touchPosition){
                    if(pos===0){
                        return
                    }
                    products.item(0).style.marginLeft=`-${(pos-1)*productWidth}`;
                    setPosition(position-1);
                }
            }
        }
        
    }
    //Obtener los productos por la categoria
   /*  const getProductsCat= () =>{ 
        console.log("id del departamento : ", departamentList.departaments[match.params.departament_name].id)
        let cid = {
            cid : departamentList.departaments[match.params.departament_name].id 
        }
        axios.post('loadProductsCat', {cid}).then(response=>{
            console.log("Productos inicio",response.data.products)
            setProductsList(response.data.products.data)
            setLoading(false);
            //console.log(productsList)
        }).catch(error =>{
            console.log(error.response.data)
        })
    } */

    const getImageFromServer = filename => {
        //const ext='jpg';
        const image=new Image();
        image.src=`images/category_banners/1980x232/${match.params.departament_name}.png`;
        image.onload = () => {
            setBannerImage(image.src);
        }
    }

    const getSubdepartaments = () =>{
        let cid = {
            cid : departamentList.departaments[match.params.departament_name].id 
        }
        axios.post('loadSubdepartaments', cid).then(response=>{
            //console.log("Subdepartamentos",response)
            setSubDepartaments(response.data);
        }).catch(error =>{
            console.log(error.response.data)
        })
    }
    const getDepartaments = () =>{
        axios.post('loadDepartaments').then(response=>{
            console.log("departamentos",response)
            setDepartaments(response.data);
        }).catch(error =>{
            console.log(error.response.data)
        })
    }

    const handleSelectChange = ({target : {value}}) =>{
        setOrden(value);
        //console.log(value)
    }
    const handleSelectSubChange = ({target}) =>{
        setSubOrden(target.value);
        setSubName(target.options[target.selectedIndex].text);
        //console.log(value)
    }

    //Mostrar los productos ordenados por precio
    useEffect(() => {
        //console.log(departamentList.departaments[match.params.departament_name].id);
        getSubdepartaments();
        getDepartaments();
        setDepartament_id(departamentList.departaments[match.params.departament_name].id || 0);
        departamentList.departaments[match.params.departament_name].id && getImageFromServer(match.params.departament_name);
         let cid = {
            cid : departamentList.departaments[match.params.departament_name].id 
        }
        axios.post('loadProductsCat', {cid,orden,maxPrice:maxPrice,minPrice:minPrice,subOrden}).then(response=>{
            console.log("productos",response.data)
            setProductsList(response.data.products.data)
            setLoading(false);
        }).catch(error =>{
            console.log(error.response.data)
        }) 
        //getProductsCat();
    }, [maxPrice,minPrice,orden,subOrden])
    return(
        <>
            {loading?
                <LoadingSpinner loading={loading}/>
                :
            <>
                    <div className="w-full 2xl:px-0 max-w-design mx-auto mt-1">
                        {/* <div className="w-full flex flex-grow">
                            <div className="bg-gradient-to-r from-red-500 to-yellow-500 w-1/3 h-32 flex flex-center overflow-hidden rounded-xl mr-4">
                                <div className="w-full flex flex-center">
                                    {departamentList.departaments[match.params.departament_name].name}
                                </div>  
                               {/*  Departamento: 
                                <p className="text-primary">{departamentList.departaments[match.params.departament_name].name}</p>
                            </div>
                            <div className="bg-gradient-to-r from-red-500 to-yellow-500 w-2/3 h-32 flex flex-center text-white rounded-xl bg-gradient-to-r from-bg-primary to-bg-extra">
                                <p>{subName}</p>
                            </div>
                           <img src={bannerImage} className={"w-full "+(bannerImage===''?'hidden':'')} />
                        </div> */}
                        <div className="container w-full pt-1 " >
                            <div className="w-full flex flex-wrap bg-white py-2 px-2">
                                <div className="w-full h-10 lg:w-56 lg:h-auto lg:border-r lg:pr-4 lg:pb-4 shadow-mb"> {/* Filter Options */}
                                    <div className="w-1/4 h-10 flex-start flex lg:w-full">
                                        <h2 className="text-primary"><i className="las la-filter"></i> Filtrar</h2>
                                    </div>
                                    <div className="w-0 flex-grow lg:w-full flex flex-start flex-wrap lg:mb-2">
                                        <label><small>Ordenar por:</small></label>
                                        <select value={orden}
                                                className="ml-1 pl-1 lg:w-full lg:ml-0 flex-grow h-8 border rounded-sm bg-gray-50 text-xs text-gray-500" 
                                                onChange={handleSelectChange}>
                                            <option value="asc">Relevancia</option>
                                            <option value="asc">Precio menor</option>
                                            <option value="desc">Precio mayor</option>
                                        </select>
                                    </div>
                                    <div className="w-14 h-10 flex flex-center lg:hidden text-gray-500 rounded-full">
                                        <a className="btn h-10 w-10 flex flex-center rounded-full" href="#" onClick={e=>{e.preventDefault(); e.stopPropagation(); }}>
                                            <i className="las la-ellipsis-v text-3xl"></i>
                                        </a>
                                    </div>
                                    <div className="w-full">
                                        <div className="w-full flex flex-start flex-wrap pb-2" >
                                            <label>
                                                <small>Rango de precios</small>
                                            </label>
                                            <div className="w-full flex justify-between">
                                                <input type="number" name="" className="h-8 border w-5/6 text-xs px-2 text-gray-500" value={minPrice} onChange={e=>setMinPrice(e.target.value)} min={0} />
                                                <span className="mx-2 text-secondary">-</span>
                                                <input type="number" name="" className="h-8 border w-5/6 text-xs px-2 text-gray-500" value={maxPrice} onChange={e=>setMaxPrice(e.target.value)} min={0}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="w-0 flex-grow lg:w-full flex flex-start flex-wrap lg:mb-2">
                                        <label><small className="text-primary">Subdepartamentos:</small></label>
                                        <select value={subOrden}
                                                className="ml-1 pl-1 lg:w-full lg:ml-0 flex-grow h-8 border rounded-sm bg-gray-50 text-xs text-gray-500" 
                                                onChange={handleSelectSubChange}>
                                                <option value="">Todos</option>
                                                {subDepartaments.map((item,index)=>(  
                                                    <option key={index} value={item.id}>{item.name}</option>
                                                ))}
                                        </select>
                                    </div>
                                    <div className="w-full flex flex-col">
                                        <h2 className="text-primary my-2">Departamentos</h2>
                                        {departaments.map((item,index)=>( 
                                                    <Link key={index} to={`/categoria/${item.name}`}><small className="hover:text-primary">{item.name}</small></Link> 
                                                ))}                
                                    </div>
                                </div>
                                <div className="w-full bg-white lg:w-0 lg:flex-grow overflow-hidden whitespace-nowrap">
                                    <div className="w-full h-auto flex flex-wrap" onTouchStart={touchStart} onTouchEnd={touchEnd}>
                                        {productsList.map((item,index)=>(                 
                                                    <Product key={index} product={item} tam="w-1/4"/>
                                                ))}
                                    </div>
        
                                </div>
                            </div>
                        </div>
                    
                    </div>
                </>
            }
        </>
    );
};