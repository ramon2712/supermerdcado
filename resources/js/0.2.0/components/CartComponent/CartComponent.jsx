import axios from "axios";
import { useContext, useEffect, useState } from "react";
import { Link,useHistory } from "react-router-dom";
import AppContext from "../../context/Application/AppContext";
import { useTinyModalDialog } from "../Utils/tinymodalmx/tinymodal";
import AddCart from "./AddCart";
import swal from 'sweetalert';

const CartComponent = (props) => {
    const [items, setItems] = useState([]);
    const [disabledButton,setDisabledButton] = useState(false);
    const [inputs, setInputs] = useState({});
    const [total, setTotal] = useState(0);
    const [openModal, setOpenModal] = useState(false);
    const [itemD, setItemD] = useState({});
    const history=useHistory();
    const Modal = useTinyModalDialog();
    const { user, cart, setCountCart } = useContext(AppContext);
    const getItems = () => {
        if (user.auth) {
            axios.get("/api/cart/" + user.id).then((res) => {
                //console.log(res.data);
                let inp = {};
                let count = 0;
                //console.log(res)
                res.data.forEach((element) => {
                    inp = {
                        ...inp,
                        ["quantity_" + element.id]: element.quantity,
                    };
                    count = count + element.sale_price * element.quantity;
                });
                if(!res.data.length){
                    setDisabledButton(true);
                }
                setInputs(inp);
                setItems(res.data);
                setTotal(count);
            });
        } else {
            axios.get("/cookie/cart").then((resp) => {
                let inp = {};
                let count = 0;
                console.log(resp)
                if(resp.data.length){
                    //console.log("Datos");
                    resp.data.forEach((element) => {
                        inp = {
                            ...inp,
                            ["quantity_" + element.id]: element.quantity,
                        };
                        count = count + element.sale_price * element.quantity;
                    });
                    setInputs(inp);
                    setTotal(count);
                    setItems(resp.data || []);
                    setDisabledButton(false);
                } else{
                    //console.log("Sin Datos");
                    setDisabledButton(true);
                }
            });
        }
    };
    const editQuantity = (item, quantity) => {
        let editItem = {
            id_user: item.id_user,
            id_product: item.id_product,
            status: 0,
            quantity: item.quantity + quantity,
        };
        if (item.quantity+quantity>0)
            axios.put("/api/cart/" + item.id, editItem).then((res) => {
                //console.log(res, "resseteando items");
                getItems();
            });
    };
    const handleChange = (item, e) => {
        const value = e.target.value;
        setInputs({ ...inputs, ["quantity_" + item.id]: value });
        let editItem = {
            id_user: item.id_user,
            id_product: item.id_product,
            status: 0,
            quantity: value,
        };
        if ((value < 0 && item.quantity > 0) || value > 0)
            axios.put("/api/cart/" + item.id, editItem).then((res) => {
                //console.log(res, "reseteando items");
                getItems();
            });
    };
    const deleteItem = (item) =>{
        swal({
            title: "¿Estás seguro?",
            text: "Se eliminará el artículo "+item.name,
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                axios.delete('/api/cart/'+item.id).then(response=>{
                    console.log('Item eliminado:', item.id);
                    //setItemD(null)
                    //setOpenModal(false);
                    getItems();
                    swal("El elemento se eliminó de tu carrito", {
                        icon: "success",
                      });
                });
            }
          });

    }
    const generateOrder = () =>{

        let newOrder = {
            user_id: user.id, 
            order_status_id: 1,
            order_delivery_periods_id:1,
            order_types_id: 1,
            user_address_id:1,                    
            amount: total, 
            subtotal:total-(total*0.16),                  
        };
        axios.post('api/orders', newOrder).then(response=>{
            console.log(response.data)
            history.push(`/check/${response.data.order_id}`);
        }).catch(errors=>{
            console.log(errors.response);
        })           
    };
    useEffect(() => {
        getItems();
        if(items === []){
            console.log("vacio")
        }
    }, [user]);
    return (
        <>
                <div className="w-full px-0.5 lg:px-24 2xl:px-0 max-w-design mx-auto p-8 bg-white">
                    <h2 className="text-5xl">Tu carrito</h2>
                    <div className="w-full flex flex-wrap flex-center">
                        {items.length>0 ? 
                        (
                            items.map((item, index) => (
                                <div
                                    key={index}
                                    className="flex p-8 w-full md:w-4/5 flex-wrap border-b"
                                >
                                    <img
                                        src={
                                            
                                            "product/departaments/" +
                                            item.filename
                                        }
                                        alt=""
                                        className="bg-green-500 h-1/8 w-1/8 md:w-12 md:h-12"
                                    />
                                    <div className="flex-grow ml-8">
                                        <Link to="">
                                            <h3>{item.name}</h3>
                                        </Link>
                                        <button
                                            className="text-primary p-4 w-5/8"
                                            onClick={(e) => {
                                                setItemD(item);
                                                deleteItem(item);
                                                //setOpenModal(!openModal);
                                            }}
                                        >
                                            Eliminar del carrito
                                        </button>
                                    </div>

                                    <div className="w-24 h-10 flex flex-center">
                                        <input
                                            type="number"
                                            name={"quantity_" + item.id}
                                            step="1"
                                            min="1"
                                            value={inputs["quantity_" + item.id]}
                                            className="w-10 h-10 border text-center"
                                            onChange={(e) => handleChange(item, e)}
                                        />
                                        <div className="h-full w-6 border border-l-0 flex flex-wrap">
                                            <button
                                                className="w-full h-1/2 flex flex-center"
                                                onClick={() =>
                                                    editQuantity(item, 1)
                                                }
                                            >
                                                +
                                            </button>
                                            <button
                                                className="w-full border-t h-1/2 flex flex-center"
                                                onClick={() =>
                                                    editQuantity(item, -1)
                                                }
                                            >
                                                -
                                            </button>
                                        </div>
                                    </div>
                                    <div className="w-32 flex flex-end">
                                        <h4 className="text-xl text-black">
                                        {new Intl.NumberFormat("ES-MX", {
                                            style: "currency",
                                            currency: "MXN"
                                        }).format(item.sale_price * item.quantity)}
                                        </h4>
                                    </div>
                                </div>
                            ))
                        ):
                        <AddCart />
                        }
                        
                    </div>
                    {items.length>0 && 
                        <div className="flex flex-wrap flex-end p-8">
                        <div className="flex flex-col flex-end">
                            <h4 className="text-5xl text-primary">Total</h4>
                            <h2 className="text-3xl text-secondary">
                                {new Intl.NumberFormat("ES-MX", {
                                    style: "currency",
                                    currency: "MXN"
                                }).format(total)}
                            </h2>
                            <button className={`bg-primary border rounded h-12 p-2 text-white mt-4`}
                            onClick={generateOrder} disabled={disabledButton}>
                            Continuar con la compra
                            </button>
                        </div>
                    </div>
                    }
                </div>
                
                <Modal.TinyModalFrame on={openModal}>
                    <h1 className="text-2xl text-primary">Eliminar del carrito a:</h1>
                    <h1 className="text-xl text-black">{itemD.name}</h1>
                    <div className="w-full flex flex-end space-x-4">
                        <button className="border rounded h-12 text-primary p-3" onClick={deleteItem}>
                            Continuar
                        </button>
                        <button
                            onClick={(e) => setOpenModal(!openModal)}
                            className="bg-primary h-12 rounded text-white p-3"
                        >
                            Cancelar
                        </button>
                    </div>
                </Modal.TinyModalFrame>
        </>
    );
};
export default CartComponent;
