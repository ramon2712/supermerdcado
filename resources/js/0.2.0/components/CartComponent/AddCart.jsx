
const AddCart = ()=>{
    return(
        <div className="w-full flex flex-col flex-center">
            <small>Añade productos a tu carrito</small>
            <img src="https://img.icons8.com/dusk/50/000000/shopping-cart-loaded--v2.png"/>
        </div>
    )
}

export default AddCart;