
export default (props) => {
    return(
        <div className="w-full max-w-design lg:px-12 2xl:px-0 mx-auto" {...props}></div>
    )
}