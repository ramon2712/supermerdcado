import styled from "styled-components";
import {carbon, danger, focus, white,lightGray,primaryAlt,secondary,ultralightGray,yellowAlt, greenAlt, focusAlt} from './colorPallete'

export const shadow=`rgba(0, 0, 0, 0) 0px 0px 0px 0px, rgba(0, 0, 0, 0) 0px 0px 0px 0px, rgba(0, 0, 0, 0.1) 0px 1px 3px 0px, rgba(0, 0, 0, 0.06) 0px 1px 2px 0px`

export const Table = styled.table`
    border-collapse: collapse;
    border: solid 1px ${lightGray};
    width: 100%;
    & th{
        background-color: ${primaryAlt};
        color: ${white};
        font-size: 0.8rem;
        font-weight: 500;
        text-align: left;
        padding: 1rem 0.5rem;
        &:first-child, &:last-child{
            border-right: none
        }
        &.text-center{
            text-align: center;
        }
    }
    &.minimalist{ 
        border: none;
        & th{
        background-color: ${white};
        border-bottom: solid 1px ${primaryAlt};
        color: ${primaryAlt};
        padding: 0.5rem;
        }
    }
    & tbody{
        & tr:nth-child(odd){
            background-color: white;
        }
        & tr:nth-child(even){
            background-color: ${ultralightGray};
        }
        & td{
            color: ${carbon};
            font-size: 0.8rem;
            padding: 0.5rem 0.5rem;
            &.text-red{
                color: ${danger};
            }
            &.text-yellow{
                color: ${yellowAlt}
            }
            &.text-green{
                color: ${greenAlt};
            }
            & label.tag{
                background-color: ${lightGray};
                border-radius: 0.125rem;
                box-shadow: ${shadow};
                color: ${focus};
                display: inline-flex;
                margin: 0;
                margin-bottom: 0.25rem;
                margin-right: 0.25rem;
                padding: 0.125rem 0.75rem;
                text-transform: capitalize;
            }
            & a, & button{
                align-items: center;
                color: ${secondary};
                display: inline-flex;
                font-size: 0.8rem;
                justify-content: flex-start;
                margin-right: 0.5rem;
                padding: 0 0.5rem;
                &.edit:hover{
                    color: ${focus};
                }
                &.delete:hover{
                    color: ${danger}
                }
                & .material-icons-outlined{
                    font-size: 1.25rem;
                    margin-right: 0.25rem;
                }
            }
        }
        & input[type=checkbox]{
            display:none;
            &+ label span{
                align-items: center;
                background-color: ${lightGray};
                border-radius: 50%;
                color: transparent;
                cursor: pointer;
                display: inline-flex;
                height: 1.25rem;
                justify-content: center;
                width: 1.25rem;
                transition: background-color 200ms ease, color 200ms ease;
                & .material-icons{
                    font-size: 1rem;
                }
            }
            &:hover + label span{
                background-color: ${focus};
            }
            &:checked + label span{
                background-color: ${focusAlt};
                color:white;
            }
        }
    }
`