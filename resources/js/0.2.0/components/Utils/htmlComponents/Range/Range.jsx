import './range.scss';

export default ({title,name=""})=>{
    return(
        <div className="w-full px-2 lg:px-0 relative">
            <label className="block">
                <small>{title}</small>
            </label>
            <div className="w-full">
                <input type="range" name={name} min="0" max="1000" defaultValue="100" className="range-input" />
                <input type="range" name={name} min="0" max="1000" defaultValue="900" className="range-input" />
            </div>
        </div>
    );
};