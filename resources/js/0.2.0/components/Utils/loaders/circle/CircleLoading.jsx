import styled, { keyframes } from "styled-components"

const spin=keyframes`
    100%{
        transform: rotate(360deg); 
    }
`;
const ContainerDiv=styled.div`
    align-items:center;
    display: flex;
    height: ${({on})=>on?'100%' : 'inherit'};
    position: relative;
    justify-content: center;
    width:100%;
`;
const LoadDiv=styled.div`
    align-items:center;
    display: flex;
    height: 100%;
    justify-content: center;
    width: 100%;
    opacity: ${({on})=>on?1 : 0};
    left: 0;
    position: absolute;
    visibility: ${({on})=>on?'visible' : 'collapse'};
    top:0;
    transition-property: visibility, opacity;
    transition-duration: 300ms;
    transition-timing-function: ease-in-out;
    z-index: 1;
    & div{
        border: solid 3px ${({borderColor})=>borderColor || '#0373fc'};
        border-left-color: transparent;
        border-radius: 50%;
        display: ${({on})=>on?'block' : 'none'};
        height: 10rem;
        position: relative;
        width: 10rem;
        animation: ${spin} infinite linear 1500ms;
    }
`;
const ChildrenDiv=styled.div`
    opacity: ${({on})=>!on?1 : 0};
    visibility: ${({on})=>!on?'visible' : 'collapse'};
    z-index: 2;
`;
export default ({on,borderColor,children})=>{

    return(
        <ContainerDiv on={on}>
            <LoadDiv borderColor={borderColor} on={on}>
                <div></div>
            </LoadDiv>
            <ChildrenDiv on={on}>
                {children}
            </ChildrenDiv>
        </ContainerDiv>
    );
};