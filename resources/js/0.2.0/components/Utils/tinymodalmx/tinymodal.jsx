import { createRef, useEffect, useState } from "react"
import './style/tinymodal.css'
import successImage from './images/success.svg'
import infoImage from './images/info.svg'
import warnImage from './images/warning.svg'
import closeImage from './images/close.svg'
import saveImage from './images/save.svg'
import deleteImage from './images/delete.svg'
import questionImage from './images/question.svg'

export const useTinyModalDialog=function(){
    const[title,setTitle]=useState('Success!')
    const[visible,setVisible]=useState(false)
    const[message,setMessage]=useState('Task completed successful')
    const[callback,setCallback]=useState({f:()=>{}})
    const[buttonText,setButtonText]=useState('Continuar')
    const[loading,setLoading]=useState('')
    const[icon,setIcon]=useState('')
    const[type,setType]=useState(0)
    const close=()=>{
        setTitle('')
        setMessage('')
        setCallback(()=>{})
        setVisible(false)
    }
    const sl = (title,message) => {
        setLoading('loading')
        setTitle(title)
        setMessage(message)
        setVisible(true)
    }
    const icons=[
        saveImage,
        successImage,
        infoImage,
        warnImage,
        closeImage,
        deleteImage,
        questionImage
    ]
    const show=(title,message,type=2,buttonText='Continuar')=>{
        setLoading('')
        setTitle(title)
        setMessage(message)
        setButtonText(buttonText)
        setType(type)
        setIcon(icons.length>type?icons[type]:2)
        setVisible(true)
    }
    return {
        TinyModalDialog:()=>(<TinyModalDialog 
            visible={visible}
            title={title}
            message={message}
            close={()=>{close()}}
            buttonText={buttonText}
            type={type}
            icon={icon}
            callback={callback}
            loading={loading}
            sl={sl}
            show={show}
            />),
        TinyModalFrame:TinyModalFrame,
        show:show,
        dialog:(title,message,type=0,buttonText='Continuar',callback)=>{
            setLoading('')
            setTitle(title)
            setMessage(message)
            setButtonText(buttonText)
            setType(type)
            setIcon(icons.length>type?icons[type]:0)
            setCallback({f:callback})
            setVisible(true)
        },
        loading:(title,message)=>sl(title,message),
        close:close,
        isVisible: _=>visible,
    };
}

const TinyModalDialog = props => {
    const dialogRef=createRef()
    const[isLoader,setIsLoader]=useState(false)
    const types={
        LOADING:-1,
        CONFIRM:0,
        SUCCESS:1,
        INFO:2,
        WARN:3,
        ERROR:4,
        CONFIRM_DELETE:5,
        CONFIRM_QUESTION:6
    }
    const buttonAction=()=>{
        if(props.type!==types.CONFIRM && props.type!==types.CONFIRM_DELETE && props.type!==types.CONFIRM_QUESTION){
            props.close()
            return
        }
        if(props.callback.f && {}.toString.call(props.callback.f) === '[object Function]'){
            props.callback.f({setLoading:props.sl,close:props.close,show:props.show})
            return
        }
        //props.visible.setOn(false)
    }
    useEffect(()=>{
        if(props.visible){
            dialogRef.current.querySelector("#TinyOkButton")?.focus()
        }
    })
    return(
        <>
            <div id="TinyBackground" className={props.visible?'On':''}></div>
            <div ref={dialogRef} className={"wrap-tinymodal "+(props.visible?'On':'')}>
                <div id="TinyModalDialog" className={props.visible?'On':''}>
                    <div>
                        <div id="TinyImageContainer">
                            <div id="TinyIcon" className={props.loading}>
                                {<img src={props.icon} alt="dialog" />}
                            </div>
                        </div>
                        <div className="tmd-title">{props.title}</div>
                        <div id="TinyMessage">{props.message}</div>
                        {props.loading!=='loading' &&
                        <div id="TinyButtons">
                            {
                            (props.type===0 || props.type===5 || props.type===6) &&
                            <button type="button" className="close" onClick={props.close}>Cancelar</button>
                            }
                            <button id="TinyOkButton" type="button" className={props.type===5?'delete':"success"} onClick={buttonAction}>{props?.buttonText || 'Continuar'}</button>
                        </div>
                        }
                    </div>
                </div>
            </div>
        </>
    )
}

const TinyModalFrame=({on,children,xl})=>(
    <>
    {on?
        <div className="w-full h-full absolute left-0 top-0 mt-4 bg-black bg-opacity-20 flex flex-center z-10">
            <div className={"p-4 shadow-xl bg-white rounded-xl m-h-p-95 overflow-auto relative "+(xl?'w-10/12':'w-1/2 ')}>
                {children}
            </div>
        </div>
    :
        <></>
    }
    </>
)