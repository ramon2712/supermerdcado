import FormContact from "./FormContact";
import "./style.css";
export default function Contact(){
    return(
        <div className="container mx-auto my-12 bg-gray-100">
            <div className="text-secondary text-3xl text-center pt-8 pb-6">Ayuda y contacto</div>
              <FormContact></FormContact>
            <div className="mx-auto text-center grid grid-cols-4">
                <p className="px-4 text-black col-span-2 col-start-2 bg-white">También puedes llamarnos</p>
            </div>
            <div className="grid grid-cols-4">
                <div className="col-span-2 col-start-2 bg-white  shadow-lg">
                     <i className="las la-phone text-3xl lg:text-xl ml-4"></i>
                    <span className="text-secondary text-base font-black leading-loose"> Teléfonos</span>
                    <p className="text-secondary text-base leading-loose ml-5">Ventas: 4939310191</p>
                    <p className="text-secondary text-base leading-loose ml-5">Oficinas: 4939310534 al 40</p>
                </div>
            </div>
        </div>
    );
}
