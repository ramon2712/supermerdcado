import axios from "axios";
import { useState } from "react";
import "./style.css";
import { useFormik } from "formik";
import * as Yup from "yup";
import PulseLoader from "react-spinners/PulseLoader"

export default function FormContact(){  
    const [isLoading,setIsLoading] = useState(false)
    const formik = useFormik({
        initialValues:{
            name: '',
            subjet: '',
            email: '',
            message: ''
        },
        validationSchema: Yup.object({
            name: Yup.string().required('Este campo es obligatorio'),
            subjet: Yup.string().required('Este campo es obligatorio'),
            email: Yup.string().email('Correo invalido').required('Este campo es obligatorio'),
            message: Yup.string().required('Este campo es obligatorio')
        }),
        validateOnChange:false,
        onSubmit: emailData =>{
            setIsLoading(true);
            axios.post('pruebaEmail', {...emailData}).then(response=>{
                //console.log(response.data.products)
                if(response){
                    setTimeout(() => {
                        setIsLoading(false)
                        swal({
                            title: "¡Correo enviado!",
                            text: 'El correo se envió correctamente, en breve recibirá una respuesta.',
                            icon: "success",
                            button: "Aceptar"
                        });
                        formik.resetForm();
                    }, 1000);
                    }
            }).catch(error =>{
                console.log(error.response.data)
            })
            
        },
    })
    return(
        <div className="grid grid-cols-4 bg-gray-100" >
                <form onSubmit={formik.handleSubmit} className="col-span-2 col-start-2 p-8 bg-white rounded-md mb-0 shadow-lg rounded-lg">
                    <div className="control-grup">
                        <div className="w-full text-sm text-secondary">Nombre:</div>
                        <input type="text"
                         placeholder={formik.errors.name ? formik.errors.name:"Nombre completo"}
                         className={`h-8 w-full rounded focus:border-gray-300 border-2 ${formik.errors.name ? "border-red-400":"border-gray-200" }`} 
                         onChange={formik.handleChange}
                         name="name"
                         value={formik.values.name}/>
                    </div>
                    <div className="control-grup">
                        <div className="w-full text-sm text-secondary">Asunto del mensaje:</div>
                        <input type="text" 
                        placeholder={formik.errors.subjet ? formik.errors.subjet:"Asunto"}
                        className={`h-8 w-full rounded focus:border-gray-300 border-2 ${formik.errors.subjet ? "border-red-400":"border-gray-200" }`} 
                        onChange={formik.handleChange}
                        name="subjet"
                        value={formik.values.subjet}/>
                    </div>
                    <div className="control-grup w-full">
                        <div className="text-sm text-secondary">Correo electrónico:</div>
                        <input type="email" 
                        placeholder={formik.errors.name ? formik.errors.email:"Ingrese el correo electrónico"} 
                        className={`h-8 w-full rounded focus:border-gray-300 border-2 ${formik.errors.email ? "border-red-400":"border-gray-200" }`} 
                        onChange={formik.handleChange}
                       name="email"
                       value={formik.values.email}/>
                    </div>
                    <div className="control-grup">
                        <div className="w-full text-sm text-secondary">Mensaje:</div>
                        <textarea 
                        placeholder={formik.errors.name ? formik.errors.message:"Escriba su mensaje detalladamente"} 
                        className="h-80 w-full rounded-md border-2 border-gray-200"
                        onChange={formik.handleChange}
                        name="message"
                        value={formik.values.message}/>
                    </div>
                    <div className="control-grup">
                        <div className="w-full flex flex-end py-4">
                             <button type="submit" 
                             className="bg-primary hover:bg-red-400 text-white rounded-md h-10 w-full md:w-1/3"
                             disabled={isLoading}
                             >
                                 {!isLoading ? "Enviar Mensaje" : 
                                 
                                 <div className="w-full h-full flex flex-wrap flex-center">
                                    <small>Enviando...</small>
                                    <PulseLoader color={`ffda00`} size={10} />
                                </div>}</button>
                        </div>          
                    </div>
                </form>
                <div>
                </div>
            </div>

    );
}