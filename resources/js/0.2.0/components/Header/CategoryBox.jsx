import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import slug from "../../../slug";

export default ({off}) => {
    const [loading,setLoading]=useState(true);
    const [categories, setCategories] = useState([]);

    const loadData=()=>{
        axios.post('ajax/loadCategories')
        .then(response=>{
            setCategories(response.data.categories);
            setLoading(false);
        })
        .catch(error=>setLoading(false));
    }

    useEffect(()=>{
        loading && loadData();
    },[loading]);

    return(
        <>
            <div className="w-full h-auto relative flex flex-start-top flex-wrap">
                {categories.map((item,index)=>(
                    <div key={index} className="w-full md:w-1/2 lg:w-1/4 px-2 lg:inline-block align-top mb-2">
                        <Link to={`/categoria/${slug(item.category.name)}`} className="text-blue-custom" onClick={e=>off(false)}>
                            <span className="text-2xl font-black lg:text-base">{item.category.name}</span>
                        </Link>
                        <div className="w-full pl-2 text-lg lg:text-sm">
                            <ul>
                                {item.subcategories.map((it,idx)=>(
                                    <li key={idx} className="text-blue-custom-light">
                                        <Link to={`/categoria/${slug(item.category.name)}/${slug(it.name)}`} onClick={e=>off(false)}>{it.name}</Link>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div>
                ))}
                <div className="w-full py-2 flex flex-end px-2">
                    <Link to="/categoria/todas" className="text-blue-custom lg:text-sm"  onClick={e=>off(false)}>Ver más categorías</Link>
                </div>
            </div>
        </>
    )
}