import { useContext, useEffect, useState } from "react"
import { useHistory } from "react-router";
import AppContext from "../../context/Application/AppContext";
export default ({on,off}) => {
    
    const {searchText}=useContext(AppContext);
    const [search, setSearch] = useState('');
    const [productsList,setProductsList] = useState([]);
    const history=useHistory();
    const windowResize=()=>{
        const w=window.innerWidth;
        if(w>=1024){
            off(true);
        }
    }

    const getProducts = evento => {
    if (evento.key === 'Enter') {
        history.push(`/search/${search}`);
    }
}
    const handleQChange= event => {
        return setSearch(event.target.value)
    }

    useEffect(()=>{
        window.onresize=windowResize;
        windowResize();
    },[])
    return(
        <div className={`absolute left-0 top-0 w-full h-full bg-gray-100 z-10 flex flex-center ${on?'max-h-full':'max-h-0'} lg:relative lg:w-0 lg:flex-grow lg:bg-transparent lg:pl-16 transition-all`}>
            <input type="text" 
                name="q" 
                defaultValue={searchText} 
                value={search} 
                onChange={handleQChange}
                onKeyPress={getProducts}
                className="flex-grow h-full bg-gray-200 text-gray-700 px-2 text-sm lg:h-3/4 lg:flex-grow-0 lg:w-1/2 lg:rounded-full transition-shadow"
                placeholder="Busca entre miles de productos"
                autoComplete="off"
            />
            <button className={`h-full w-16 ${on?'':'hidden'} lg:hidden flex flex-center text-dark-100`} onClick={e=>off(false)}>
                <i className="las la-times text-3xl"></i>
            </button>

           
        </div>
    )
}