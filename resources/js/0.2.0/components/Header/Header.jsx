import { useContext, useEffect, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';
import logoImage from '../../assets/images/Logo_barra.svg';
import AppContext from '../../context/Application/AppContext';
import CategoryBox from './CategoryBox';
import Searchbox from './Searchbox';

const Header = props => {
    const [openSearchbox, setOpenSearchbox] = useState(false);
    const [openAdvice, setOpenAdvice] = useState(false);
    const [openMenu, setOpenMenu] = useState(false);
    const [notificationCounter, setNotificationCounter] = useState(0);

    const {cart,user,initCart}=useContext(AppContext);

    useEffect(()=>{
        const header=document.getElementById('header');
        const headerSpace=document.getElementById('header-space');
        headerSpace.style.height=header.offsetHeight+'px';
    },[]);

    return(
        <>
            <header id="header" className="w-full fixed left-0 top-0 bg-white shadow z-30">
                {openAdvice &&
                <div className="w-full h-10 bg-red-600">
                    <div className="h-full w-full max-w-design mx-auto text-sm"></div>
                </div>}
                <div className="max-w-design h-18 lg:h-18 w-full px-2 lg:px-6 xl:px-12 2xl:px-0 mx-auto flex text-gray-50 relative">
                    {/* <div className="w-12 h-full flex flex-center lg:hidden relative">
                        {openMenu?
                        <i className="las la-times text-3xl"></i>
                        :
                        <i className="las la-bars text-3xl"></i>
                        }
                        <button className="absolute left-0 top-0 w-full h-full" onClick={e=>setOpenMenu(true)}></button>
                    </div> */}
                    <div className="h-full flex-grow lg:flex-grow-0 lg:w-40 flex flex-start relative pb-2">
                        <img src={logoImage} alt="Logotipo Macep" className="h-1/2 lg:h-full" />
                        <Link to="/" className="block absolute left-0 top-0 w-full h-full" />
                    </div>
                    <Searchbox on={openSearchbox} off={setOpenSearchbox} />
                    <div className="h-full flex">
                        <div className="h-full w-10 flex flex-center lg:hidden">
                            <button className="h-full w-full flex flex-center" onClick={e=>setOpenSearchbox(true)}>
                                <i className="las la-search text-2xl"></i>
                            </button>
                        </div>
                        <div className="h-full w-10 lg:w-auto flex flex-center lg:flex-start lg:mr-2 relative text-primary hover:text-red-500">
                            <i className="lar la-user text-3xl lg:text-xl"></i>
                            <span className="hidden lg:inline text-xs">{user.auth?`${user.name} ${user.lastname}`:`Mi cuenta`}</span>
                            <Link to="/mi-cuenta" className="block absolute left-0 top-0 w-full h-full" />
                        </div>
                        <div className="h-full w-10 lg:w-auto flex flex-center relative text-primary hover:text-red-500">
                            {(cart.counter>0) &&
                            <div className="h-6 w-6 absolute -right-2 lg:-right-3 top-1 bg-yellow-300 bg-opacity-50 rounded-full text-blue-800 text-xs flex flex-center">
                                {cart.counter}
                            </div>}
                            <i className="las la-luggage-cart text-3xl lg:text-xl"></i>
                            <span className="hidden lg:inline text-xs">Mi carrito</span>
                            <Link to="/carrito" className="block absolute left-0 top-0 w-full h-full" />
                        </div>
                        <div className="h-full w-10 lg:w-auto flex flex-center relative text-primary hover:text-red-500" style={{marginLeft: 8}}>
                            <i className="las la-envelope text-3xl lg:text-xl"></i>
                            <span className="hidden lg:inline text-xs">Contacto</span>
                            <Link to="/contacto" className="block absolute left-0 top-0 w-full h-full"></Link>
                        </div>
                    </div>
                </div>
            </header>
            <div id="header-space" className="w-full h-12 lg:h-20 relative">
                <div className="w-full h-full max-w-design mx-auto relative">
                    <div className={`w-full absolute left-0 top-full bg-gray-100 z-50 lg:shadow-md whitespace-nowrap overflow-hidden transition-all duration-300 ${openMenu?'max-w-full':'max-w-0'}`}>
                        <div className="w-full p-2">
                            <CategoryBox off={setOpenMenu} />
                        </div>
                    </div>
                </div>
            </div>
            {openMenu &&
            <div className="w-full h-screen fixed left-0 top-0 z-30" onClick={e=>setOpenMenu(false)}></div>
            }
        </>
    )
};

export default Header;