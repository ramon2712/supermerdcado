import { useFormik } from "formik";
import { useState } from "react";
import { StyledForm } from "../../forms/StyledForm";

export default (props)=>{
    const [currentPasswordType, setCurrentPasswordType] = useState(true);
    const [newPasswordType, setNewPasswordType] = useState(true);

    const handleSubmit=(values,{setSubmitting})=>{

    }
    const formik=useFormik({
        initialValues:{
            currentPassword:'',
            newPassword:'',
        },
        onSubmit:handleSubmit
    })
    return(
        <div className="w-full flex flex-start flex-wrap p-2 bg-white rounded mb-2">
            <div className="w-full h-10 flex flex-start mb-4">
                <h2 className="text-lg flex-grow text-dark-100 lg:text-base">Modificar contraseña</h2>
            </div>
            <div className="w-full">
                <StyledForm method="post" onSubmit={formik.handleSubmit}>
                    <div className="form-group">
                        <label className="w-full">Contraseña actual</label>
                        <input type={currentPasswordType?'password':'text'} name="currentPassword" value={formik.values.currentPassword} onChange={formik.handleChange} className="" placeholder="" />
                        <button type="button" onClick={e=>setCurrentPasswordType(!currentPasswordType)} className="simple">
                            <i className={`lar ${currentPasswordType?'la-eye-slash':'la-eye'}`}></i>
                        </button>
                    </div>
                    <div className="form-group">
                        <label className="w-full">Nueva contraseña</label>
                        <input type={newPasswordType?'password':'text'} name="newPassword" value={formik.values.newPassword} onChange={formik.handleChange} className="" placeholder="" />
                        <button type="button" onClick={e=>setNewPasswordType(!newPasswordType)} className="simple">
                            <i className={`lar ${newPasswordType?'la-eye-slash':'la-eye'}`}></i>
                        </button>
                    </div>
                    <div className="w-full flex flex-end">
                        <button type="submit" className="dashButton outlined">Modificar</button>
                    </div>
                </StyledForm>
            </div>
            
        </div>
    )
}