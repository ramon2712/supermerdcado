import { useFormik } from "formik"
import { useContext, useEffect, useState } from "react";
import AppContext from "../../../context/Application/AppContext";
import { StyledForm } from "../../forms/StyledForm"
import PasswordForm from "./PasswordForm";

export default (props)=>{
    const [infoReadOnly, setInfoReadOnly] = useState(false);
    const [loading, setLoading] = useState(true);
    const [formValues, setFormValues] = useState({
        name:'',
        lastname:'',
        email:'',
        phone:'',
        rfc:''
    })

    const {user,setUser}=useContext(AppContext);

    const handleSubmit=(values,{setSubmitting})=>{

    }
    const formik=useFormik({
        initialValues:{
            ...formValues
        },
        enableReinitialize:true,
        onSubmit:handleSubmit,
    });

    useEffect(()=>{
        setLoading(false);
        setFormValues({
            name:user.name,
            lastname:user.lastname,
            email:user.email,
            phone:user.phone,
            rfc:user.rfc
        });
    },[]);
    return(
        <div className="w-full px-2 lg:w-3/4 xl:w-7/12 lg:px-0 mx-auto h-96">
            <div className="pt-8 w-full">
                <div className="w-full flex flex-start flex-wrap p-2 bg-white rounded mb-4">
                    <div className="w-full h-10 flex flex-start mb-4">
                        <h2 className="text-lg flex-grow text-dark-100 lg:text-base">Información de tu cuenta</h2>
                    </div>
                    <div className="w-full">
                        <StyledForm>
                            <div className="form-group">
                                <label>Correo electrónico</label>
                                <input type="text" name="email" value={formik.values.email} onChange={formik.handleChange} readOnly className="w-full" placeholder="Dirección de correo electrónico" />
                            </div>
                            <div className="form-group">
                                <label>Nombre(s)</label>
                                <input type="text" name="name" value={formik.values.name} onChange={formik.handleChange} readOnly={infoReadOnly} className="w-full" placeholder="Nombre" />
                            </div>
                            <div className="form-group">
                                <label>Apellido(s)</label>
                                <input type="text" name="lastname" value={formik.values.lastname} onChange={formik.handleChange} readOnly={infoReadOnly} className="w-full" placeholder="Apellidos" />
                            </div>
                            <div className="form-group">
                                <label>Teléfono celular</label>
                                <input type="text" name="phone" value={formik.values.phone} onChange={formik.handleChange} readOnly={infoReadOnly} className="w-full" placeholder="Teléfono celular" />
                            </div>
                            <div className="form-group">
                                <label>RFC</label>
                                <input type="text" name="email" value={formik.values.rfc} onChange={formik.handleChange} readOnly className="w-full uppercase" placeholder="Dirección de correo electrónico" />
                            </div>
                            {!infoReadOnly &&
                            <div className="w-full h-10 flex flex-end">
                                {loading ?
                                <div className="loading-spin mr-2"></div>
                                :
                                <button type="submit" className="btn-primary dashButton">Guardar información</button>}
                            </div>
                            }
                        </StyledForm>
                    </div>
                </div>
                <PasswordForm />
            </div>
        </div>
    )
}