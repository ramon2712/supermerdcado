import { useContext } from "react"
import { NavLink, Redirect, Route, Switch } from "react-router-dom"
import AppContext from "../../context/Application/AppContext"
import UserInformation from "./information/UserInformation"
import Orders from "./Orders/vwOrders"

export default (props)=> {
    const {user}=useContext(AppContext);
    return(
        <>
        {
        user.ready &&
            <>
                {   user.auth?
                    <div className="w-full h-full-outheader flex flex-wrap flex-col-reverse lg:flex-row">
                        <div className="w-full h-20 bg-gray-200 lg:w-22 lg:h-full flex flex-center">
                            <div className="h-full lg:w-full lg:h-auto">
                                <NavLink to="/mi-cuenta/informacion" activeClassName="border-primary bg-black bg-opacity-5" className="w-20 h-20 lg:w-22 lg:h-14 mx-0.5 lg:mx-0 lg:my-0.5 p-2 inline-flex flex-center text-gray-600 border-t-4 lg:border-t-0 lg:border-l-4 border-transparent">
                                    <div className="text-center">
                                        <i className="lar la-user-circle text-4xl"></i>
                                        <span className="block text-xs">Informes</span>
                                    </div>
                                </NavLink>
                                <NavLink to="/mi-cuenta/ordenes" activeClassName="border-primary bg-black bg-opacity-5" className="w-20 h-20 lg:w-22 lg:h-14 mx-0.5 lg:mx-0 lg:my-0.5 p-2 inline-flex flex-center text-gray-600 border-t-4 lg:border-t-0 lg:border-l-4 border-transparent">
                                    <div className="text-center">
                                        <i className="las la-shopping-basket text-4xl"></i>
                                        <span className="block text-xs">Ordenes</span>
                                    </div>
                                </NavLink>
                                <NavLink to="/mi-cuenta/facturacion" activeClassName="border-primary bg-black bg-opacity-5" className="w-20 h-20 lg:w-22 lg:h-14 mx-0.5 lg:mx-0 lg:my-0.5 p-2 inline-flex flex-center text-gray-600 border-t-4 lg:border-t-0 lg:border-l-4 border-transparent">
                                    <div className="text-center">
                                        <i className="las la-file-invoice-dollar text-4xl"></i>
                                        <span className="block text-xs">Facturación</span>
                                    </div>
                                </NavLink>
                                <NavLink to="/mi-cuenta/rastreo" activeClassName="border-primary bg-black bg-opacity-5" className="w-20 h-20 lg:w-22 lg:h-14 mx-0.5 lg:mx-0 lg:my-0.5 p-2 inline-flex flex-center text-gray-600 border-t-4 lg:border-t-0 lg:border-l-4 border-transparent">
                                    <div className="text-center">
                                        <i className="las la-shipping-fast text-4xl"></i>
                                        <span className="block text-xs">Rastrear</span>
                                    </div>
                                </NavLink>
                                <NavLink to="/logout" activeClassName="border-primary bg-black bg-opacity-5" className="w-20 h-20 lg:w-22 lg:h-14 mx-0.5 lg:mx-0 lg:my-0.5 p-2 inline-flex flex-center text-gray-600 border-t-4 lg:border-t-0 lg:border-l-4 border-transparent">
                                    <div className="text-center">
                                        <i className="las la-door-open text-4xl"></i>
                                        <span className="block text-xs">Cerrar sesión</span>
                                    </div>
                                </NavLink>
                            </div>
                        </div>
                        <div className="w-full h-0 flex-grow bg-gray-100 lg:w-0 lg:h-full overflow-auto">
                            <Switch>
                                <Route exact path="/mi-cuenta/informacion" component={UserInformation} />
                                <Route exact path="/mi-cuenta/ordenes" component={Orders} />
                            </Switch>
                        </div>
                    </div>
                    :
                    <Redirect to="/login" />
                }
            </>
        }
        </>
    )
}