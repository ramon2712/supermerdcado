import axios from "axios";
import { useContext, useEffect, useState } from "react";
import { Link,useHistory } from "react-router-dom";
import AppContext from "../../../context/Application/AppContext";
import { useTinyModalDialog } from "../../Utils/tinymodalmx/tinymodal";
import {Table} from "../../styledComponents/Table"
import moment from "moment";
import LoadingSpinner from "../../htmlComponents/loading"

const ViewOrders = (props)=>{

    const {user }= useContext(AppContext);
    const [orders,setOrders] = useState([]);
    const [orderView,setOrderView]=useState([]);
    const [openModal, setOpenModal] = useState(false);
    const [loading,setLoading] = useState(true);

    const [total, setTotal]= useState(0);
    const [paymentStatus, setPaymentStatus]= useState("");
    const [orderStatus, setOrderStatus]= useState("");
    const [taxes, setTaxes]= useState(0);
    const [subtotal, setSubtotal]= useState("");
    const [paymentType, setPaymentType] = useState("")

    const Modal = useTinyModalDialog();
    const [tipoModal,setTipoModal]=useState(1);
    const history=useHistory();

    const getOrders = async () =>{
        if (user.auth) {
            await axios.post('ordersProd', {oid:user.id}).then((res) => {
                console.log(res);
                setOrders(res.data.products);
                setLoading(false);
                //setFecha(orders.created_at.toLocaleDataString("es-ES",options));
                //console.log(orders)           
            }).catch(error =>{
                console.log(error.res)
            });
        } 
    }

    const getOrdersProducts = async (order) =>{
        //console.log("Manda esta orden al modal de ver detalle")
        //console.log(order)
        //console.log(order.id)
        setTotal(order.amount);
        setPaymentStatus(order.name_payment_status);
        setOrderStatus(order.name_status);
        setTaxes(order.net_taxes);
        setSubtotal(order.subtotal);
        if (user.auth) {
            await axios.post('viewOrdersProd', {oid:order.id}).then((res) => {
                //console.log(res.data.orders);
                setOrderView(res.data.orders);
                //console.log(orderView)                        
            }).catch(error =>{
                console.log(error.res)
            });
        }
    }
    const getPaymentShedules = (orderId) =>{
        console.log(orderId);
        axios.post('paymentshedules',{oid:orderId}).then((res) => {
            //console.log("DEDO")
            //console.log(res.data[0].payment_type_name); 
            setPaymentType(res.data[0].payment_type_name)
            return false
        }).catch(error =>{
            console.log(error.res)
        });
         
    }



    useEffect(() => {
        if(loading) getOrders();
        //getPaymentShedules(140);
    }, [loading])
    return(
        <>
        <div className="w-full px-0.5">
            <h2 className="text-5xl flex flex-center my-1">Tus órdenes</h2>
            <h1 className="flex flex-center my-1">{`${orders.length} compras en total.`}</h1>
            {loading ? 
                <LoadingSpinner loading={loading}/>
            :
            <div className="w-full flex flex-wrap flex-center"
            >           <>
                        {orders.map((order, index) => (
                            
                            <div
                            
                                key={index}
                                className="flex w-full md:w-4/5 inline-flex border rounded-xl shadow-lg my-2 bg-white"
                            >

                                <div className="w-5/6 flex ml-5 p-5">
                                        <div className="flex flex-center mr-2">
                                            <img src="images/order.png" className="w-12 h-12"></img>
                                        </div>
                                        <div className="">
                                            <h3 className="text-xl">{`ID de compra: ${order.id}`}</h3>
                                            <p className="text-secondary font-normal"><strong>Total de la compra : </strong>${order.amount}</p>
                                            <p className="text-secondary font-normal"><strong>{`Fecha de la compra: `}</strong> {moment(order.created_at).locale('es').format('LLL')}</p>
                                        </div>
                                       
                                </div>
                                <div className="w-1/6 border-l-2 flex flex-col flex-center">
                                        <small className="my-4">Acciones</small>
                                        <button className="bg-green-500 text-white w-full hover:bg-green-700  py-1 text-xs mt-2"
                                                onClick={(e) => {
                                                    setTipoModal(1);
                                                    getOrdersProducts(order);
                                                    setOpenModal(!openModal);
                                                    getPaymentShedules(order.id);
                                                }}>
                                            <i className="las la-eye text-xs mx-1"></i>
                                            <span>Ver Detalle</span>
                                        </button>
                                        <button className="bg-green-500 text-white w-full hover:bg-green-700 py-1 text-xs mt-2"
                                                onClick={(e) =>{
                                                    setTipoModal(2);
                                                    setOpenModal(!openModal);
                                                }}>
                                            <i className="las la-file-invoice"></i>
                                            <span>Requerir Factura</span>
                                        </button>
                                        {order.paymentSchedule === 0 &&
                                        <button className="bg-green-500 text-white w-full hover:bg-green-700 py-1 text-xs mt-2 mx-2">
                                            <i className="las la-file-invoice"></i>
                                            <Link to={`/check/${order.id}`}>Concluir compra</Link>
                                        </button>}
                                </div>
                            </div>
                        ))}
                        </>
                    </div>
            }
            
        </div>
        <Modal.TinyModalFrame on={openModal} >
            {tipoModal === 1 ?
            (<>
            <h1 className="text-2xl text-primary flex flex-center">Detalles de tu compra</h1>
            <div className="w-full p-12">
                <Table>
                    <thead>
                        <tr>
                            <th className="w-4/12">Producto</th>
                            <th className="w-1/12">Cantidad</th>
                            <th className="w-1/12">Subtotal</th>
                            <th className="w-1/12">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        {orderView.map((item,index)=>(
                            <tr key={'row_'+index}>
                                    <>
                                    <td>
                                        {item.product_name}
                                    </td>
                                    <td className="">
                                        {item.quantity}
                                    </td>
                                    <td>${item.price}</td>
                                    <td>${item.amount}</td>
                                    </>
                            </tr>
                        ))}
                    </tbody>
                </Table>
            </div>                      
            <div className="w-full space-x-4">
                <div className="ml-12">
                    <p className="font-bold text-xl">Más detalles</p>
                    <p className="text-secondary font-normal"><strong>Tipo de pago : </strong>{paymentType}</p>
                    <p className="text-secondary font-normal"><strong>Estado del pago : </strong>{paymentStatus}</p>
                    <p className="text-secondary font-normal"><strong>Estado del pedido : </strong>{orderStatus}</p>
                    <p className="text-secondary font-normal"><strong>Subtotal : </strong>${subtotal}</p>
                    <p className="text-secondary font-normal"><strong>Impuestos : </strong>${taxes}</p>
                    <p className="text-secondary font-normal"><strong>Total : </strong>${total}</p>
                    
                </div>
                <div className="flex flex-end">
                    <button
                        onClick={(e) => setOpenModal(!openModal)}
                        className="bg-primary h-10 rounded text-white p-2"
                    >
                        Aceptar
                    </button>
                </div>
            </div>
            </>): tipoModal === 2 &&
            <>
             <span>Requiriendo Factura...</span>     
             <div className="w-full flex flex-end space-x-4">
                <button
                    onClick={(e) => setOpenModal(!openModal)}
                    className="bg-primary h-10 rounded text-white p-2"
                >
                    Aceptar
                </button>
            </div>          
            </>}
        </Modal.TinyModalFrame>                                        
        </>
    );
}

export default ViewOrders;