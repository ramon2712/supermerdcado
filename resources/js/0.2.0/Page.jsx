import axios from "axios";
import { useContext, useEffect, useState } from "react";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Login from "./components/Authenticate/Login/Login";
import Register from "./components/Authenticate/Register/Register";
import AppContext from "./context/Application/AppContext";
import Store from "./views/Store/Store";
import ResetPassword from "./components/Authenticate/ResetPassword/ResetPassword"
import pageErrors from "./components/htmlComponents/pageErrors"

const Page = props => {
    const [loading, setLoading] = useState(true)
    const {user,setUser,initCart}=useContext(AppContext);

    useEffect(()=>{
        if(loading){
            //console.log("Hola");
            setLoading(false);
            axios.post('react-application')
            .then(response=>{
                const {data}=response;
                setUser({...data.user,ready:true,auth:data.auth});
                initCart(data.auth);
                console.log("Page ",response)
            });
        }

    });
    return(
        <Router>
            <Switch>
                <Route exact path="/login" component={Login} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/password-reset" component={ResetPassword} />
                <Route component={Store} />
            </Switch>
        </Router>
    )
};
export default Page;