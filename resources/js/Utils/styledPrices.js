export default (value) => {
    const val=value?.split('.') || [0];
    return(
        <span>{val[0]}.<sup>{(val.length>1)?val[1]:'00'}</sup></span>
    )
};