<html>

    <head>
        <base href="{{asset('/')}}" />

        <!-- SEO Meta Tags -->
        <meta name="viewport" content="width=device-width,initial-scale=1"/>

        <title>Supermercado al más barato</title>
        <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
        <link href="{{asset('css/app.css')}}" rel="stylesheet" />
        <link rel="shortcut icon" type="image/ico" href="/images/favicon.svg"/>
        <script type="text/javascript" src="https://cdn.conekta.io/js/latest/conekta.js"></script>
    </head>

    <body>
        <div id="root"></div>
        <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
    <body>

</html>