<!DOCTYPE html>
<html lang="en">
<head>
    <title>Mensaje de contacto recibido</title>
</head>
<body>
    <p>Correo recibido de: {{$contactData['name']}} {{$contactData['email']}}</p>
    <p>Asunto: {{$contactData['subjet']}}</p>
    <p>Contenido del mensaje; {{$contactData['message']}}</p>
</body>
</html>