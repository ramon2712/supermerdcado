<?php

use App\Http\Controllers\Api\CategoriesController;
use App\Http\Controllers\Api\ProductImagesController;
use App\Http\Controllers\Api\UsersAdressesController;
use App\Http\Controllers\Auth\VerifyController;
use App\Http\Controllers\FilesController;
use App\Http\Controllers\LoginCartController;
use App\Http\Controllers\productsController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\searchController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\DepartamentsController;
use App\Http\Controllers\SubdepartamentsController;
use App\Http\Controllers\Web\CookieCartController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Notifications\ResetPasswordNotification;
use App\Http\Controllers\Api\ScheduleController;
use App\Http\Controllers\PaymentSchedulesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/pruebas', [searchController::class,'improvedSearch']);


Route::post('authenticate/check',function(){
    if(Auth::check()){
        return response()->json(['auth'=>true,'user'=>Auth::user()]);
    }
    return response()->json(['auth'=>false]);
});

Route::get('/cookie/cart', [CookieCartController::class,'getCartCookie']);
Route::post('/cookie/cart', [CookieCartController::class,'setCartCookie']);
Route::post('/orderSchedule', [ScheduleController::class,'show']);
Route::post('/cookie/addcart', [LoginCartController::class,'addCartCookie']);
Route::post('/search',[searchController::class,'doSearch'])->name('search');
Route::get('/register/verify/{code}',[VerifyController::class,'verify'])->name('verify');
Route::get('/password/reset/{code}',[VerifyController::class,'reset'])->name('reset');
Route::get('logout',function(){
    Auth::logout();
    return response()->json(['auth'=>false]);
});

Route::group(['prefix'=>'ajax'],function(){
    Route::post('loadCategories',[CategoriesController::class,'menuList'])->name('menuList');
});

/* ALL GET HERE */
Route::get('product/images/{folder}/{filename}',[FilesController::class,'selectProductImage'])->name('getImage');
Route::get('product/departaments/{filename}',[FilesController::class,'selectProductImage'])->name('getImage');

/* ALL POST HERE */
Route::post('/paymentshedules', [PaymentSchedulesController::class,'payment'])->name('paymentshedules');
Route::post('sendEmail',[ResetPasswordNotification::class,'sendEmailContact'])->name('contact');
Route::post('/sendScheduleOxxo',[EmailController::class,'sendRecieptEmail'])->name('schedule');
Route::post('/sendScheduleSpei',[EmailController::class,'sendRecieptEmailSpei'])->name('scheduleSpei');
Route::post('/pruebaEmail',[EmailController::class,'sendEmailContact'])->name('emailContact');
Route::post('ordersProd',[OrdersController::class,'listOrdersProducts'])->name('ordersProducts');
Route::post('viewOrdersProd',[OrdersController::class,'listViewOrdersProducts'])->name('viewOrdersProducts');
Route::post('loadProductsList',[productsController::class,'list'])->name('productsList');
Route::post('loadProductsCat',[productsController::class,'listCategories'])->name('productsCat');

Route::post('loadSubdepartaments',[SubdepartamentsController::class,'listSubDepartaments'])->name('subdepartaments');
Route::post('loadDepartaments',[DepartamentsController::class,'listDepartaments'])->name('Departaments');
Route::post('listProductsBrand',[productsController::class,'listProductsBrand'])->name('productsBrand');
Route::post('product/details',[productsController::class,'details'])->name('productDetails');
Route::post('product/datasheet',[productsController::class,'datasheet'])->name('productDatasheet');
Route::post('product/categories',[CategoriesController::class,'list'])->name('getcategories');
Route::post('react-application',function(){
    return response()->json(['user'=>Auth::user(),'auth'=>Auth::check()]);
});

Route::get('{all?}', function(){ return view('react.index'); })
->where('all', '.+');


Auth::routes();



// Route::get('/',[ContentController::class, 'getHome']);
// Route::get('/detail/{id}',[ProductController::class, 'detail']);
// Route::get('categories/{slug}',[ContentController::class, 'searchCategory'])->name('categories');
// Auth::routes();
// Route::get('/perfil',[UserController::class, 'index'])->name('profile');
// Route::get('/perfil/{id}',[UserController::class, 'edit'])->name('edit-profile');
// Route::resource('address', AddressController::class, ['except'=>'show','create', 'edit']);

// Route::apiResource('api/cart', 'App\Http\Controllers\Api\CartController');
// Route::get('/cart',[App\Http\Controllers\Web\CartController::class, 'index']);
// Route::get('/home', [App\Http\Controllers\Web\HomeController::class, 'index'])->name('home');
// Route::post('/cookie/cart', [App\Http\Controllers\Web\CookieCartController::class, 'setCartCookie'])->name('setCartCookies');
// Route::get('/cookie/cart', [App\Http\Controllers\Web\CookieCartController::class, 'getCartCookie'])->name('getCartCookies');
// Route::put('/cookie/cart/{id}', [App\Http\Controllers\Web\CookieCartController::class, 'editCartCookie'])->name('updateCartCookies');
// Route::delete('/cookie/cart/{id}', [App\Http\Controllers\Web\CookieCartController::class, 'deleteCookieItem'])->name('deleteCartCookies');
