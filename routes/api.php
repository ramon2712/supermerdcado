<?php

use App\Http\Controllers\Api\oxxopay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

///////////////////// TEST /////////////////////////////

use App\Http\Controllers\Api\AdministratorsController;
use App\Http\Controllers\Api\AppController;
use App\Http\Controllers\Api\AuthApiController;
use App\Http\Controllers\Api\BrandsController;
use App\Http\Controllers\Api\CategoriesController;
use App\Http\Controllers\Api\DepartamentsController;
use App\Http\Controllers\Api\FreightersController;
use App\Http\Controllers\Api\PaymentsController;
use App\Http\Controllers\Api\PermissionsController;
use App\Http\Controllers\Api\ProductImagesController;
use App\Http\Controllers\Api\ProductPricesController;
use App\Http\Controllers\Api\ProductsController;
use App\Http\Controllers\Api\RolesController;
use App\Http\Controllers\Api\SecValuesController;
use App\Http\Controllers\Api\WarehousesController;
use App\Http\Controllers\Api\DeliveryTimesController;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Api\Utils;
use App\Http\Controllers\FilesController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\Web\UserController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\Web\CookieCartController;
use App\Http\Controllers\Api\ProductDatasheetsController;
use App\Http\Controllers\Api\ViewProductSecsController;
use App\Http\Controllers\Api\ProductSecsController;
use App\Http\Controllers\Api\CartController;
use App\Http\Controllers\CardPaymentController;
use App\Http\Controllers\speiController;

////////////////////////////////////////////////////////

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::apiResource('/cart', 'App\Http\Controllers\Api\CartController');
Route::apiResource('/orders', 'App\Http\Controllers\Api\OrdersController');
Route::apiResource('/orders/products', 'App\Http\Controllers\Api\OrderProductsController');
Route::apiResource('/address', 'App\Http\Controllers\AddressController');
Route::apiResource('/checkout/payments', 'App\Http\Controllers\Api\CheckPayController');
Route::apiResource('/checkout/schedules', 'App\Http\Controllers\Api\ScheduleController');
Route::apiResource('/checkout/order/payments', 'App\Http\Controllers\Api\OrderPaymentController');
Route::apiResource('/cp/colonias', 'App\Http\Controllers\Api\ColoniasController', ['except'=>'create','index', 'edit', 'destroy']);
Route::post('/card',[CardPaymentController::class, 'createCustomer']);
Route::post('/oxxo',[oxxopay::class, 'newOxxoPay']);
Route::post('/spei',[speiController::class, 'createCustomer']);

/*  Wichi   */
Route::middleware(['api.ajax'])->group(function(){
    Route::post('login/attempt',[AuthApiController::class,'attempt'])->name('loginAttempt');
});


Route::get('files/images/{folder}/{filename}',[ProductImagesController::class,'select'])->name('getImage');

Route::group(['prefix'=>'auth'],function(){
    Route::middleware('auth:api')->group(function(){
        Route::post('initApplication',[AppController::class,'initApplication'])->name('initApplication');
        Route::post('login/check',[AuthApiController::class,'check'])->name('loginCheck');
        Route::post('logout',[AuthApiController::class,'logout'])->name('apiLogout');
        Route::post('login/getUser',[AuthApiController::class,'getUser'])->name('loginGetUser');
        

        /* FILES */
        Route::get('files/receipts/images/{filename}',[FilesController::class,'selectImage'])->name('getReceiptImage');
        Route::get('files/receipts/documents/{filename}',[FilesController::class,'selectDocument'])->name('getReceiptDocument');
        /* SELECT */
        Route::group(['prefix'=>'select'],function(){
            Route::post('menus',[AppController::class,'selectMenus'])->name('selectMenus');
            Route::post('productsData',[ProductsController::class,'select'])->name('productsData');
            Route::post('productData',[ProductsController::class,'selectOne'])->name('productData');
            Route::post('productPrices',[ProductPricesController::class,'selectOne'])->name('productPrices');
            Route::post('categories',[CategoriesController::class,'select'])->name('selectCategories');
            Route::post('categoriesOne',[CategoriesController::class,'selectOne'])->name(('selectCategoriesOne'));
            Route::post('roles',[RolesController::class,'select'])->name(('selectRoles'));
            Route::post('rolesOne',[RolesController::class,'selectOne'])->name('selectRolesOne');
            Route::post('adminusers',[AdministratorsController::class,'select'])->name('selectAdminusers');
            Route::post('adminusersOne',[AdministratorsController::class,'selectOne'])->name('selectAdminusersOne');
            Route::post('freighters',[FreightersController::class,'select'])->name('selectFreighters');
            Route::post('freightersOne',[FreightersController::class,'selectOne'])->name('selectFreightersOne');
            Route::post('payments',[PaymentsController::class,'select'])->name('selectPayments');
            Route::post('paymentsOne',[PaymentsController::class,'selectOne'])->name('selectPaymentsOne');
            Route::post('products',[ProductsController::class,'select'])->name('selectProducts');
            Route::post('productsOne',[ProductsController::class,'selectOne'])->name('selectProductsOne');
            Route::post('productOneWithCode',[ProductsController::class,'selectOneWithCode'])->name('selectProductsOneWithCode');
            Route::post('productImages',[ProductImagesController::class,'selectForProduct'])->name('selectProductImages');
            Route::post('warehouses',[WarehousesController::class,'select'])->name('selectWarehouses');
            Route::post('warehousesOne',[WarehousesController::class,'selectOne'])->name('selectWarehousesOne');
            Route::post('warehousesWithStock',[WarehousesController::class,'selectWarehousesWithStock'])->name('selectWarehousesWithStock');
            Route::post('warehousesWithStockOne',[WarehousesController::class,'selectWarehousesWithStockOne'])->name('selectWarehousesWithStockOne');
            Route::post('brands',[BrandsController::class,'select'])->name('selectBrands');
            Route::post('brandsOne',[BrandsController::class,'selectOne'])->name('selectBrandsOne');
            Route::post('departaments',[DepartamentsController::class,'select'])->name('selectDepartaments');
            Route::post('departamentsOne',[DepartamentsController::class,'selectOne'])->name('selectDepartamentsOne');
            Route::post('orders',[OrdersController::class,'select'])->name('selectOrders');
            Route::post('ordersOne',[OrdersController::class,'selectOne'])->name('selectOrdersOne');
            Route::post('orderPayment',[OrdersController::class,'selectOrderPayment'])->name('selectOrderPayment');
            Route::post('orderPaymentSchedules',[OrdersController::class,'selectPaymentSchedules'])->name('selectOrderPaymentSchedules');
            Route::post('orderInventoryList',[OrdersController::class,'selectInventoryList'])->name('selectOrderInventoryList');
            Route::post('orderPaymentHistory',[OrdersController::class,'selectPaymentHistory'])->name('selectPaymentHistory');
            Route::post('userAddresses',[UsersController::class,'selectUserAddresses'])->name('selectUserAddresses');
            //desde aqui todo es de marce XD
            Route::post('deliveryTime',[DeliveryTimeController::class,'select'])->name('selectDeliveryTime');  //echo por marce
            Route::post('deliveryTimeOne',[DeliveryTimeController::class,'selectOne'])->name('selectDeliveryTimeOne');  //echo por marce
            Route::post('paymentPartialTypes',[PaymentPartialTypesController::class,'select'])->name('selectPaymentPartialTypes');  //echo por marce
            Route::post('paymentPartialTypesOne',[PaymentPartialTypesController::class,'selectOne'])->name('selectPaymentPartialTypesOne');  //echo por marce
            Route::post('productDatasheet',[ProductDatasheetsController::class,'select'])->name('selectProductDatasheet');  //echo por marce
            Route::post('productDatasheetOne',[ProductDatasheetsController::class,'selectOne'])->name('selectProductDatasheetOne');  //echo por marce
            Route::post('viewProductSecs',[ViewProductSecsController::class,'select'])->name('selectViewProductSecs');  //echo por marce
            Route::post('productSecs',[ProductSecsController::class,'select'])->name('selectProductSecs');  //echo por marce
            Route::post('productSecsOne',[ProductSecsController::class,'selectOne'])->name('selectProductSecsOne');  //echo por marce
            Route::post('secsValues',[SecValuesController::class,'select'])->name('selectSecsValues');  //echo por marce
            //hasta aqui todo es de marce XD


            Route::group(['prefix'=>'list'],function(){
                Route::post('subcategories',[CategoriesController::class,'list'])->name('listSubcategories');
                Route::post('brands',[BrandsController::class,'list'])->name('listBrands');
                Route::post('permissions',[PermissionsController::class,'list'])->name('listPermissions');
                Route::post('roles',[RolesController::class,'list'])->name('listRoles');
                Route::post('categories',[CategoriesController::class,'list'])->name('listCategories');
                Route::post('orders',[OrdersController::class,'list'])->name('listOrders');
                Route::post('clients',[UserController::class,'list'])->name('listClients');
                Route::post('paymentTypes',[PaymentsController::class,'listPaymentTypes'])->name('listPaymentTypes');
                Route::post('deliveryTimes',[DeliveryTimesController::class,'list'])->name('listDeliveryTimes');
                Route::post('orderTypes',[OrdersController::class,'listOrderTypes'])->name('listOrderTypes');
                Route::post('productWithStock',[OrdersController::class,'productWithStock'])->name('listProductWithStock');
            });

            Route::group(['prefix'=>'utils'],function(){
                Route::post('getAddress',[Utils::class,'getAddress'])->name('utilsGetAddress');
            });
        });
        /* FOR GIT */

        /* INSERT */
        Route::group(['prefix'=>'create'],function(){
            Route::post('product',[ProductsController::class,'insert'])->name('insertProduct');
            Route::post('products',[ProductsController::class,'insert'])->name('insertProducts'); /* <-edit line to Masive Insert */
            Route::post('categories',[CategoriesController::class,'insert'])->name('insertCategories');
            Route::post('roles',[RolesController::class,'insert'])->name('insertRoles');
            Route::post('adminuser',[AdministratorsController::class,'insert'])->name('insertAdminuser');
            Route::post('freighters',[FreightersController::class,'insert'])->name('insertFreighters');
            Route::post('payments',[PaymentsController::class,'insert'])->name('insertPayments');
            Route::post('warehouses',[WarehousesController::class,'insert'])->name('selectWarehouses');
            Route::post('brands',[BrandsController::class,'insert'])->name('selectBrands');
            Route::post('departaments',[DepartamentsController::class,'insert'])->name('insertDepartaments');
            Route::post('orders',[OrdersController::class,'insert'])->name('insertOrders');
            Route::post('userAddress',[UsersController::class,'insertAddress'])->name('insertUserAddress');
            //desde aqui todo es de marce XD
            Route::post('deliveryTime',[DeliveryTimeController::class,'insert'])->name('selectDeliveryTime');  //echo por marce
            Route::post('paymentPartialTypes',[PaymentPartialTypesController::class,'insert'])->name('selectPaymentPartialTypes');  //echo por marce
            Route::post('productDatasheet',[ProductDatasheetsController::class,'insert'])->name('insertProductDatasheet'); //echo por marce
            Route::post('productSecs',[ProductSecsController::class,'insert'])->name('insertProductSecs'); //echo por marce
            Route::post('secsValues',[SecValuesController::class,'insert'])->name('insertSecsValues');//echo por marce
            //hasta aqui todo es de marce XD
        });

        /* UPDATE */
        Route::group(['prefix'=>'set'],function(){
            Route::post('product',[ProductsController::class,'update'])->name('updateProduct');
            Route::post('products',[ProductsController::class,'update'])->name('updateProducts');/* <-edit line to Masive Update */
            Route::post('productPrices',[ProductPricesController::class,'update'])->name('productPrices');
            Route::post('rolesPermissions',[PermissionsController::class,'update'])->name('setRolesPermissions');
            Route::post('categories',[CategoriesController::class,'update'])->name('setCategories');
            Route::post('roles',[RolesController::class,'update'])->name('setRoles');
            Route::post('adminuser',[AdministratorsController::class,'update'])->name('setAdminuser');
            Route::post('freighters',[FreightersController::class,'update'])->name('setFreighters');
            Route::post('payments',[PaymentsController::class,'update'])->name('setPayments');
            Route::post('warehouses',[WarehousesController::class,'update'])->name('selectWarehouses');
            Route::post('warehouseStock',[WarehousesController::class,'updateStock'])->name('selectWarehouseStock');
            Route::post('brands',[BrandsController::class,'update'])->name('selectBrands');
            Route::post('departaments',[DepartamentsController::class,'update'])->name('updateDepartaments');
            Route::post('orders',[OrdersController::class,'update'])->name('updateOrders');
            Route::post('orderAuthorize',[OrdersController::class,'updateAuthorize'])->name('updateOrderAuthorize');
            Route::post('orderRecreate',[OrdersController::class,'recreate'])->name('orderRecreate');
            Route::post('orderValidateInventory',[OrdersController::class,'validateInventory'])->name('orderValidateInventory');
            Route::post('userAddress',[UsersController::class,'updateAddress'])->name('updateUserAddress');
            //desde aqui todo es de marce XD
            Route::post('deliveryTime',[DeliveryTimeController::class,'update'])->name('selectDeliveryTime');  //echo por marce
            Route::post('paymentPartialTypes',[PaymentPartialTypesController::class,'update'])->name('selectPaymentPartialTypes');  //echo por marce
            Route::post('productDatasheet',[ProductDatasheetsController::class,'update'])->name('updateProductDatasheet');//echo por marce
            Route::post('productSecs',[ProductSecsController::class,'update'])->name('updateProductSecs');//echo por marce
            Route::post('secsValues',[SecValuesController::class,'update'])->name('updateSecsValues');//echo por marce
            //hasta aqui todo es de marce XD
        });

        /* DELETE */
        Route::group(['prefix'=>'delete'],function(){
            
            Route::post('/cookie/cart/',[CookieCartController::class,'deleteCookieItem'])->name('deleteItem');
            Route::post('products',[ProductsController::class,'delete'])->name('updateProducts');
            Route::post('productImage',[ProductImagesController::class,'delete'])->name('delProductImage');
            Route::post('secValue',[SecValuesController::class,'delete'])->name('delSecValue');
            Route::post('categories',[CategoriesController::class,'delete'])->name('delCategories');
            Route::post('roles',[RolesController::class,'delete'])->name('deleteRoles');
            Route::post('adminuser',[AdministratorsController::class,'delete'])->name('deleteAdminuser');
            Route::post('freighters',[FreightersController::class,'delete'])->name('deleteFreighters');
            Route::post('payments',[PaymentsController::class,'delete'])->name('deletePayments');
            Route::post('warehouses',[WarehousesController::class,'delete'])->name('selectWarehouses');
            Route::post('brands',[BrandsController::class,'delete'])->name('selectBrands');
            Route::post('departaments',[DepartamentsController::class,'delete'])->name('deleteDepartaments');
            Route::post('orders',[OrdersController::class,'delete'])->name('deleteOrders');
            Route::post('orderReceipt',[OrdersController::class,'deleteOrderReceipt'])->name('deleteOrderReceipt');
            //desde aqui todo es de marce XD
            Route::post('deliveryTime',[DeliveryTimeController::class,'delete'])->name('selectDeliveryTime');  //echo por marce
            Route::post('paymentPartialTypes',[PaymentPartialTypesController::class,'delete'])->name('selectPaymentPartialTypes');  //echo por marce
            Route::post('productDatasheet',[ProductDatasheetsController::class,'delete'])->name('deleteProductDatasheet');//echo por marce
            Route::post('productSecs',[ProductSecsController::class,'delete'])->name('deleteProductSecs');//echo por marce
            Route::post('secsValues',[SecValuesController::class,'delete'])->name('deleteSecsValues');//echo por marce
            //hasta aqui todo es de marce XD
        });

        /* UPLOAD */
        Route::group(['prefix'=>'upload'],function(){
            Route::post('productImage',[ProductImagesController::class,'upload'])->name('uploadProductImage');
            Route::post('orderImageReceipt',[OrdersController::class,'uploadImage'])->name('uploadOrderImageReceipt');
            Route::post('orderFileReceipt',[OrdersController::class,'uploadFile'])->name('uploadOrderFileReceipt');
        });
    });
});
