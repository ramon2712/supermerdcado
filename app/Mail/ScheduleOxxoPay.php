<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ScheduleOxxoPay extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $reference;
    public $total;
    public $contactData;
    public function __construct($contactData)
    {
        $this->contactData=$contactData;
        $this->reference=$contactData["reference"];
        $this->total=$contactData['total'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                    ->view('emails.oxxoPay');
    }
}

