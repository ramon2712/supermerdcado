<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ScheduleOxxoPay extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $clabe;
    public $total;
    public $bank;
    public $contactData;
    public function __construct($contactData)
    {
        $this->contactData=$contactData;
        $this->clabe=$contactData["clabe"];
        $this->total=$contactData["total"];
        $this->bank=$contactData["bank"];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                    ->view('emails.spei');
    }
}
