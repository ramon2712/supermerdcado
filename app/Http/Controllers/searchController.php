<?php

namespace App\Http\Controllers;

use App\Models\Products;
use App\Models\ViewProducts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class searchController extends Controller
{
    
    public function doSearch(Request $request){
        $q=$request->q;
        $products=ViewProducts::where(['active'=>1])->where('searchText','LIKE',"%$q%")->paginate(8);
        return response()->json(['products'=>$products,'q'=>$q]);

    }
    

    public function improvedSearch(Request $request) {

        $validator = Validator::make($request->all(),[
            'searchText' => 'string',
            'offset' => 'required|numeric',
            'limit' => 'required|numeric',
            'id_brand' => 'numeric',
            'id_category' => 'numeric',
            'min_price' => 'numeric',
            'max_price' => 'numeric'
        ]);

        // $cookie = $request->cookie('client_history'); //Cookie original, descomentar cuando este lista

        $cookie = [2, 64,21 ]; //Cookie de prueba

        if($validator->fails()) {
            return response()->json(['message'=>'Revisa los campos faltantes', 'fields'=>$validator->failed(), 'e'=>1]);
        }   

        $result1 = ViewProducts::active()
            ->search(trim($request->searchText))
            ->brand($request->id_brand)
            ->category($request->id_category)
            ->min($request->min_price)
            ->max($request->max_price)
            ->whereNotIn('id', $cookie)
            ->orderBy('sales', 'DESC')
            ->orderBy('visits', 'DESC')
            ->offset($request->offset)->limit($request->limit)->paginate(10);

        $result2 = ViewProducts::active()
            ->search(trim($request->searchText))
            ->brand($request->id_brand)
            ->category($request->id_category)
            ->min($request->min_price)
            ->max($request->max_price)
            ->whereIn('id', $cookie)
            ->orderBy('sales', 'DESC')
            ->orderBy('visits', 'DESC')
            ->offset($request->offset)->limit($request->limit)->paginate(10);

        $products = $result2->concat($result1);

        if($products->count() < 1){
            return response()->json(['message'=>'No se encontrarón coincidencias', 'e'=>0]);
        }else{
            return response()->json([
                'message'=>'Estos son los resultados encontrados',
                'products'=>$products,
                'category'=>$request->id_category,
                'brand'=>$request->id_brand,
                'oldOffset'=>$request->offset, 
                'oldLimit'=> $request->limit,
                'newOffset'=>$request->offset + "1",
                'newLimit'=>$request->limit + "1",
                'e'=>0
            ]);
        }        
    }
}