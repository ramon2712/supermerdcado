<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class VerifyController extends Controller
{
    public function verify($code)
    {
        $user = User::where('verified_token', $code)->first();
        if (!$user) {
            return redirect('/');
        }

        //$user->confirmed = true;
        $user->verified_token = null;
        $user->save();

        return response()->json("verificando"); //redirect('/');//->with('notification', 'Has confirmado correctamente tu correo!');
    }

    public function reset($token, Request $request)
    {
        $url = $token.'\\';
        $url = $url.$request->email;
        return redirect('password-verify/'.$url);
        return response()->json($url);
    }
}
