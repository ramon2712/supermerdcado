<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Web\Category;
use App\Models\Web\Product;

class ContentController extends Controller
{
    /*public function index(){
        $products= Product::orderBy('id','DESC')->paginate(4);
        $products->each(function($products){
            $products->category;
            $products->id;
        });
    }*/
    //
    public function getHome(){
        /* $categories= Category::where('active','1')->orderBy('name','Asc')->get();
        $products= Product::where('active','1')->orderBy('name','Asc')->get();
        $data= ['categories'=>$categories,'products'=>$products];  */     
        return view('react.index');
      

    }
    public function getHomeIndex(){
        /* $categories= Category::where('active','1')->orderBy('name','Asc')->get();
        $products= Product::where('active','1')->orderBy('name','Asc')->get();
        $data= ['categories'=>$categories,'products'=>$products];  */     
        return view('react.index');
      

    }

    public function searchCategory($name){
        $categories= Category::where('active','1')->orderBy('name','Asc')->get();
        $category = Category::SearchCategory($name)->first();
        $products = $category->products()->paginate(4);
        //dd($products);
        $products->each(function($products){
            $products->name;
            $products->description;
        });
        $data= ['categories'=>$categories,'products'=>$products]; 
        return view('products.show_products',$data);
    }
    
   
}
