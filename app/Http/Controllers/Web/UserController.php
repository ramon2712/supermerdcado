<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\ViewUsers;
use Illuminate\Http\Request;

use App\Models\Web\AddressShipping;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(){
        $data = AddressShipping::orderBy('id', 'DESC')->paginate();
        return view('user.show_profile',['shipping'=>$data]);
    }


    public function edit(){
        return view('user.partials.edit');

    }

    public function list(Request $request){
        $searchText=$request->searchText;
        $users=ViewUsers::where(['id_user_status'=>1])->where('searchText','LIKE','%'.$searchText.'%')->select(['id','name','lastname','email'])->limit(10)->get();
        $list=[];
        foreach($users as $u){
            $list[]=['id'=>$u->id,'text'=>'0'.$u->id.'u'." | $u->name $u->lastname | $u->email"];
        }

        return response()->json(['listUsers'=>$list]);
    }

    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            'name'=>'required|string',
            'lastname'=>'required|string',
            'phone'=>'required|string'
        ]);

        if($validator->fails()){
            return response()->json(['e'=>1,'message'=>'Formulario incompleto','fields'=>$validator->failed()]);
        }
        if(!preg_match("/^[0-9]{3}-[0-9]{4}-[0-9]{4}$/", $request->phone) && !preg_match("/^[0-9]{10}$/", $request->phone)) {
            return response()->json(['e'=>2,'message'=>'Teléfono no válido']);
        }
        $user=User::find(Auth::user()->id);
        $user->name=$request->name;
        $user->lastname=$request->lastname;
        $user->phone->$request->phone;
    }
    
}
