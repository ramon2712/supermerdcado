<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Products;

class ProductController extends Controller
{

    function detail($id){
        
        $data= Products::find($id);
        //dd($data);
        return view('products.detail',['product'=>$data]);
    }

}
