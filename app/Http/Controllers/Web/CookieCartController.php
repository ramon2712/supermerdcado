<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use App\Models\ViewProducts;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Web\Product as WebProduct;
use Illuminate\Support\Facades\Validator;

class CookieCartController extends Controller
{
    public function setCartCookie(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id'=>'required|numeric|exists:products',
            'quantity'=>'required|numeric'
        ]);
        if($validator->fails()){
            return $this->getCartCookie($request);
        }
        $minutes=43200;
        $cart = $request -> cookie('cart');
        $product = ViewProducts::find($request->id);
        if($cart != null){
            $data = json_decode($cart);
            $arraydata = (array)$data;
            $check=false;
            foreach($arraydata as $a){
                if($a->id===$request->id){
                    $a->quantity+=$request->quantity;
                    $check=true;
                }
            }
            if(!$check){
                $arraydata[]=[
                    'id'=>$request->id,
                    'quantity'=>$request->quantity,
                    'name'=>$product->name,
                    'filename'=>$product->filename,
                    'id_user'=>$request->id_user,
                    'sale_price'=>$product->sale_price,
                    'net_sale_price'=>$product->net_sale_price
                ];
            }
            $response= response()->json($arraydata);
            $response->headers->setCookie(cookie('cart',json_encode($arraydata), $minutes));
            return $response;
        }else{
            $data=[];
            $data [] = [
                'id'=>$request->id,
                'quantity'=>$request->quantity,
                'name'=>$product->name,
                'filename'=>$product->filename,
                'id_user'=>$request->id_user,
                'sale_price'=>$product->sale_price,
                'net_sale_price'=>$product->net_sale_price
            ];
            $response= response()->json($data);
            $response->headers->setCookie(cookie('cart',json_encode($data), $minutes));
            return $response;
        }        
    }  

    public function getCartCookie(Request $request)
    {
        $cart = $request -> cookie('cart');
        $data = json_decode($cart);
        return response() -> json($data);
    }

    public function editCartCookie(Request $request, $id){
        $cart = $request -> cookie('cart');
        $data = json_decode($cart);
        $array = (array)$data;
        $dataCart = [];
        $salto=true;
        foreach($array as $item){
            if($item->id === $id){
                $salto = false;
                //$quantity = $item->quantity + $request->quantity;
                $dataCart[] = ['id'=>$item->id, 'quantity'=>$request->quantity];
            }else{
                $dataCart[] = $item;
            }
        }
        $minutes=43200;
        $response= response()->json($dataCart);
        $response->headers->setCookie(cookie('cart',json_encode($dataCart), $minutes));
        return ($response);
    }

    public function deleteCookieItem(Request $request, $id){
        $cart = $request -> cookie('cart');
        $data = json_decode($cart);
        $array = (array)$data;
        $dataCart = [];
        foreach($array as $item){
            if($item->id != $id){
                //$quantity = $item->quantity + $request->quantity;
                $dataCart[] = $item;//['id'=>$item->id, 'quantity'=>$request->quantity];
            }
        }
        $minutes=43200;
        $response = New Response('Cookie de cart modificada');
        $data_json = json_encode($dataCart);
        $response -> withCookie(cookie('cart',$data_json, $minutes));
        return ($response);
    }
}
