<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Departaments;
use App\Models\ViewProducts;
use App\Models\Subdepartaments;

class SubdepartamentsController extends Controller
{
    public function listSubdepartaments(Request $request){
        $cid=$request->cid;
        $subdepartaments=Subdepartaments::where(["id_departament"=>$cid])->get();
        return response()->json($subdepartaments);
    }
}
