<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Conekta\Conekta;
use App\Models\Web\order_product as orderProduct;
use App\Models\ViewOrderProducts;
use App\Models\User;
use App\Models\Web\Order;

class CardPaymentController extends Controller
{
  function __construct(){
    Conekta::setApiKey('key_nUsmpLsws6SPsqW1eRrdhA');
    Conekta::setApiVersion("2.0.0");
  }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createCustomer(Request $request){      
      $products = ViewOrderProducts::where('order_id', $request->order_id)->where('active',1)->get();
      $orderM = Order::find($request->order_id);
      $order_products = [];
      //return response()->json($products);
      foreach($products as $product){
        $order_products[]=["name"=>strval($product->product_name), "unit_price"=>intval(floatval($product->price)*100), "quantity"=>intval($product->quantity),"sku"=>strval($product->sku)];//$product->quantity];
      }
      //return response()->json(["order_products"=>$order_products,"orderM"=>$orderM]);
      //return response()->json($order_products);
      //return response()->json($order_products);
      $user = User::find($orderM->user_id);
        try {
            $customer = \Conekta\Customer::create(
              [
                "name" => $user->name." ".$user->lastname, // "Fulanito Pérez",
                "email" => $user->email, //"fulanito@conekta.com",
                "phone" => $user->phone,
                "metadata" => ["reference" => "12987324097", "random_key" => "random value"],
                "payment_sources" => [
                  [
                    "type" => "card",
                    "token_id" => $request->token_id//"tok_test_visa_4242"
                  ]
                ]//payment_sources
              ]//customer
            );
            //return response()->json($customer);
          } catch (\Conekta\ProccessingError $error){
            //echo $error->getMesage();
            return response()->json($error);
          } catch (\Conekta\ParameterValidationError $error){
            //echo $error->getMessage();
            return response()->json($error);
          } catch (\Conekta\Handler $error){
            //echo $error->getMessage();
            return response()->json($error);
          }
          //return response()->json($customer);
          //Implementación de una orden.
        try{
          $order = \Conekta\Order::create(
            [
              "line_items" => //[
                $order_products
              //   /*[
              //     "name" => "Tacos",
              //     "unit_price" => 1000,
              //     "quantity" => 120
              //   ]
              // ]
              ,
              "shipping_lines" => [
                // /*[
                //   "amount" => 1500,
                //   "carrier" => "FEDEX"
                // ]
              ], //shipping_lines - physical goods only
              "currency" => "MXN",
              "customer_info" => [
                "customer_id" => $customer->id//"cus_2fkJPFjQKABcmiZWz"
              ],
              "shipping_contact" => [
                "address" => [
                  "street1" => "Calle 123, int 2",
                  "postal_code" => "06100",
                  "country" => "MX"
                ]
              ], //shipping_contact - required only for physical goods
              "metadata" => ["reference" => "12987324097", "more_info" => "lalalalala"],
              "charges" => [
                [
                  "payment_method" => [
                    "type" => "default"
                  ] //payment_method - use customer's default - a card
                    //to charge a card, different from the default,
                    //you can indicate the card's source_id as shown in the Retry Card Section
                ]
              ]
            ]
          );
        } catch (\Conekta\ProcessingError $error){
          //echo $error->getMesage();
          return response()->json($error);
        } catch (\Conekta\ParameterValidationError $error){
          //echo $error->getMesage();
          return response()->json($error);
        } catch (\Conekta\Handler $error){
          //echo $error->getMesage();
          return response()->json($error);
        }
        //return response()->json($order);
        /*$info = [
          "ID: "=> $order->id,
          "Status: "=> $order->payment_status,
          "$"=> $order->amount/100 . $order->currency,
          "Order"=>
          $order->line_items[0]->quantity .
              "-". $order->line_items[0]->name .
              "- $". $order->line_items[0]->unit_price/100,
          "Payment info"=>
          "CODE:". $order->charges[0]->payment_method->auth_code,
          "Card info:"=>
              "- ". $order->charges[0]->payment_method->name .
              "- ". $order->charges[0]->payment_method->last4 .
              "- ". $order->charges[0]->payment_method->brand .
              "- ". $order->charges[0]->payment_method->type
        ];*/
        return response()->json([$order]);
    }
}
