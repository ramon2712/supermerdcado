<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CartItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id'         => $this->id,
            'id_product' => $this->id_product,
            'quantity'   => $this->quantity,
            'created_at' => $this->created_at->diffForHumans()
        ];

    }
}