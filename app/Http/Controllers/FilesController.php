<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FilesController extends Controller
{
    public function selectImage(Request $request,$filename){
        $image=storage_path().'/app/public/receipts/images/'.$filename;
        if(!file_exists($image)){
            $image=storage_path().'/app/public/images/thumbnails/default.jpg';
        }
        return response()->file($image);
    }

    public function selectDocument(Request $request,$filename){
        $document=storage_path().'/app/public/receipts/documents/'.$filename;
        if(!file_exists($document)){
            $document=storage_path().'/app/public/images/thumbnails/default.jpg';
        }
        return response()->file($document);
    }

    public function selectProductImage(Request $request,$filename){
        $image=storage_path()."/app/public/images/departaments/".$filename;
        if(!file_exists($image)){
            $image=storage_path().'/app/public/images/departaments/default.jpg';
        }
        return response()->file($image);
    }
}
