<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use App\Models\Categories;
use App\Models\ProductImages;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoriesController extends Controller
{
    public function select(Request $request){
        $offset=$request->offset?$request->offset : 0;
        $limit=$request->limit?$request->limit : 10;
        $parent=$request->parent?$request->parent : 0;
        $searchText=$request->searchText?$request->searchText:'';
        $categories=[];
        $categories = Categories::where(['active'=>1,'parent_category'=>$parent])->where('name','LIKE','%'.$searchText.'%')->orderBy('name')->limit($limit)->offset($offset)->get();
        
        $title="";
        if($parent>0){
            $parentCategory=Categories::where(['active'=>1,'id'=>$parent])->select('name')->get();
            if($parentCategory->count()){
                $title="Mostrando subcategorías de ".ucfirst($parentCategory[0]->name);
            }
        }
        $count=Categories::where('active',1)->select('id')->count();
        $responseData = [];
        foreach($categories as $c){
            $products=Products::where(['id_category'=>$c->id,'active'=>1])->select('id')->get();
            $subcats=Categories::where(['parent_category'=>$c->id,'active'=>1])->select('id')->get();
            $responseData[]=['c'=>$c,'subcategories_count'=>$subcats->count(),'products_count'=>$products->count()];
        }
        return response()->json(['categories'=>$responseData,'title'=>$title,'count'=>$count]);
    }

    public function list(Request $request){
        $validator=Validator::make($request->all(),[
            'cid'=>'numeric|required'
        ]);

        if($validator->fails()){
            return response()->json(['message'=>"Complete la información solicidata","fields"=>$validator->failed()],400);
        }

        $categories=$this->getMainCategories($request->cid);
        $category=Categories::where(['active'=>1,'id'=>$request->cid]);
        $childs=$this->getChildCategories($request->cid);
        $list=[];
        
        $list[]=['category'=>$category];
        foreach($childs as $ch){
            $ch->children=1;
            $list[]=['category'=>$ch];
        }

        foreach($categories as $c){
            $list[]=['category'=>$c];
        }

        if($request->cid > 0){
            $list[]=['category'=>['name'=>'Categorías principales','id'=>0],'subcategory_count'=>0];
        }

        return response()->json(['categories'=>$list]);

    }

    public function menuList(Request $request){
        $cats=Categories::where(['active'=>1,'parent_category'=>0])->limit(4)->get();
        $categories=[];
        foreach($cats as $c){
            $categories[]=[
                'category'=>$c,
                'subcategories'=>Categories::where(['active'=>1,'parent_category'=>$c->id])->limit(8)->get()
            ];
        }

        return response()->json(['categories'=>$categories]);
    }

    public function selectOne(Request $request){
        $validator=Validator::make($request->all(),[
            'cid'=>'numeric|required'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Parámetros no válidos'],400);
        }
        $category=['id'=>0,'name'=>'','parent_category'=>0];
        if($request->cid > 0){
            $category=Categories::where(['id'=>$request->cid])->select(['id','name','parent_category'])->first();
        }
        $catList=[];
        //$catList[]=['category'=>$category];
        $categories=$this->getMainCategories($request->cid);
        $childs=$this->getChildCategories($request->cid);
        /* foreach($childs as $ch){
            $ch->children=1;
            $catList[]=['category'=>$ch];
        } */
        foreach($categories as $c){
            $catList[]=['category'=>$c];
        }
        return response()->json(['category'=>$category,'categories'=>$catList]);
    }

    private function getMainCategories($cid){
        return Categories::where(['active'=>1,'parent_category'=>0])->where('id','<>',$cid)->select(['id','name'])->orderBy('name','ASC')->get();
    }

    private function getChildCategories($cid){
        return Categories::where(['active'=>1,'parent_category'=>$cid])->select(['id','name'])->get();
    }

    public function insert(Request $request){

        $validator=Validator::make($request->all(),[
            "name"=>"required",
            "pid"=>"required|numeric",
        ]);

        if($validator->fails()){
            return response()->json(["message"=>"Contiene errores",$validator->failed()],400);
        }
        $fields=[
            "name"=>$request->name,
            "parent_category"=>$request->pid
        ];
        $response=Categories::create($fields);
        return response()->json([$response]);
    }

    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            "name"=>"required",
            "pid"=>"required|numeric|min:0",
            "cid"=>"required|numeric|exists:categories,id"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"No se recibió información",$validator->failed()],400);
        }
        $fields=[
            "name"=>$request->name,
            "parent_category"=>$request->pid
        ];      
        $response=Categories::where(['id'=>$request->cid])->update($fields);
        if($response){ 
            $response=Categories::where(['id'=>$request->cid])->first(); 
            return response()->json([$response],200); 
        }
        return response()->json(["message"=>"No se pudo procesar la actualización, intente nuevamente"],400);
    }
    
    public function delete(Request $request){
        $validator=Validator::make($request->all(),[
            "cid"=>"required"
        ]);
        if($validator->fails()){
            return response()->json(["status"=>"400","message"=>"Información recibida no válida",$validator->failed()]);
        }
        $fields=explode(',',$request->cid);
        $response=[];
        $withError=[];
        foreach($fields as $item){
            $childs=Categories::where(['parent_category'=>$item,'active'=>1])->get();
            if(!$childs->count()){
                Categories::where(['id'=>$item])->update(['active'=>0]);
                $response[]=Categories::where(['id'=>$item])->select(['id','name'])->get();
            }
            else{
                $withError[]=Categories::where(['id'=>$item])->select(['id','name'])->get();
            }
        }
        if($response || $withError){ return response()->json(["success"=>$response,"errors"=>$withError]); }
        return response()->json(["message"=>"No se pudo procesar la eliminación, intente nuevamente","fields"=>$fields,400]);
    }
}
