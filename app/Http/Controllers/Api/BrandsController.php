<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use App\Models\Brands;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BrandsController extends Controller
{
    public function list(Request $request){
        $brands=Brands::where(['active'=>1])->orderBy('name')->get();

        return response()->json(['brands'=>$brands]);
    }

    public function select(Request $request){
        $offset=$request->offset?$request->offset : 0;
        $limit=$request->limit?$request->limit : 10;
        $searchText=$request->searchText?$request->searchText:'';

        $brands=Brands::where(['active'=>1])
        ->where('name','LIKE','%'.$searchText.'%')
        ->limit($limit)
        ->offset($offset)
        ->orderBy('name','ASC')
        ->get();
        
        return response()->json(['brands'=>$brands]);
    }

    public function selectOne(Request $request){
        $validator=Validator::make($request->all(),[
            'bid'=>'required|numeric'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Marca no válida'],400);
        }
        $brand=['id'=>0,'name'=>''];
        if($request->bid > 0){
            $brand=Brands::where(['active'=>1,'id'=>$request->bid])->first();
            if(!$brand->count()){
                return response()->json(['message'=>'Marca no encontrada'],400);
            }
        }
        return response()->json(['brand'=>$brand]);
    }

    public function insert(Request $request){
        $validator=Validator::make($request->all(),['name'=>'string|required|unique:brands,name']);
        if($validator->fails()){
            $fields=$validator->failed();
            if(isset($fields['name']['Unique'])){
                return response()->json(['message'=>'El nombre de la marca ya existe'],400);
            }
            return response()->json(['message'=>'No se recibió níngún dato para almacenar'],400);
        }
        $brand=Brands::create(['name'=>$request->name]);
        return response()->json(['brand'=>$brand,'message'=>'Registro agredado']);
    }

    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            'name'=>'string|required|unique:brands,name',
            'bid'=>'numeric|exists:brands,id'
        ]);
        if($validator->fails()){
            $fields=$validator->failed();
            if(isset($fields['bid']['Exists']) || isset($fields['bid']['exists'])){
                return response()->json(['message'=>'No existe la marca'],400);
            }
            if(isset($fields['name']['Unique'])){
                return response()->json(['message'=>'El nombre de la marca ya existe'],400);
            }
            return response()->json(['message'=>'Nombre de marca no válido'],400);
        }
        
        if(!Brands::where(['id'=>$request->bid])->update(['name'=>$request->name])){
            return response()->json(['message'=>'No se pudo actualizar, identificador de marca no existe'],400);
        }
        return response()->json(['message'=>'Registro actualizado']);
    }

    public function delete(Request $request){
        $validator=Validator::make($request->all(),['bid'=>'numeric|exists:brands,id']);
        if($validator->fails()){
            if(isset($fields['bid']['Exists']) || isset($fields['bid']['exists'])){
                return response()->json(['message'=>'No existe la marca'],400);
            }
            return response()->json(['message'=>'Identifidor de marca no válido'],400);
        }
        if(!Brands::where(['id'=>$request->bid])->update(['active'=>0])){
            return response()->json(['message'=>'Identifidor de marca no válido'],400);
        }
        return response()->json(['message'=>'Registro eliminado']);
    }
}
