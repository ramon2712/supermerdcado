<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\MxZipCodes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Utils extends Controller
{
    public function getAddress(Request $request){
        $info=['cp'=>0,'colonia'=>'','municipio'=>'','estado'=>''];
        $validator=Validator::make($request->all(),[
            'cp'=>'numeric|required|exists:mx_zip_codes,cp'
        ]);
        $cp=MxZipCodes::where(['cp'=>$request->cp])->first();
        if($validator->fails() && !$cp){
            return response()->json(['info'=>$info]);
        }
        return response()->json(['info'=>$cp]);
    }

    
}
