<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use App\Models\Administrators;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthApiController extends Controller
{
    
    public function attempt(Request $request){

        $validator=Validator::make($request->all(),[
            'username'=>'required|string',
            'password' => 'required|string',
        ]);
        
        if($validator->fails()){
            return response()->json(['code'=>1,'message'=>'Complete el formulario','errors'=>$validator->errors()->all()],422);
        }

        $user=Administrators::where(['username'=>$request->username,'active'=>1])->first();
        if($user){
            if(Hash::check($request->password,$user->password)){
                $token=$user->createToken('Macep Auth Token')->accessToken;
                
                
                
                return response()->json([
                    'apiKey'=>$token,
                    'user'=>[
                        'id'=>$user->id,
                        'username'=>$user->username,
                        'name'=>$user->name,
                        'lastname'=>$user->lastname,
                        'personal_id'=>$user->personal_id,
                        'idr'=>$user->id_role,
                        'idd'=>$user->id_departament
                    ]
                ]);
            }
            return response()->json(['code'=>3,'message'=>'Su contraseña es incorrecta '],422);
        }
        return response()->json(['code'=>2,'message'=>'No se encontró el usuario'],422);

    }

    public function logout(Request $request){

        $token=Auth::user()->token();
        
        $token->revoke();

        //Auth::logout();
        //$token->revoke();
        return response()->json(["message"=>"Tu sesión ha sido cerreda"]);

    }

    public function getUser(Request $request){
        return response()->json(['user'=>Auth::user()]);
    }
}
