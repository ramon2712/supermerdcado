<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\ViewWarehousesStock;
use App\Models\WarehouseProducts;
use App\Models\Warehouses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WarehousesController extends Controller
{
    
    public function select(Request $request){
        $offset=$request->offset?$request->offset : 0;
        $limit=$request->limit?$request->limit : 10;
        $searchText=$request->searchText?$request->searchText:'';
        $warehouses = Warehouses::where(['active'=>1])->where('name','LIKE','%'.$searchText.'%')->orderBy('name')->limit($limit)->offset($offset)->get();
        return response()->json(['warehouses'=>$warehouses]);
    }

    public function selectOne(Request $request){
        $validator=Validator::make($request->all(),[
            'wid'=>'required|numeric'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Almacén no válido'],400);
        }
        if($request->wid==0){
            $warehouse=['id'=>0,'name'=>'','location'=>'','geolocation'=>''];
            return response()->json(['warehouse'=>$warehouse]);
        }
        $warehouse=Warehouses::where(['active'=>1,'id'=>$request->wid])->first();
        if(!$warehouse->count()){
            return response()->json(['message'=>'Almacén no encontrado'],400);
        }
        return response()->json(['warehouse'=>$warehouse]);
    }

    public function selectWarehousesWithStock(Request $request){
        $validator=Validator::make($request->all(),[
            'pid'=>'required|numeric'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Debe seleccionar un producto'],400);
        }
        $warehouses=ViewWarehousesStock::where(['id_product'=>$request->pid])->get();
        return response()->json(['warehouses'=>$warehouses]);
    }

    public function selectWarehousesWithStockOne(Request $request){
        $validator=Validator::make($request->all(),[
            'pid'=>'required|numeric',
            'wid'=>'required|numeric'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Petición no válida'],400);
        }
        $warehouse=ViewWarehousesStock::where(['id_product'=>$request->pid,'id_warehouse'=>$request->wid])->first();
        return response()->json(['warehouse'=>$warehouse]);
    }

    public function insert(Request $request){
        $validator=Validator::make($request->all(),[
            "name"=>"required|min:5",
            "location"=>"required|min:5"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        $geolocation=$request->geolocation?$request->geolocation:'';
        $fields=[
            "name"=>$request->name,
            "location"=>$request->location,
            "geolocation"=>$geolocation
        ];
        $warehouse=Warehouses::create($fields);

        return response()->json(['message'=>'Almacén guardado','warehouse'=>$warehouse]);
    }

    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            "name"=>"required|min:5",
            "location"=>"required|min:5",
            "wid"=>"numeric|required"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        $geolocation=$request->geolocation?$request->geolocation:'';
        $fields=[
            "name"=>$request->name,
            "location"=>$request->location,
            "geolocation"=>$geolocation
        ];
        if(Warehouses::where(['id'=>$request->wid])->update($fields)){
            return response()->json(['message'=>'Almacén actualizado','warehouse'=>Warehouses::where(['id'=>$request->wid])->get()]);
        }
        return response()->json(["message"=>"No se pudo procesar la actualización, intente nuevamente",400]);
    }

    public function updateStock(Request $request){
        $validator=Validator::make($request->all(),[
            'pid'=>'required|numeric|exists:products,id',
            'wid'=>'required|numeric|exists:warehouses,id',
            'min_stock'=>'required|numeric',
            'max_stock'=>'required|numeric',
            'stock'=>'required|numeric'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Completar formulario','fields'=>$validator->failed()],400);
        }
        if(WarehouseProducts::where(['id_product'=>$request->pid,'id_warehouse'=>$request->wid])->exists()){
            $stock=WarehouseProducts::where(['id_product'=>$request->pid,'id_warehouse'=>$request->wid])->first();
            $stock->min_stock=$request->min_stock;
            $stock->max_stock=$request->max_stock;
            $stock->stock+=$request->stock;
            $stock->save();
            return response()->json(['message'=>'Stock actualizado','stock'=>$stock]);
        }
        $fields=[
            'stock'=>$request->stock,
            'id_product'=>$request->pid,
            'id_warehouse'=>$request->wid
        ];
        $stock=WarehouseProducts::create($fields);
        $stock->min_stock=$request->min_stock;
        $stock->max_stock=$request->max_stock;
        $stock->save();
        return response()->json(['message'=>'Stock ingresado','stock'=>$stock]);
    }

    public function delete(Request $request){
        $validator=Validator::make($request->all(),[
            "wid"=>"numeric|required"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        if(Warehouses::where(['id'=>$request->wid])->update(['active'=>0])){
            return response()->json(["message"=>"Almacén eliminado"]);
        }
        return response()->json(["message"=>"No se pudo procesar la eliminación, intente nuevamente",400]);
    }

}
