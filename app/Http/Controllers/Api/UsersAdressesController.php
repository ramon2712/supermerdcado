<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserAddresses;
use Auth;
use Illuminate\Support\Facades\Validator;

class UsersAdressesController extends Controller
{

    public function storeUA(Request $request) {
        
        if(!Auth::check()){
            return response()->json(['message'=>'Inicie sesion con su cuenta de usuario para realizar esta acción', 'e'=>1]);
        }

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric|exists:users,id',
            'email' => 'required|email',
            'phone' => 'required|digits:10',
            'mx_zip_code_cp' => 'required|numeric|exists:mx_zip_codes,cp',
            'street' => 'required|string',
            'colonia' => 'required|string',
            'full_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'description' => 'required|string'
        ]);

        if($validator->fails()){
            return response()->json(['message'=>'Revise que la información proporcionada sea correcta', 'fields'=>$validator->failed(), 'e'=>1]);
        }
        UserAddresses::create($request->all());
        return response()->json(['message'=>'Se ha guardado su dirección de entrega', 'e'=>0]);

    }

    public function updateUA(Request $request) {

        if(!Auth::check()) {
            return response()->json(['message'=>'Inicie sesion con su cuenta de usuario para realizar esta acción', 'e'=>1]);
        }

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric|exists:users,id',
            'email' => 'required|email',
            'phone' => 'required|digits:10',
            'mx_zip_code_cp' => 'required|numeric|exists:mx_zip_codes,cp',
            'street' => 'required|string',
            'colonia' => 'required|string',
            'full_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'description' => 'required|string'
        ]);

        if($validator->fails()){
            return response()->json(['message'=>'Revise que la información proporcionada sea correcta', 'fields'=>$validator->failed(), 'e'=>1]);
        }

        UserAddresses::where(['id'=>$request->id])->update($request->all());
        return response()->json(['message'=>'Su información de entrega ha sido actualizada', 'e'=>0]);

    }

    public function getUA() {
        
        if(!Auth::check()) {
            return response()->json(['message'=>'Inicie sesion con su cuenta de usuario para realizar esta acción', 'e'=>1]);
        } 

        $user = UserAddresses::where(['user_id'=>Auth::user()->id])->get();

        if(!$user == null) {
            return response()->json(['user'=>$user, 'e'=>0]);
        }

        return response()->json(['message'=>'No hay direcciones de envio registradas', 'e'=>1]);
        
    }

    public function activeMainAddress(Request $request) {

        if(!Auth::check()) {
            return response()->json(['message'=>'Inicie sesion con su cuenta de usuario para realizar esta acción', 'e'=>1]);
        } 

        $validator = Validator::make($request->all(), [
            'main' => 'required|numeric|min:0|max:1'
        ]);

        if($validator->fails()){
            return response()->json(['message'=>'Seleccione una dirección principal para entrega', 'e'=>1]);
        }

        UserAddresses::where(['user_id'=>Auth::user()->id])->update(['main'=>0]);
        UserAddresses::where(['id'=>$request->id])->update(['main'=>$request->main]);

        return response()->json(['message'=>'Se ha actualizado la direccion principal de entrega', 'e'=>0]);

    }

    public function destroyUA(Request $request) {
        if(!Auth::check()) {
            return response()->json(['message'=>'Inicie sesion con su cuenta de usuario para realizar esta acción', 'e'=>1]);
        }
        
        UserAddresses::destroy(['id'=>$request->id]);
        return response()->json(['message'=>'Se elimino la direccion de envio', 'e'=>0]);
    }
    
}
