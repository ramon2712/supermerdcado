<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Web\PaymentSchedule;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $schedule = PaymentSchedule::create($request->all());
        return $schedule;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Web\PaymentSchedule  $paymentSchedule
     * @return \Illuminate\Http\Response
     */
    public function getOrderSchedule(){

    }
    public function show(Request $request)
    {
        $id=$request->id;
        $schedule = PaymentSchedule::where(["order_id"=>$id])->first();
        if($schedule){
        return response()->json(["schedule"=>$schedule, "id"=>"1"]);
        }else{
            return response()->json("0");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Web\PaymentSchedule  $paymentSchedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentSchedule $paymentSchedule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Web\PaymentSchedule  $paymentSchedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentSchedule $paymentSchedule)
    {
        //
    }
}
