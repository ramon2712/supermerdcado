<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\OrderProducts;
use App\Models\Web\Order;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;
use App\Models\ViewOrderProducts;
use App\Models\User;
use Conekta\Conekta;

class oxxopay extends Controller
{
    function __construct(){
        Conekta::setApiKey('key_nUsmpLsws6SPsqW1eRrdhA');
        Conekta::setApiVersion("2.0.0");
    }

    function newOxxoPay (Request $request){
        $products = ViewOrderProducts::where('order_id', $request->order_id)->where('active',1)->get();
        $orderM = Order::find($request->order_id);
        $order_products = [];
        //return response()->json($products);
        foreach($products as $product){
            $order_products[]=["name"=>$product->product_name, "unit_price"=>intval(floatval($product->price)*100), "quantity"=>intval($product->quantity)];//$product->quantity];
        }
        $user = User::find($orderM->user_id);
        try{
            $thirty_days_from_now = (new DateTime())->add(new DateInterval('P1D'))->getTimestamp();           
            $order = \Conekta\Order::create(
              [
                "line_items" => $order_products,
                "shipping_lines" => [
                ], //shipping_lines - physical goods only
                "currency" => "MXN",
                "customer_info" => [
                  "name" => $user->name." ".$user->lastname,
                  "email" => $user->email,
                  "phone" => $user->phone
                ],
                "shipping_contact" => [
                  "address" => [
                    "street1" => "Calle 123, int 2",
                    "postal_code" => "06100",
                    "country" => "MX"
                  ]
                ], //shipping_contact - required only for physical goods
                "charges" => [
                  [
                    "payment_method" => [
                      "type" => "oxxo_cash",
                      "expires_at" => $thirty_days_from_now
                    ]
                  ]
                ]
              ]
            );
            return response()->json($order);
          } catch (\Conekta\ParameterValidationError $error){
            return response()->json($error->getMessage()) ;
          } catch (\Conekta\Handler $error){
            return response()->json($error->getMessage()) ;
          }
    }
}
