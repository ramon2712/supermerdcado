<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use App\Models\ProductDatasheet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductDatasheetsController extends Controller
{
    public function select(Request $request){
        $validator=Validator::make($request->all(),[
            "pid"=>"required|exists:products,id"
        ]);
        $offset=$request->offset?$request->offset : 0;
        $limit=$request->limit?$request->limit : 10;
        $datasheet = ProductDatasheet::where(['active'=>1])->orderBy('name')->limit($limit)->offset($offset)->get();
        return response()->json(['datasheet'=>$datasheet]);
    }

    public function selectOne(Request $request){
        $validator=Validator::make($request->all(),[
            /* 'pid'=>'required|numeric', */
            'pdid'=>'required|numeric|exists:product_datasheets,id'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Petición no válida'],400);
        }
        $datasheet=ProductDatasheet::find($request->pdid);
        return response()->json(['datasheet'=>$datasheet]);
    }

    public function insert(Request $request){
        $validator=Validator::make($request->all(),[
            "name"=>"required|min:1",
            "value"=>"required|min:1",
            "pid"=>"required|exists:products,id"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        $fields=[
            "name"=>$request->name,
            "value"=>$request->value,
            "id_product"=>$request->pid
        ];
        $datasheet=ProductDatasheet::create($fields);

        return response()->json([$datasheet]);
    }
    
    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            "name"=>"required|min:1",
            "value"=>"required|min:1",
            "pid"=>"required|exists:products,id",
            "did"=>"numeric|required"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        $fields=[
            "name"=>$request->name,
            "value"=>$request->value,
            "id_product"=>$request->pid
        ];
        if(ProductDatasheet::where(['id'=>$request->did])->update($fields)){
            return response()->json([ProductDatasheet::where(['id'=>$request->did])->get()]);
        }
        return response()->json(["message"=>"No se pudo procesar la actualización, intente nuevamente",400]);
    }

    public function delete(Request $request){
        $validator=Validator::make($request->all(),[
            "did"=>"numeric|required"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        if(ProductDatasheet::where(['id'=>$request->did])->update(['active'=>0])){
            return response()->json(["message"=>"Producto eliminado"]);
        }
        return response()->json(["message"=>"No se pudo procesar la eliminación, intente nuevamente",400]);
    }
}
