<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use App\Models\SecValues;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SecValuesController extends Controller
{
    public function select(Request $request){
        $validator=Validator::make($request->all(),[
            "sid"=>"required|exists:product_secs,id"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        $offset=$request->offset?$request->offset : 0;
        $limit=$request->limit?$request->limit : 10;
        $values = SecValues::where(['active'=>1,'id_sec'=>$request->sid])->orderBy('name')->limit($limit)->offset($offset)->get();
        return response()->json(['values'=>$values]);
    }

    public function insert(Request $request){
        $validator=Validator::make($request->all(),[
            "name"=>"required|min:1",
            "description"=>"required|min:1",
            "sid"=>"required|exists:product_secs,id"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        $fields=[
            "name"=>$request->name,
            "description"=>$request->description,
            "id_sec"=>$request->$request->sid
        ];
        $sec=SecValues::create($fields);

        return response()->json([$sec]);
    }

    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            "name"=>"required|min:1",
            "description"=>"required|min:1",
            "sid"=>"required|exists:product_secs,id",
            "svid"=>"numeric|required"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        $fields=[
            "name"=>$request->name,
            "description"=>$request->description,
            "id_sec"=>$request->sid
        ];
        if(SecValues::where(['id'=>$request->svid])->update($fields)){
            return response()->json([SecValues::where(['id'=>$request->svid])->get()]);
        }
        return response()->json(["message"=>"No se pudo procesar la actualización, intente nuevamente",400]);
    }

    public function delete(Request $request){
        $validator=Validator::make($request->all(),[
            "svid"=>"numeric|required"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        if(SecValues::where(['id'=>$request->svid])->update(['active'=>0])){
            return response()->json(["message"=>"Producto eliminado"]);
        }
        return response()->json(["message"=>"No se pudo procesar la eliminación, intente nuevamente",400]);
    }
}
