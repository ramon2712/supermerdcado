<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\AdminDepartaments;
use App\Models\Administrators;
use App\Models\AdminRoles;
use App\Models\ViewAdministrators;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdministratorsController extends Controller
{
    public function select(Request $request){
        $offset=$request->offset?$request->offset : 0;
        $limit=$request->limit?$request->limit : 10;
        $searchText=$request->searchText?$request->searchText:'';

        $administrators=ViewAdministrators::where(['active'=>1])
        ->where('searchText','LIKE','%'.$searchText.'%')
        ->orderBy('name')
        ->limit($limit)
        ->offset($offset)
        ->get();
        
        return response()->json(['administrators'=>$administrators]);
    }

    public function selectOne(Request $request){
        $validator=Validator::make($request->all(),[
            'uid'=>'numeric|required'
        ]);

        if($validator->fails()){
            return response()->json(['message'=>'Seleccione un usuario'],400);
        }
        $administrator=[
            'id'=>0,
            'username'=>'',
            'name'=>'',
            'lastname'=>'',
            'personal_id'=>'',
            'id_role'=>0,
            'id_departament'=>0
        ];
        if($request->uid > 0){
            $administrator=ViewAdministrators::where(['active'=>1,'id'=>$request->uid])
            ->select(['id','username','name','lastname','personal_id','id_role','id_departament'])->first();

            if(!$administrator->count()){
                return response()->json(['message'=>'Usuario no encontrado'],400);
            }
        }
        $departaments=AdminDepartaments::where(['active'=>1])->orderBy('name')->get();
        $roles=AdminRoles::where(['active'=>1])->orderBy('name')->get();

        return response()->json(['administrator'=>$administrator,'departaments'=>$departaments,'roles'=>$roles]);
    }

    public function insert(Request $request){
        $validator=Validator::make($request->all(),[
            'username'=>'string|required|min:5|unique:administrators,username',
            'name'=>'string|required|min:2',
            'lastname'=>'string|required|min:2',
            'personal_id'=>'numeric',
            'id_departament'=>'numeric|required|exists:admin_departaments,id',
            'id_role'=>'numeric|required|exists:admin_roles,id',
            'password'=>'required|string|min:6'
        ]);

        if($validator->fails()){
            $failed=$validator->failed();
            if(isset($failed['password'])){
                return response()->json(['message'=>'Ingresa una contraseña de 6 caracteres mínimo'],400);
            }
            return response()->json(['message'=>'Complete todos los campos requeridos'],400);
        }
        $fields=[
            'username'=>$request->username,
            'name'=>$request->name,
            'lastname'=>$request->lastname,
            'personal_id'=>$request->personal_id,
            'id_role'=>$request->id_role,
            'id_departament'=>$request->id_departament,
            'password'=>Hash::make($request->password)
        ];
        $admin = Administrators::create($fields);
        return response()->json(['message'=>'Usuario creado correctamente','values'=>$request->all()]);
    }

    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            'name'=>'string|required|min:2',
            'lastname'=>'string|required|min:2',
            'personal_id'=>'numeric',
            'id_departament'=>'numeric|required|exists:admin_departaments,id',
            'id_role'=>'numeric|required|exists:admin_roles,id',
            'uid'=>'numeric|required|exists:administrators,id'
        ]);

        if($validator->fails()){
            $failed=$validator->failed();
            /* if(isset($failed['password'])){
                return response()->json(['message'=>'Ingresa una contraseña de 6 caracteres mínimo'],400);
            } */
            return response()->json(['message'=>'Complete todos los campos requeridos','fields'=>$validator->failed()],400);
        }
        $fields=[
            'username'=>$request->username,
            'name'=>$request->name,
            'lastname'=>$request->lastname,
            'personal_id'=>$request->personal_id,
            'id_role'=>$request->id_role,
            'id_departament'=>$request->id_departament,
        ];
        if(isset($request->password) && strlen($request->password)>5){
            $fields['password']=Hash::make($request->password);
        }
        if(!Administrators::where(['id'=>$request->uid])->update($fields)){
            return response()->json(['message'=>'Usuario no encontrado'],400);
        }

        return response()->json(['message'=>'Usuario actualizado satisfactoriamente','values'=>$request->all()]);
    }

    public function delete(Request $request){
        $validator=Validator::make($request->all(),[
            'uid'=>'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json(['message'=>'Usuario no válido'],400);
        }
        if($request->uid==1){
            return response()->json(['message'=>'El usuario por defecto de la aplicación no se puede eliminar'],400);
        }
        if($request->uid==Auth::user()->id){
            return response()->json(['message'=>'El usuario de la sesión no se puede eliminar'],400);
        }
        if(!Administrators::where(['id'=>$request->uid])->update(['active'=>0])){
            return response()->json(['message'=>'Usuario no encontrado'],400);
        }
        return response()->json(['message'=>'Usuario eliminado']);
    }

}
