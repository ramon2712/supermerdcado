<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UsersCrudController extends Controller
{

    public function getUserData() {

        if(Auth::check()){
            $id = Auth::user()->id;
            $userData = User::find($id);
            return response()->json(['user'=>$userData, 'e'=>0]);
        }

        return response()->json(['message'=>'Inicie sesion con su cuenta de usuario para realizar esta acción','e'=>1]);
    }

    public function updateUserData(Request $request) {

        if(Auth::check()){
            $validator=Validator::make($request->all(),[
                'name'=>'required|regex:/^[\pL\s\-]+$/u',
                'lastname'=>'required|regex:/^[\pL\s\-]+$/u',
                'email' => 'required|email',
                'phone'=>'digits:10|required',
                'balance'=>'numeric|required',
            ]);
            
            if($validator->fails()){
                return response()->json(['message'=>'Fallo el registro, revise que la información proporcionada sea correcta','fields'=>$validator->failed(),'e'=>1]);
            }
    
            $fields=[
                'name'=>$request->name,
                'lastname'=>$request->lastname,
                'phone'=>$request->phone,
                'balance'=>$request->balance,
                'email'=>$request->email
            ];
    
            User::where([Auth::user()->id])->update($fields);
            return response()->json(['message'=>'Sus cambios han sido guardados','e'=>0]);
        }
        return response()->json(['message'=>'Inicie sesion con su cuenta de usuario para realizar esta acción','e'=>1]);
    }

    public function updateUserPass(Request $request) {

        if(Auth::check()){

            $validator=Validator::make($request->all(),[
                'password' => 'required|confirmed',
            ]);

            if($validator->fails()){
                return response()->json(['message'=>'Las contraseñas no coinciden','fields'=>$validator->failed(),'e'=>1]);
            }

            $fields=[
                'password'=>hash::make($request->password),
            ];

            if (User::where([Auth::user()->id])->update($fields)){
                return response()->json(['message'=>'Su nueva contraseña ha sido guardada', 'e'=>0]);
            }

            return response()->json(['message'=>'Si entro', 'e'=>0]);
        }

        return response()->json(['message'=>'Inicie sesion con su cuenta de usuario para realizar esta acción','e'=>1]);

    }

    public function updateUserStatus(Request $request) {

        $validator=Validator::make($request->all(),[
            'id_user_status' => 'numeric|required|exists:user_status,id',
        ]);
    
        if($validator->fails()){
            return response()->json(['message'=>'Selecciona un estatus válido para el usuario','fields'=>$validator->failed(),'e'=>1]);
        }

        $fields=[
            'id_user_status'=>$request->id_user_status
        ];

        if(User::where(['id'=>$request->id])->update($fields)){
            return response()->json(['message'=>'Se ha cambiado el estatus del usuario','e'=>0]);
        }
    
    }
}
