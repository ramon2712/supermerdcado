<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use App\Models\Freighters;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FreightersController extends Controller
{
    public function select(Request $request){
        $offset=$request->offset?$request->offset : 0;
        $limit=$request->limit?$request->limit : 10;
        $searchText=$request->searchText?$request->searchText:'';

        $freighters=Freighters::where(['active'=>1])
        ->where('name','LIKE','%'.$searchText.'%')
        ->orderBy('main','DESC')
        ->limit($limit)
        ->offset($offset)
        ->get();

        return response()->json(['freighters'=>$freighters]);
    }

    public function selectOne(Request $request){
        $validator=Validator::make($request->all(),[
            'fid'=>'required|numeric'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Petición inválida'],400);
        }
        $freighter=['id'=>0,'name'=>'','address'=>'','phone'=>''];
        if($request->fid==0){
            return response()->json(['freighter'=>$freighter]);
        }
        $freighter=Freighters::where(['active'=>1,'id'=>$request->fid])->first();
        if(!$freighter->count()){
            return response()->json(['message'=>'Fletera no encontrado'],400);
        }
        return response()->json(['freighter'=>$freighter]);
    }

    public function insert(Request $request){
        
        
        $validator=Validator::make($request->all(),[
            "name"=>"required|string",
            "address"=>"required|min:5"
        ]);
        
        if($validator->fails()){
            return response()->json(["message"=>"Formulario incompleto",$validator->failed()],400);
        }
        
        $phone=$request->phone?$request->phone:'';
        //return response($phone);
        $fields=[
            "name"=>$request->name,
            "address"=>$request->address,
            "phone"=>$phone
        ];

        //return response($fields);
        //return response()->json(["message"=>"Entra en insert 4"]);
        $freighters=Freighters::create($fields);

        return response()->json(['message'=>'Fletera agregada correctamente','freighters'=>$freighters]);
    }

    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            "name"=>"required|string",
            "address"=>"required|min:5",
            "fid"=>"required"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        $phone=$request->phone?$request->phone:'';
        $fields=[
            "name"=>$request->name,
            "address"=>$request->address,
            "phone"=>$phone
        ];
        if(Freighters::where(['id'=>$request->fid])->update($fields)){
            return response()->json(['message'=>'Datos actualizados',Freighters::where(['id'=>$request->fid])->get()]);
        }
        return response()->json(["message"=>"Ningún campo para actualizar"],400);
    }

    public function delete(Request $request){
        
        $validator=Validator::make($request->all(),[
            "fid"=>"required"
        ]);
        //return response($request);
        
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        
        if(!Freighters::where(['id'=>$request->fid])->update(['active'=>0])){
            return response()->json(["message"=>"No se pudo procesar la eliminación, intente nuevamente"],400);
        }
        return response()->json(["message"=>"Fletera eliminada"]);
    }


}
