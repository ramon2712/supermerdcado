<?php

namespace App\Http\Controllers\Api;

use App\Events\Dashboard;
use App\Http\Controllers\Controller;
use App\Models\AdminActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Env;
use Illuminate\Support\Facades\Auth;

class ActivityController extends Controller
{
    public function setActivity(Request $request){
        $user=Auth::user();

        $ipdata = json_decode(file_get_contents("http://timezoneapi.io/api/ip/?" . $request->ip . Env::get('TIMEZONE_TOKEN')));
        $city='not_found';
        $country='not_found';
        if($ipdata['meta']['code']=='200'){
            $city=$ipdata['data']['city'].', '.$ipdata['data']['state'];
            $country=$ipdata['data']['country'];
        }
        $activity=AdminActivity::create([
            'id_aministrator'=>$user->id,
            'component'=>$request->component,
            'admin_ip'=>$request->ip,
            'city'=>$city,
            'country'=>$country
        ]);

        $date = date("Y-m-d H:i:s");
        $time = strtotime($date);
        $time = $time - (15 * 60);
        $date = date("Y-m-d H:i:s", $time);

        $activeAdmins=AdminActivity::where(['created_at','=>',$date])->count();

        broadcast(new Dashboard(['type'=>'ADMIN_ACTIVITY','activeAdmins'=>$activeAdmins]))->toOthers();

    }
}
