<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CartItem;
use Illuminate\Http\Request;
use App\Http\Resources\CartItem as CartResources;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\CartItem as CartRequest;
use App\Http\Resources\CartItemViewColllection;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    protected $cartItem;

    public function  __construct(CartItem $cartItem)
    {
        //$this->middleware('auth');
        $this->cartItem = $cartItem;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = DB::select('select cart_products.status, cart_products.id, cart_products.id_user, cart_products.id_product, 
        cart_products.quantity, cart_products.created_at, products.name, products.description, product_images.id as id_image,product_images.filename as filename, 
        product_prices.sale_price from (((cart_products 
        INNER JOIN products ON cart_products.id_product = products.id and cart_products.id_user = :id_user and cart_products.status= 0) 
        INNER JOIN product_images ON product_images.id_product = products.id and product_images.main = 1) 
        INNER JOIN product_prices on product_prices.id_product = products.id)', 
        ['id_user' =>Auth::user()->id]); 
        return response()->json(
                $items
        );
         //response()->json($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $item = CartItem::where('id_product', $request->id_product)->where('id_user', $request->id_user)->where('status', 0)->first();
       
        if($item){
            $cartitem = CartItem::find($item->id);
            $item_request = [
                'id_product'=>$request->id_product,
                'id_user' => $request->id_user,
                'status' => $request->status,
                'quantity' => $request ->quantity+$cartitem->quantity
            ];
            $cartitem = CartItem::find($item->id);
            $cartitem ->update($item_request);
            //$item->update($request->all());
            //$this.update($request, $request->id);
            return response()->json($cartitem, 201);
        }else{
            $cartItem = $this->cartItem->create($request->all());
            return response()->json($cartItem, 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CartItem  $cartItem
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $items= DB::select('select cart_products.status, cart_products.id, cart_products.id_user, cart_products.id_product, 
        cart_products.quantity, cart_products.created_at, products.name, products.description, product_images.id as id_image,product_images.filename as filename, 
        product_prices.sale_price from (((cart_products 
        INNER JOIN products ON cart_products.id_product = products.id and cart_products.id_user = :id_user and cart_products.status= 0) 
        INNER JOIN product_images ON product_images.id_product = products.id and product_images.main = 1 and product_images.active = 1) 
        INNER JOIN product_prices on product_prices.id_product = products.id)', 
        ['id_user' =>$id]); 
        return response()->json(
                $items
        );
        /*$cartItem = CartItem::find($id);
        if($cartItem){
            return response()->json ($cartItem);
        }else{
            return response()->json ();
        }*/
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CartItem  $cartItem
     * @return \Illuminate\Http\Response
     */
    /*public function update(Request $request, CartItem $cartItem)
    {
        $cartItem->update($request->all());
        return response()->json(new CartResources($cartItem));
    }*/

    public function update(Request $request, $id)
    {          
        $cartItem=CartItem::find($id);
        $cartItem->update($request->all());
        return $cartItem;
        //return response()->json(['request'=>$request]);//new CartResources($cartItem));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CartItem  $cartItem
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {  
        $cartItem = CartItem::find($id);
        $cartItem->delete();
        return response() ->json(null, 204);
    }

}
