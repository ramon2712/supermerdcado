<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\ViewDeliverTipeStock;
use App\Models\PaymentPartialTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PaymentPartialTypesController extends Controller
{
    
    public function select(Request $request){
        $offset=$request->offset?$request->offset : 0;
        $limit=$request->limit?$request->limit : 10;
        $searchText=$request->searchText?$request->searchText:'';   
        $paymentPartialTypes = PaymentPartialTypes::where(['active'=>1])->where('name','LIKE','%'.$searchText.'%')->orderBy('name')->limit($limit)->offset($offset)->get();
        return response()->json(['paymentPartialTypes'=>$paymentPartialTypes]);
    }

    public function selectOne(Request $request){
        $validator=Validator::make($request->all(),[
            'pptid'=>'required|numeric'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Tipo de pago parcial no válido xD xD'],400);
        }
        if($request->pptid==0){
            $paymentPartialType=['id'=>0,'name'=>''];
            return response()->json(['paymentPartialType'=>$paymentPartialType]);
        }
        $paymentPartialType=PaymentPartialTypes::where(['active'=>1,'id'=>$request->pptid])->first();
        if(!$paymentPartialType->count()){
            return response()->json(['message'=>'Tipo de pago parcial no encontrado'],400);
        }
        return response()->json(['paymentPartialType'=>$paymentPartialType]);
    }

    public function insert(Request $request){
        $validator=Validator::make($request->all(),[
            "name"=>"required|min:1",
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        $fields=[
            "name"=>$request->name,
        ];
        $paymentPartialType=PaymentPartialTypes::create($fields);

        return response()->json(['message'=>'Tipo de pago parcial guardado','paymentPartialType'=>$paymentPartialType]);
    }

    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            "name"=>"required|min:1",
            "pptid"=>"numeric|required"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        $fields=[
            "name"=>$request->name,
        ];
        if(PaymentPartialTypes::where(['id'=>$request->pptid])->update($fields)){
            return response()->json(['message'=>'Tipo de pago parcial actualizado','paymentPartialType'=>PaymentPartialTypes::where(['id'=>$request->pptid])->get()]);
        }
        return response()->json(["message"=>"No se pudo procesar la actualización, intente nuevamente",400]);
    }

    public function delete(Request $request){
        $validator=Validator::make($request->all(),[
            "pptid"=>"numeric|required"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        if(PaymentPartialTypes::where(['id'=>$request->pptid])->update(['active'=>0])){
            return response()->json(["message"=>"Tipo de pago parcial eliminado"]);
        }
        return response()->json(["message"=>"No se pudo procesar la eliminación, intente nuevamente",400]);
    }

}
