<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use App\Models\Payments;
use App\Models\paymentTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PaymentsController extends Controller
{
    public function select(Request $request){
        //return response()->json(["message"=>"Entra en select"]);
        $offset=$request->offset?$request->offset : 0;
        $limit=$request->limit?$request->limit : 10;
        $searchText=$request->searchText?$request->searchText:'';
        $payments = Payments::where(['active'=>1])->where('name','LIKE','%'.$searchText.'%')->orderBy('name')->limit($limit)->offset($offset)->get();
        //return response()->json(['warehouses'=>$warehouses,'status'=>200]);
        return response()->json(['payments'=>$payments]);
    }

    public function selectOne(Request $request){
        $validator=Validator::make($request->all(),[
            'pmid'=>'numeric|required'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Método de págo no válido'],400);
        }
        $payment=['id'=>0,'name'=>'','description'=>''];
        if($request->pmid==0){
            return response()->json(['payment'=>$payment]);
        }
        $payment=Payments::where(['active'=>1,'id'=>$request->pmid])->first();
        if(!$payment->count()){
            return response()->json(['message'=>'Método de pago no encontrado'],400);
        }
        return response()->json(['payment'=>$payment]);
    }

    public function listPaymentTypes(Request $request){
        $list=paymentTypes::where(['active'=>1])->orderBy('id')->get();
        return response()->json(['paymentTypes'=>$list]);
    }

    public function insert(Request $request){
        
        $validator=Validator::make($request->all(),[
            "name"=>"required|string",
        ]);
        
        if($validator->fails()){
            return response()->json(["message"=>"Contiene errores",$validator->failed()],400);
        }
        //return response($request->all());
        $fields=[
            "name"=>$request->name,
            "description"=>$request->description,
        ];

        //return response($fields);
        //return response()->json(["message"=>"Entra en insert 4"]);
        $payments=Payments::create($fields);

        return response()->json(['message'=>'Información guardada correctamente',$payments]);
    }

    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            "name"=>"required|string",
            "description"=>"required|string",
            "pmid"=>"required"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        $fields=[
            "name"=>$request->name,
            "description"=>$request->description,
        ];
        if(Payments::where(['id'=>$request->pmid])->update($fields)){
            return response()->json(['message'=>'Registro actualizado',Payments::where(['id'=>$request->pmid])->first()]);
        }
        return response()->json(["message"=>"Nada que actualizar"]);
    }

    public function delete(Request $request){
        
        $validator=Validator::make($request->all(),[
            "pmid"=>"required"
        ]);
        //return response($request);
        
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        
        $fields=explode(',',$request->pmid);
        foreach($fields as $item){
            //return response()->json(["message"=>"entra en foreach"]);
            Payments::where(['id'=>$item])->update(['active'=>0]);
        }
        
        if(Payments::where(['id'=>$request->pmid])->update(['active'=>0])){
            return response()->json(["message"=>"Registro eliminado"]);
        }
        return response()->json(["message"=>"No se pudo procesar la eliminación, intente nuevamente",400]);
    }

}
