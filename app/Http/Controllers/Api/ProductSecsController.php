<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use App\Models\ProductSecs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductSecsController extends Controller
{
    public function select(Request $request){
        $offset=$request->offset?$request->offset : 0;
        $limit=$request->limit?$request->limit : 10;
        $pid=$request->pid?$request->pid:0;
        $secs = ProductSecs::where(['active'=>1,'id_product'=>$pid])->orderBy('name')->limit($limit)->offset($offset)->get();
        return response()->json(['secs'=>$secs]);
    }

    public function selectOne(Request $request){
        $validator=Validator::make($request->all(),[
            /* 'pid'=>'required|numeric', */
            'psid'=>'required|numeric|exists:product_secs,id'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Petición no válida'],400);
        }
        $secs=ProductSecs::find($request->psid);
        return response()->json(['secs'=>$secs]);
    }
    
    public function insert(Request $request){
        $validator=Validator::make($request->all(),[
            "name"=>"required|min:1",
            "description"=>"required|min:1",
            "pid"=>"required|exists:product,id"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        $fields=[
            "name"=>$request->name,
            "description"=>$request->description,
            "id_product"=>$request->pid
        ];
        $secs=ProductSecs::create($fields);

        return response()->json([$secs]);
    }

    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            "name"=>"required|min:1",
            "description"=>"required|min:1",
            "pid"=>"required|exists:products,id",
            "sid"=>"numeric|required"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        $fields=[
            "name"=>$request->name,
            "description"=>$request->description,
            "id_product"=>$request->cid
        ];
        if(ProductSecs::where(['id'=>$request->sid])->update($fields)){
            return response()->json([ProductSecs::where(['id'=>$request->sid])->get()]);
        }
        return response()->json(["message"=>"No se pudo procesar la actualización, intente nuevamente",400]);
    }

    public function delete(Request $request){
        $validator=Validator::make($request->all(),[
            "sid"=>"numeric|required"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        if(ProductSecs::where(['id'=>$request->sid])->update(['active'=>0])){
            return response()->json(["message"=>"Producto eliminado"]);
        }
        return response()->json(["message"=>"No se pudo procesar la eliminación, intente nuevamente",400]);
    }
}
