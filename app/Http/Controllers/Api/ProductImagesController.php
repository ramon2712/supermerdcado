<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use App\Models\ProductImages;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Str;

class ProductImagesController extends Controller
{
    public function selectForProduct(Request $request){
        $validator=Validator::make($request->all(),[
            "pid"=>"required|exists:products,id"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Especifique que imagen requiere","fields"=>$validator->failed()],400);
        }
        $images = ProductImages::where(['active'=>1,'id_product'=>$request->pid])->orderBy('main','DESC')->get();
        return response()->json(['images'=>$images]);
    }

    public function select(Request $request,$folder="",$filename=""){
        $image=storage_path().'/app/public/images/'.$folder.'/'.$filename;
        if(!file_exists($image)){
            $image=storage_path().'/app/public/images/thumbnails/default.jpg';
        }
        return response()->file($image);
    }

    public function selectAll(Request $request){
        $offset=$request->offset?$request->offset : 0;
        $limit=$request->limit?$request->limit : 10;
        $images = ProductImages::where(['active'=>1])->groupBy('id_product')->orderBy('main','DESC')->limit($limit)->offset($offset)->get();
        return response()->json(['images'=>$images]);
    }

    public function upload(Request $request){
        $validator=Validator::make($request->all(),[
            "pid"=>"required|exists:products,id"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"No se identificó el producto","e_code"=>"file_product_not_found","fields"=>$validator->failed()],400);
        }

        if(!$request->hasFile('product_image')){
            return response()->json(["message"=>"Debe subir una imagen","e_code"=>"file_not_found"],400);
        }

        $position=$request->position?$request->position:0;

        $ext=strtolower($request->file('product_image')->extension());
        if(!in_array($ext,['jpg','png','webp'])){
            return response()->json(["message"=>"Debe subir una imagen valida","e_code"=>"file_invalid"],400);
        }

        

        //AQUI EMPIEZA LOGICA PARA VALIDAR, ENCODEAR A WEBP Y HACER RESIZE A LA IMAGEN
        $sizeValidate = Validator::make($request->file(), [
            "product_image"=>"dimensions:min_width=1024,min:height=1024",
        ]);
        
        $nameProd = Str::slug(Product::where('id', $request->pid)->value('name'));
        $filename = $nameProd.Carbon::now()->format('-sihdmy');

        if($sizeValidate->fails()){
            return response()->json(['message'=>'Solo se aceptan imágenes de minimo 1024x1024px', 'e'=>1]);
        }

        $file = $request->file('product_image');
        
        $original = Image::make($file)->encode('webp');

        Storage::disk('public')->put("images/1024x1024/".$filename, $original);
        Storage::disk('public')->put("images/512x512/".$filename, $original->resize(512, 512)->save($filename));
        Storage::disk('public')->put("images/thumbnails/".$filename, $original->resize(128, 128)->save($filename));

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    
        $main=ProductImages::where(['id_product'=>$request->pid,'main'=>1])->get();
        $fields=[
            // "filename"=>$filename[count($filename)-1],
            "filename"=>$filename,
            "main"=>$position,
            "id_product"=>$request->pid,
            "main"=>($main->count())?0:1
        ];
        $image=ProductImages::create($fields);

        return response()->json(['image'=>$image]);
    }

    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            "position"=>"required|numeric",
            "fid"=>"numeric|required",
            "pid"=>"numeric|required"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Especificar imagen a editar","fields"=>$validator->failed()],400);
        }
        $filename=ProductImages::where(['id'=>$request->fid])->select('filename')->get();
        if($filename){
            if($request->hasFile('product_image')){
                $file = $request->file('product_image');
                $filename=Storage::putFile("product_".$request->pid , $request->file('product_image'));
            }
            if($request->position > 0){
                ProductImages::where(['id_product'=>$request->pid])->update(['main'=>0]);
            }
            $fields=[
                "filename"=>$filename,
                "main"=>$request->position
            ];
            if(ProductImages::where(['id'=>$request->pid])->update($fields)){
                return response()->json([ProductImages::where(['id'=>$request->fid])->get()]);
            }
            return response()->json(["message"=>"No se pudo procesar la actualización, intente nuevamente",400]);
        }
        return response()->json(["message"=>"No se pudo procesar la actualización, intente nuevamente",400]);
    }

    public function delete(Request $request){
        $validator=Validator::make($request->all(),[
            "iid"=>"numeric|required"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        if(ProductImages::where(['id'=>$request->iid])->update(['active'=>0])){
            $image=ProductImages::where(['id'=>$request->iid])->select(['main','id_product'])->get();
            if($image->count()>0 && $image[0]->main===1){
                $main=ProductImages::where(['active'=>1,'id_product'=>$image[0]->id_product])->first();
                if($main){
                    ProductImages::where(['id'=>$main->id])->update(['main'=>1]);
                }
            }
            return response()->json(["message"=>"Imagen eliminada"]);
        }
        return response()->json(["message"=>"No se pudo procesar la eliminación, intente nuevamente",400]);
    }
}
