<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Web\Colonias;
use Illuminate\Http\Request;

class ColoniasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Colonias  $colonias
     * @return \Illuminate\Http\Response
     */
    public function show($cp)
    {
        $colonias = Colonias::where('cp', $cp)->get();
        return response()->json($colonias);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Colonias  $colonias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Colonias $colonias)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Colonias  $colonias
     * @return \Illuminate\Http\Response
     */
    public function destroy(Colonias $colonias)
    {
        //
    }
}
