<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use App\Models\ProductPrices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductPricesController extends Controller
{
    
    public function selectOne(Request $request){
        $validator=Validator::make($request->all(),[
            'pid'=>'numeric|required',
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Producto no encontrado'],400);
        }
        $item=ProductPrices::where(['id_product'=>$request->pid])->first();
        if(!$item){
            return response()->json([
            'message'=>'Producto no encontrado',
            'prices'=>[
                'sale_price'=>0,
                'purchase_price'=>0,
                'profit_margin'=>0,
                'id_product'=>$request->pid
            ]]); 
        }
        return response()->json(['message'=>'Precios','prices'=>$item]);
    }

    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            'pid'=>'numeric|required',
            'sale_price'=>'numeric|required',
            'purchase_price'=>'numeric',
            'profit_margin'
        ]);

        if($validator->fails()){
            return response()->json(['message'=>'Completar formulario'],400);
        }
        $fields=[
            "sale_price"=>$request->sale_price,
            "purchase_price"=>$request->purchase_price,
            "profit_margin"=>$request->profit_margin
        ];
        $item=ProductPrices::where(['id_product'=>$request->pid])->first();
        $prices=[];
        if($item){
            if(ProductPrices::where(['id_product'=>$request->pid])->update($fields)){
            $prices=ProductPrices::where(['id_product'=>$request->pid])->first();
            }
        }
        else{
            $fields['id_product']=$request->pid;
            $prices=ProductPrices::create($fields);
        }
        return response()->json(["message"=>"Información almacenada",'prices'=>$prices]);
    }

}
