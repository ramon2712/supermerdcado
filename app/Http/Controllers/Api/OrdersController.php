<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Orders;
use App\Models\ProductPrices;
use App\Models\Web\Order;
use Illuminate\Http\Request;
use App\Models\Web\CartItem;
use App\Models\Web\order_product;
use App\Models\Web\Product;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{
    protected $order;
    protected $product;
    public function  __construct(Orders $order,order_product $product)
    {
        $this->order = $order;
        $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
        return response()->json('Hola');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = $this->order->create($request->all());
        $order_products = CartItem::where('id_user', $request->user_id)->where('status', 0)->get();
        if($order_products){
            foreach($order_products as $product){
                $price = ProductPrices::where('id_product', $product->id_product)->first();
                $price_item = 0;
                if($price){
                    $price_item = $price->sale_price;
                }
                $order_product = ['product_id'=>$product->id_product, 'order_id'=>$order->id, 'quantity'=>$product->quantity, 'price'=>$price_item, 'amount'=>($product->quantity*$price_item), 'ship_quantity'=>$product->quantity];
                $this->product->create($order_product);
                $cartitem = CartItem::find($product->id);
                $updatecart = ['id'=>$cartitem->id, 'id_product'=>$cartitem->id_product, 'id_user'=>$cartitem->id_user, 'quantity'=>$cartitem->quantity, 'status'=>1];
                $cartitem->update($updatecart);
            }  
            
            return response()->json($order_product);
        }
        else{
            return response()->json("error");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Web\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Orders::find($id);
        if($order){
            if($order->id_user == Auth::id()){
                return response()->json ($order);
            }else{
                return response()->json('No encontrado');
            }
        }else{
            return response()->json ("no encontrado");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Web\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order=Orders::find($id);
        $order->update($request->all());
        return $order;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Web\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Orders $order)
    {
        $order = Orders::find($order->id);
        $order->id_order_state = 9;
        $order->save();
        return response() ->json(null, 204);
    }
}
