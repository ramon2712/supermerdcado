<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\Departaments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DepartamentsController extends Controller
{
    public function select(Request $request){
        $offset=$request->offset?$request->offset : 0;
        $limit=$request->limit?$request->limit : 10;
        $searchText=$request->searchText?$request->searchText:'';

        $departaments=Departaments::where(['active'=>1])
        ->where('name','LIKE','%'.$searchText.'%')
        ->limit($limit)
        ->offset($offset)
        ->orderBy('name','ASC')
        ->get();
        
        return response()->json(['departaments'=>$departaments]);
    }

    public function selectOne(Request $request){
        $validator=Validator::make($request->all(),[
            'did'=>'required|numeric'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Departamento no válido'],400);
        }
        $departament=['id'=>0,'name'=>'','description'=>''];
        if($request->did > 0){
            $departament=Departaments::where(['active'=>1,'id'=>$request->did])->first();
            if(!$departament->count()){
                return response()->json(['message'=>'Departamento no encontrado'],400);
            }
        }
        return response()->json(['departament'=>$departament]);
    }

    public function insert(Request $request){
        $validator=Validator::make($request->all(),[
            'name'=> 'string|required|unique:admin_departaments,name',
            'description'=> 'string|required'
        ]);
        if($validator->fails()){
            $fields=$validator->failed();
            if(isset($fields['name']['Unique'])){
                return response()->json(['message'=>'El nombre del departamento ya existe'],400);
            }
            return response()->json(['message'=>'No se recibió níngún dato para almacenar'],400);
        }
        $departament=Departaments::create(['name'=>$request->name,'description'=>$request->description]);
        return response()->json(['departament'=>$departament,'message'=>'Registro agredado']);
    }

    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            'name'=>'string|required',
            'description'=> 'string|required',
            'did'=>'numeric|exists:admin_departaments,id'
        ]);
        if($validator->fails()){
            $fields=$validator->failed();
            if(isset($fields['did']['Exists']) || isset($fields['did']['exists'])){
                return response()->json(['message'=>'No existe el departamento'],400);
            }
            return response()->json(['message'=>'Nombre de departamento no válido'],400);
        }
        
        if(!Departaments::where(['id'=>$request->did])->update(['name'=>$request->name,'description'=>$request->description])){
            return response()->json(['message'=>'Sin información para actualizar'],400);
        }
        return response()->json(['message'=>'Registro actualizado']);
    }

    public function delete(Request $request){
        $validator=Validator::make($request->all(),['did'=>'numeric|exists:brands,id']);
        if($validator->fails()){
            if(isset($fields['bid']['Exists']) || isset($fields['bid']['exists'])){
                return response()->json(['message'=>'No existe el departamento'],400);
            }
            return response()->json(['message'=>'Identifidor de departamento no válido'],400);
        }
        if(!Departaments::where(['id'=>$request->did])->update(['active'=>0])){
            return response()->json(['message'=>'Identifidor de departamento no válido'],400);
        }
        return response()->json(['message'=>'Registro eliminado']);
    }

}
