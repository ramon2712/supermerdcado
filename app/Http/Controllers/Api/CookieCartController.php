<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use App\Models\Product;
use App\Models\ProductImages;
class CookieCartController extends Controller
{
    public function setCartCookie(Request $request)
    {
        $minutes=43200;
        $response = New Response('Cookies almacenadas');
        $cart = $request -> cookie('cart');
        if($cart != null){
            $data = json_decode($cart);
            $arraydata = (array)$data;
            $array=[];
            $check=false;
            for($i=0;$i<count($arraydata);++$i){
                if($arraydata[$i]->id == $request->id){
                    $quantity = $request->quantity + $arraydata[$i]->quantity;
                    $addCart = ['id'=>$arraydata[$i]->id, 'quantity'=>$quantity];
                    $arraydata[$i]=$addCart;
                    $check =true;
                    //$arraydata[]=$addCart;
                    //return response()->json($array);//['id'=>$item->id, 'quantity'=>$quantity]);
                }
            }
            if(!$check){
                $addCart = ['id'=>$request->id, 'quantity'=>$request->quantity];
                $arraydata[]=$addCart;
            }/*
            foreach($arraydata as $item){
                if($item->id == $request->id){
                    $quantity = $request->quantity + $item->quantity;
                    $addCart = ['id'=>$item->id, 'quantity'=>$quantity];
                    $array[]=$addCart;
                    //$arraydata[]=$addCart;
                    //return response()->json($array);//['id'=>$item->id, 'quantity'=>$quantity]);
                }else{
                    $addCart = ['id'=>$request->id, 'quantity'=>$request->quantity];
                    $array[]=$addCart;
                    return response()->json($array);
                }
            }*/
            //$addCart = ['id'=>$request->id, 'quantity'=>$request->quantity];
            //$arraydata[]=$addCart;
            $data_json = json_encode($arraydata);
            $response -> withCookie(cookie('cart',$data_json, $minutes));
            return $response;
        }else{
            $data=[];
            $data [] = ['id'=>$request->id, 'quantity'=>$request->quantity];
            $data_json = json_encode($data);
            $response -> withCookie(cookie('cart',$data_json, $minutes));
            return $response;
        }        
    }  

    public function getCartCookie(Request $request)
    {
        $cart = $request -> cookie('cart');
        $data = json_decode($cart);        
        $array = (array)$data;
        if($array){
            $dataCart = [];
            foreach ($array as $item) {
                $product = Product::find($item->id);
                $image = ProductImages::where(['main'=>1, 'active'=>1, 'id_product'=>$item->id])->first();
                $filename="none";
                if($image){
                    $filename = $image->filename;
                }
                $dataCart[] = ['id'=>$item->id, 'quantity'=>$item->quantity, 'name'=>$product->name, 'filename'=>$filename];
            }
            return response() -> json($dataCart);
        }
        return response()->json(false);
    }

    public function editCartCookie(Request $request, $id){
        $cart = $request -> cookie('cart');
        $data = json_decode($cart);
        $array = (array)$data;
        $dataCart = [];
        $salto=true;
        foreach($array as $item){
            if($item->id == $id){
                $salto = false;
                //$quantity = $item->quantity + $request->quantity;
                $dataCart[] = ['id'=>$item->id, 'quantity'=>$request->quantity];
            }else{
                $dataCart[] = $item;
            }
        }
        $minutes=43200;
        $response = New Response('Cookie de cart modificada');
        $data_json = json_encode($dataCart);
        $response -> withCookie(cookie('cart',$data_json, $minutes));
        return ($response);
    }

    public function deleteCookieItem(Request $request, $id){
        $cart = $request -> cookie('cart');
        $data = json_decode($cart);
        $array = (array)$data;
        $dataCart = [];
        foreach($array as $item){
            if($item->id != $id){
                //$quantity = $item->quantity + $request->quantity;
                $dataCart[] = $item;//['id'=>$item->id, 'quantity'=>$request->quantity];
            }
        }
        $minutes=43200;
        $response = New Response('Cookie de cart modificada');
        $data_json = json_encode($dataCart);
        $response -> withCookie(cookie('cart',$data_json, $minutes));
        return ($response);
    }
}
