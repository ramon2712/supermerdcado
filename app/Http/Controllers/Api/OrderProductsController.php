<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Web\order_product;
use App\Models\Web\Product;
use Illuminate\Http\Request;
use App\Models\ProductImages;
use App\Models\Prices;
use App\Models\ProductPrices;

class OrderProductsController extends Controller
{
    protected $product;

    public function __construct(order_product $product){
        $this->product = $product;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $orderP = $this->product->create($request->all());
        return response()->json($orderP);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Web\order_product  $order_product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products = order_product::where('order_id', $id)->where('active', 1)->get();
        if($products){
            $order_products = [];
            foreach($products as $item){
                $prod = Product::find($item->product_id);
                $name = $prod->name;
                if(strlen($name)>40){
                    $prod_name = substr($prod->name,0,37)."...";
                }else{
                    $prod_name = $prod->name;
                }
                $price = ProductPrices::where('id_product', $item->product_id)->first();
                $price_item = 0;
                if($price){
                    $price_item = $price->sale_price;
                }
                $image = ProductImages::where(['main'=>1, 'active'=>1, 'id_product'=>$item->product_id])->first();
                $filename = "";
                if($image){
                    $filename = $image->filename;
                }else{
                    $filename = "none";
                }
                $product = ['id'=>$item->id, 'id_product'=> $item->product_id, 'name'=>$prod_name, 'quantity'=>$item->quantity, 'filename'=>$filename, 'sale_price'=>$price_item];
                $order_products[] = $product;
            }            
            return response()->json($order_products);
        }else{
            return response()->json('Vacio');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Web\order_product  $order_product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product=order_product::find($id);
        $product->update($request->all());
        return $product;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Web\order_product  $order_product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=order_product::find($id);
        $product->active=0;
        $product->save();
    
        return response() ->json(null, 204);
    }
}
