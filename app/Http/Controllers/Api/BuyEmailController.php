<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class BuyEmailController extends Controller
{
    function sendBuyMail($data){
        Mail::send('emails.checkout_buy', $data, function($message) use ($data) {
            //$message->to($data['email'], $data['name'])->subject('Por favor confirma tu correo');
        });
    }
}
