<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\ViewDeliverTipeStock;
use App\Models\DeliveryTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DeliveryTimesController extends Controller
{
    //

    // public function list(Request $request){
    //     $list=deliveryTimes::where(['active'=>1])->orderBy('id')->get();
    //     return response()->json(['deliveryTimes'=>$list]);
    // }

    public function select(Request $request){
        $offset=$request->offset?$request->offset : 0;
        $limit=$request->limit?$request->limit : 10;
        $searchText=$request->searchText?$request->searchText:'';   
        $deliveryTime = DeliveryTime::where(['active'=>1])->where('time','LIKE','%'.$searchText.'%')->orderBy('time')->limit($limit)->offset($offset)->get();
        return response()->json(['deliveryTime'=>$deliveryTime]);
    }

    public function selectOne(Request $request){
        $validator=Validator::make($request->all(),[
            'dtid'=>'required|numeric'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Tiempo de entrega no válido xD xD'],400);
        }
        if($request->dtid==0){
            $deliveryTim=['id'=>0,'time'=>'','location'=>'','geolocation'=>''];
            return response()->json(['deliveryTim'=>$deliveryTim]);
        }
        $deliveryTim=DeliveryTime::where(['active'=>1,'id'=>$request->dtid])->first();
        if(!$deliveryTim->count()){
            return response()->json(['message'=>'Tiempo de entrega no encontrado'],400);
        }
        return response()->json(['deliveryTim'=>$deliveryTim]);
    }

    public function insert(Request $request){
        $validator=Validator::make($request->all(),[
            "time"=>"required|min:1",
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        $fields=[
            "time"=>$request->time,
        ];
        $deliveryTim=DeliveryTime::create($fields);

        return response()->json(['message'=>'Tiempo de entrega guardado','deliveryTim'=>$deliveryTim]);
    }

    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            "time"=>"required|min:1",
            "dtid"=>"numeric|required"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        $fields=[
            "time"=>$request->time,
        ];
        if(DeliveryTime::where(['id'=>$request->dtid])->update($fields)){
            return response()->json(['message'=>'Tiempo de entrega actualizado','deliveryTim'=>DeliveryTime::where(['id'=>$request->dtid])->get()]);
        }
        return response()->json(["message"=>"No se pudo procesar la actualización, intente nuevamente"],400);
    }

    public function delete(Request $request){
        $validator=Validator::make($request->all(),[
            "dtid"=>"numeric|required"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        if(DeliveryTime::where(['id'=>$request->dtid])->update(['active'=>0])){
            return response()->json(["message"=>"Tiempo de entrega eliminado"]);
        }
        return response()->json(["message"=>"No se pudo procesar la eliminación, intente nuevamente",400]);
    }

}
