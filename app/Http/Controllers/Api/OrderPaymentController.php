<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\OrderPayments;
use Illuminate\Http\Request;

class OrderPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payment = OrderPayments::create($request->all());
        return response()->json($payment);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderPayments  $orderPayments
     * @return \Illuminate\Http\Response
     */
    public function show(OrderPayments $orderPayments)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OrderPayments  $orderPayments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderPayments $orderPayments)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderPayments  $orderPayments
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderPayments $orderPayments)
    {
        //
    }
}
