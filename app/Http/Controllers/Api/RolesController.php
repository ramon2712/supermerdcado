<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use App\Models\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{
    
    public function list(Request $request){
        
        $roles=Roles::where('active',1)->select(['id','name'])->get();
        return response()->json(['roles'=>$roles]);
    }

    public function select(Request $request){
        $searchText=$request->searchText?$request->searchText:'';
        $offset=$request->offset?$request->offset:0;
        $limit=$request->limit?$request->limit:12;

        $roles=Roles::where(['active'=>1])->where('name','LIKE','%'.$searchText.'%')->get();

        return response()->json(['roles'=>$roles]);

    }

    public function selectOne(Request $request){
        $validator=Validator::make($request->all(),[
            'rid'=>'required|numeric'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Petición inválida'],400);
        }
        $role=['id'=>0,'name'=>'','description'=>''];
        if($request->rid>0){
            $role=Roles::where(['active'=>1,'id'=>$request->rid])->first();
        }
        return response()->json(['role'=>$role]);
    }

    public function insert(Request $request){
        $validator=Validator::make($request->all(),[
            'name'=>'string|required',
            'description'=>'string|required'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Datos incompletos'],400);
        }
        $fields=['name'=>$request->name,'description'=>$request->description];
        $role=Roles::create($fields);
        return response()->json(['role'=>$role,'message'=>'Información guardada']);
    }

    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            'name'=>'string|required',
            'description'=>'string|required',
            'rid'=>'required|numeric'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Datos incompletos'],400);
        }
        $fields=['name'=>$request->name,'description'=>$request->description];
        $role=Roles::where(['id'=>$request->rid])->update($fields);
        if(!$role){
            return response()->json(['message'=>'No se econtró el rol'],400);
        }
        return response()->json(['message'=>'Información guardara']);
    }

    public function delete(Request $request){
        $validator=Validator::make($request->all(),[
            'rid'=>'numeric|required'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Datos incompletos'],400);
        }
        if(!Roles::where(['id'=>$request->rid])->update(['active'=>0])){
            return response()->json(['message'=>'No se encontró el rol'],400);
        }
        return response()->json(['message'=>'Información guardara']);
    }

}
