<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use App\Models\Brands;
use App\Models\Categories;
use App\Models\ProductDatasheet;
use App\Models\ProductImages;
use App\Models\ProductPrices;
use App\Models\Products;
use App\Models\ProductSecs;
use App\Models\SecValues;
use App\Models\ViewProducts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    public function select(Request $request){
        $offset=$request->offset?$request->offset : 0;
        $limit=$request->limit?$request->limit : 10;
        $searchText=$request->searchText?$request->searchText:'';
        $products=[];
        
        $products=ViewProducts::where(['active'=>1])->where('name','LIKE',"%$searchText%")->orderBy('name')->limit($limit)->offset($offset)->get();
        
        $values=[];
        foreach($products as $prod){
            $image=ProductImages::where(['id_product'=>$prod->id,'main'=>1,'active'=>1])->limit(1)->select('filename')->get();
            $values[]=['product'=>$prod,'image'=>$image->count()?'api/files/images/thumbnails/'.$image[0]->filename:'api/files/images/thumbnails/default.jpg'];
        }
        return response()->json(['products'=>$values]);
    }

    public function selectOne(Request $request){
        $validator=Validator::make($request->all(),[
            'pid'=>'numeric|required'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>"No existe información"],400);
        }
        $product=['id'=>0,'name'=>'','description'=>'','sku'=>'','upc'=>'','id_brand'=>0,'id_category'=>0,'max_stock_global'=>0,'min_stock_global'=>0];
        if(!$request->pid==0){
            $product=ViewProducts::where(['active'=>1,'id'=>$request->pid])->first();
        }
        /* $images=ProductImages::where(['id_product'=>$request->pid,'active'=>1])->select(['id','filename','main'])->orderBy('main','DESC')->get();
        $datasheet=ProductDatasheet::where(['id_product'=>$request->pid,'active'=>1])->select(['id','name','value'])->get();
        $prices=ProductPrices::where(['id_product'=>$request->pid])->first();
        $secs=[];
        $sc=ProductSecs::where(['active'=>1,'id_product'=>$request->pid])->get();
        foreach($sc as $s){
            $sec_values=SecValues::where(['active'=>1,'id_sec'=>$s->id])->get();
            $secs[]=['sec'=>$s,'values'=>$sec_values];
        } */
        $brands=Brands::where(['active'=>1])->orderBy('name','ASC')->get();
        $categories=Categories::where(['active'=>1])->orderBy('name','ASC')->get();

        return response()->json([
            'product'=>$product,
            'brands'=>$brands,
            'categories'=>$categories
        ]);
    }

    public function selectOneWithCode(Request $request){
        $validator=Validator::make($request->all(),[
            'code'=>'required|string'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Producto no encontrado'],400);
        }
        $product=ViewProducts::where(['active'=>1])->where(function($query) use ($request){
            $query->where(['sku'=>$request->code])->orWhere(['upc'=>$request->code]);
        })->first();
        if(!$product){
            return response()->json(['message'=>'Producto no encontrado'],400);
        }
        if($product->stock <= 0){
            return response()->json(['message'=>'Producto sin existencias'],400);
        }
            return response()->json(['product'=>$product]);
    }

    public function insert(Request $request){
        $validator=Validator::make($request->all(),[
            "name"=>"required|min:1",
            "description"=>"required|min:1",
            "cid"=>"required|exists:categories,id",
            "bid"=>"required|exists:brands,id"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        $sku=$request->sku?$request->sku:'';
        $upc=$request->upc?$request->upc:'';
        $fields=[
            "name"=>$request->name,
            "description"=>nl2br(str_replace('<','',$request->description)),
            "id_category"=>$request->cid,
            "sku"=>$sku,
            "upc"=>$upc,
            "id_brand"=>$request->bid
        ];
        $product=Products::create($fields);

        return response()->json(['message'=>'Producto guardado','product'=>$product]);
    }

    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            "name"=>"required|min:1",
            "description"=>"required|min:1",
            "cid"=>"required|exists:categories,id",
            "bid"=>"required|exists:brands,id",
            "pid"=>"numeric|required"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        $sku=$request->sku?$request->sku:'';
        $upc=$request->upc?$request->upc:'';
        $fields=[
            "name"=>$request->name,
            "description"=>$request->description,
            "id_category"=>$request->cid,
            "id_brand"=>$request->bid,
            "sku"=>$sku,
            "upc"=>$upc
        ];
        if(Products::where(['id'=>$request->pid])->update($fields)){
            return response()->json(['message'=>'Producto actualizado','product'=>Products::where(['id'=>$request->pid])->first()]);
        }
        return response()->json(["message"=>"No se pudo procesar la actualización, intente nuevamente",400]);
    }

    public function delete(Request $request){
        $validator=Validator::make($request->all(),[
            "pid"=>"numeric|required"
        ]);
        if($validator->fails()){
            return response()->json(["message"=>"Completar formulario","fields"=>$validator->failed()],400);
        }
        if(Products::where(['id'=>$request->pid])->update(['active'=>0])){
            return response()->json(["message"=>"Producto eliminado"]);
        }
        return response()->json(["message"=>"No se pudo procesar la eliminación, intente nuevamente",400]);
    }
}
