<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\UserAddresses;
use App\Models\VwUserAddresses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    
    public function selectUserAddresses(Request $request){
        $validator=Validator::make($request->all(),[
            'uid'=>'required|numeric|exists:users,id'
        ]);
        $results=VwUserAddresses::where(['active'=>1,'user_id'=>$request->uid])->get();
        if($validator->fails() || !$results->count()){
            return response()->json(['addresses'=>[]]);
        }
        return response()->json(['addresses'=>$results]);
    }

    public function insertAddress(Request $request){
        $validator=Validator::make($request->all(),[
            'uid'=>'numeric|required',
            'email'=>'email|required',
            'street'=>'string|required',
            'colonia'=>'string|required',
            'cp'=>'required|numeric|exists:mx_zip_codes,cp',
            'name'=>'string|required',
            'description'=>'string',
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Formulario incompleto','fields'=>$validator->failed()],400);
        }
        $fields=[
            'user_id'=>$request->uid,
            'email'=>$request->email,
            'phone'=>$request->phone?$request->phone:'',
            'street'=>$request->street,
            'colonia'=>$request->colonia,
            'mx_zip_code_cp'=>$request->cp,
            'full_name'=>$request->name,
            'description'=>$request->description
        ];
        $result=UserAddresses::create($fields);
        return response()->json(['message'=>'Dirección agregada','address'=>$result]);
    }

    public function updateAddress(Request $request){
        $validator=Validator::make($request->all(),[
            'id'=>'numeric|required|exists:user_addresses,id',
            'uid'=>'numeric|required',
            'email'=>'email|required',
            'street'=>'string|required',
            'colonia'=>'string|required',
            'cp'=>'required|numeric|exists:mx_zip_codes,cp',
            'name'=>'string|required',
            'description'=>'string',
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Formulario incompleto','fields'=>$validator->failed()],400);
        }
        $fields=[
            'email'=>$request->email,
            'phone'=>$request->phone?$request->phone:'',
            'street'=>$request->street,
            'colonia'=>$request->colonia,
            'mx_zip_code_cp'=>$request->cp,
            'full_name'=>$request->name,
            'description'=>$request->description
        ];
        if(!UserAddresses::where(['id'=>$request->id])->update($fields)){
            return response()->json(['message'=>'Dirección no encontrada'],400);
        }
        $result=VwUserAddresses::find($request->id);
        return response()->json(['message'=>'Dirección actualizada','address'=>$result]);
    }


}
