<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use App\Models\AdminMenus;
use App\Models\AdminRoleMenuPermissions;
use App\Models\AdminRoleSubmenuPermissions;
use App\Models\AdminSubmenus;
use App\Models\ViewMenuPermissions;
use App\Models\ViewSubmenuPermissions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PermissionsController extends Controller
{
    public function list(Request $request){
        $validator=Validator::make($request->all(),[
            'rid'=>'numeric|required'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'No es un rol válido, verifica e intenta nuevamente'],400);
        }
        $menuList=[];
        $menu=AdminMenus::where(['active'=>1])->get();
        foreach($menu as $m){
            $submenus=AdminSubmenus::where(['active'=>1,'id_admin_menu'=>$m->id])->get();
            $menuList[]=['menu'=>$m,'submenus'=>$submenus];
        }

        $authMenus=ViewMenuPermissions::where(['id_role'=>$request->rid])->select('id_menu')->get();
        $authArrMenus=[];
        foreach($authMenus as $am){
            $authArrMenus[]=$am->id_menu;
        }

        $authSubmenus=ViewSubmenuPermissions::where(['id_role'=>$request->rid])->select('id_submenu')->get();
        $authArrSubmenus=[];
        foreach($authSubmenus as $as){
            $authArrSubmenus[]=$as->id_submenu;
        }

        return response()->json(['list'=>$menuList,'permissions'=>['menus'=>$authArrMenus,'submenus'=>$authArrSubmenus]]);
    }

    public function set(Request $request){
        $validator=Validator::make($request->all(),[
            'rid'=>'required|numeric'
        ]);
        if($validator->fails() || $request->rid<=0){
            return response()->json(['message'=>'Selecciona un rol para modificar'],400);
        }
        if($request->rid==1){
            return response()->json(['message'=>'El superusuario no se puede modificar, intenta con otro'],400);
        }
        if($request->mids){
            foreach($request->mids as $item){
                if($item['value']){
                    if(!AdminRoleMenuPermissions::where(['id_menu'=>$item['mid'],'id_role'=>$request->rid])->exists()){
                        AdminRoleMenuPermissions::create(['id_menu'=>$item['mid'],'id_role'=>$request->rid]);
                    }
                }
                else{
                    AdminRoleMenuPermissions::where(['id_menu'=>$item['mid'],'id_role'=>$request->rid])->delete();
                }
            }
        }
        if($request->sids){
            foreach($request->sids as $item){
                if($item['value']){
                    if(!AdminRoleSubmenuPermissions::where(['id_submenu'=>$item['mid'],'id_role'=>$request->rid])->exists()){
                        AdminRoleSubmenuPermissions::create(['id_submenu'=>$item['mid'],'id_role'=>$request->rid]);
                    }
                }
                else{
                    AdminRoleSubmenuPermissions::where(['id_submenu'=>$item['mid'],'id_role'=>$request->rid])->delete();
                }
            }
        }
        return response()->json(['message'=>'Actualización de datos correcta']);
        
    }
}
