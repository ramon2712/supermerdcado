<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\AdminMenus;
use App\Models\AdminRoleMenuPermissions;
use App\Models\AdminRoleSubmenuPermissions;
use App\Models\AdminSubmenus;
use App\Utils\Routes;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Auth;

class AppController extends Controller
{
    public function initApplication(Request $request){
        $routes=new Routes();

        return response()->json(['menus'=>$this->menus(),'routes'=>$routes->allRoutes(),'routerDom'=>$this->routerDom()]);
    }

    public function selectMenus(Request $request){
        return response()->json(['menus'=>$this->menus()]);
    }

    private function menus(){
        $menu_permissions=AdminRoleMenuPermissions::where(['id_role'=>Auth::user()->id_role])->select('id_menu')->get();
        $menus=AdminMenus::where('active',1)->whereIn('id',$menu_permissions)->orderBy('position','ASC')->get();
        $response=[];
        foreach($menus as $m){
            $submenu_permissions=AdminRoleSubmenuPermissions::where(['id_role'=>Auth::user()->id_role])->select('id_submenu')->get();
            $submenus=AdminSubmenus::where(['active'=>1,'id_admin_menu'=>$m->id])->whereIn('id',$submenu_permissions)->orderBy('position','ASC')->get();
            $response[]=['menu'=>$m,'submenus'=>$submenus];
        }

        return $response;
    }

    private function routerDom(){
        $routes=AdminSubmenus::where(['active'=>1])->select(['id','href','component'])->orderBy('position','ASC')->get();
        return $routes;
    }
}
