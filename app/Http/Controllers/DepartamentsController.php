<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Departaments;
use App\Models\ViewProducts;

class DepartamentsController extends Controller
{
    //
    public function listDepartaments(Request $request){
        $cid=$request->cid;
        $departaments=Departaments::all();
        return response ()->json($departaments);
    }
}
