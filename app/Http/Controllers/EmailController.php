<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\AddressShippings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\MessageRecieve;
use App\Mail\ScheduleOxxoPay;
use App\Mail\ScheduleSpei;


class EmailController extends Controller 
{
    public function sendEmailContact(Request $request){
        $emailData=$request->all();
        Mail::to('ramon.moreno@macep.mx')->send(new MessageRecieve($emailData));
        return response()->json(['products'=>$emailData]);
    }

    public function sendRecieptEmail (Request $request){
        $emailData=$request->all();
        Mail::to('ramon.moreno@macep.mx')->send(new ScheduleOxxoPay($emailData));
        return response()->json(['products'=>$emailData]);
    }

    public function sendRecieptEmailSpei (Request $request){
        $emailData=$request->all();
        Mail::to('ramon.moreno@macep.mx')->send(new ScheduleSpei($emailData));
        return response()->json(['products'=>$emailData]);
    }
}
