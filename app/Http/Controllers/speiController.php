<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Conekta\Conekta;
use App\Models\Web\order_product as orderProduct;
use App\Models\ViewOrderProducts;
use App\Models\User;
use App\Models\Web\Order;
use DateInterval;
use DateTime;

class speiController extends Controller
{
    //
    function __construct(){
        Conekta::setApiKey('key_nUsmpLsws6SPsqW1eRrdhA');
        Conekta::setApiVersion("2.0.0");
      }

      public function createCustomer(Request $request){      
        $products = ViewOrderProducts::where('order_id', $request->order_id)->where('active',1)->get();
        $orderM = Order::find($request->order_id);
        $order_products = [];
        //return response()->json($products);
        foreach($products as $product){
          $order_products[]=["name"=>strval($product->product_name), "unit_price"=>intval(floatval($product->price)*100), "quantity"=>intval($product->quantity),"sku"=>strval($product->sku)];//$product->quantity];
        }
        $user = User::find($orderM->user_id);
        try{
            $thirty_days_from_now = (new DateTime())->add(new DateInterval('P30D'))->getTimestamp(); 
          
            $order = \Conekta\Order::create(
              [
                "line_items" => $order_products,
                "shipping_lines" => [
                  [
                    "amount" => 1500,
                    "carrier" => "FEDEX"
                  ]
                ], //shipping_lines - physical goods only
                "currency" => "MXN",
                "customer_info" => [
                    "name" => $user->name." ".$user->lastname,
                    "email" => $user->email,
                    "phone" => $user->phone
                ],
                "shipping_contact" => [
                  "address" => [
                    "street1" => "Calle 123, int 2",
                    "postal_code" => "06100",
                    "country" => "MX"
                  ]
                ], //shipping_contact - required only for physical goods
                "charges" => [
                  [
                    "payment_method" => [
                      "type" => "spei",
                      "expires_at" => $thirty_days_from_now
                    ]
                  ]
                ]
              ]
            );
          } catch (\Conekta\ParameterValidationError $error){
            echo $error->getMessage();
          } catch (\Conekta\Handler $error){
            echo $error->getMessage();
          }
          return response()->json([$order]);
      }  
}
