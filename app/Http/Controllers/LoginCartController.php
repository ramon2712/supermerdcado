<?php

namespace App\Http\Controllers;

use App\Models\CartItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class LoginCartController extends Controller
{
    protected $cartItem;

    public function  __construct(CartItem $cartItem)
    {
        //$this->middleware('auth');
        $this->cartItem = $cartItem;
    }

    public function addCartCookie(Request $request)
    {
        $cart = $request->cookie('cart');
        $data = json_decode($cart);
        $array = (array)$data;
        $resp = [];
        if ($array) {
            $dataCart = [];
            foreach ($array as $item) {
                $itemC = CartItem::where('id_product', $item->id)->where('id_user', $request->id_user)->where('status', 0)->first();
                if ($itemC) {
                    $cartitem = CartItem::find($item->id);
                    $item_request = [
                        'id_product' => $item->id,
                        'id_user' => $request->id_user,
                        'status' => 0,
                        'quantity' => $item->quantity + $cartitem->quantity
                    ];
                    $cartitem = CartItem::find($item->id);
                    $resp[] = $item_request;
                    $cartitem->update($item_request);
                    //$item->update($request->all());
                    //$this.update($request, $request->id);
                    //return response()->json($cartitem, 201);
                } else {
                    $addItem = [
                        'id_product' => $item->id,
                        'id_user' => $request->id_user,
                        'status' => 0,
                        'quantity' => $item->quantity
                    ];
                    $resp[] = $addItem;
                    $cartItem = $this->cartItem->create($addItem);
                    //return response()->json($cartItem, 201);
                }
            }
            Cookie::queue(Cookie::forget('cart'));
            return response()->json(['agregados'=>$resp]);
        }
    }
}
