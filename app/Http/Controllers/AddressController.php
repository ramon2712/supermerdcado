<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\AddressShippings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $address = AddressShippings::where('user_id', Auth::Id())->get();
        return $address;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'full_name'=> 'required',
            'email'=> 'required',
            'phone'=> 'required',
            'description'=> 'required',
            'street'=> 'required',
            'mx_zip_code_cp'=> 'required',
            'municipio'=> 'required',
            'colonia'=> 'required',
        ]);
       AddressShippings::create($request->all());
        return;
        //return view('user.partials.shipping', 'Address_Shipping');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $address = AddressShippings::where('user_id', $id)->get();
        return $address;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $address = AddressShippings::findOrFail($id);
        return $address;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $address = AddressShippings::findOrFail($id);
        $address->delete();
    }
}
