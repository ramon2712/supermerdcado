<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ViewOrderPaymentSchedules;
use App\Models\Web\payment;
use Illuminate\Database\Eloquent\Collection;

class PaymentSchedulesController extends Controller
{
    public function payment(Request $request){
        //echo $request->oid;
        $oid = $request->oid;
        $payments = ViewOrderPaymentSchedules::where("order_id",$oid)->select("order_id","payment_type_name")->get();
        return response()->json($payments);
    }
}