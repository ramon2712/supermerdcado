<?php

namespace App\Http\Controllers;

use App\Models\OrderDeletedReceipts;
use App\Models\OrderPaymentSchedules;
use App\Models\OrderProducts;
use App\Models\Orders;
use App\Models\OrderTypes;
use App\Models\OrderPayments;
use App\Models\Products;
use App\Models\ViewOrderProducts;
use App\Models\ViewOrders;
use App\Models\ViewPaymentsOrderLog;
use App\Models\ViewWarehouseProducts;
use App\Models\ViewWarehousesStock;
use App\Models\VwOrderPayments;
use App\Models\VwOrderPaymentSchedules;
use App\Models\Web\PaymentSchedule;
use App\Models\Warehouses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Arr;

class OrdersController extends Controller
{
    public function select(Request $request){
        $offset=$request->offset?$request->offset : 0;
        $limit=$request->limit?$request->limit : 10;
        $searchText=$request->searchText?$request->searchText:'';
        $section=$request->section?$request->section:'';
        $orders=[];
        $orders=ViewOrders::where('searchText','LIKE','%'.$searchText.'%');
        switch($section){
            case 'SHIPMENTS':
                $orders=$orders->where('order_status_id','>',2);
                break;
            default:
            $orders=$orders->limit($limit)->offset($offset)->orderBy('created_at','DESC');
        }
        $orders=$orders->get();
        return response()->json(['orders'=>$orders,'prev'=>false,'next'=>false,'offset'=>$offset,'limit'=>$limit,'searchText'=>$searchText]);
    }

    public function selectOne(Request $request){
        $validator=Validator::make($request->all(),[
            'oid'=>'required|exists:orders,id'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Orden de compra no encontrada'],400);
        }
        $order=ViewOrders::where(['id'=>$request->oid])->first();
        return response()->json(['order'=>$order]);
    }

    public function list(Request $request){
        $validator=Validator::make($request->all(),[
            'oid'=>'required|exists:orders,id'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Orden de compra no encontrada'],400);
        }
        $order=Orders::find($request->oid);
        $list=ViewOrderProducts::where(['order_id'=>$request->oid])->get();
        //$shipping=ViewOrderShippings::where(['id_order'=>$request->oid])->get();
        $client=ViewOrders::where(['id'=>$request->oid])->select(['user_id','full_name','email','phone'])->first();
        return response()->json(['productList'=>$list,'order'=>$order, /* 'shipping'=>$shipping, */'client'=>$client]);
    }

    public function listOrdersProducts(Request $request){
        $userId=$request->oid;
        //$userId=$request->oid?$request->oid:20;
        $orders=ViewOrders::where("user_id",$userId)->orderBy('created_at',"desc")->get();
        //Saber si la orden se encuentra en la tabla
        $dataCart=[];
        foreach($orders as $order){
            //$products[]=array_push($order);
            //$dataCart=$order;
            $find=PaymentSchedule::where(["order_id"=>$order->id])->first();
            if($find){

                //$orders = Arr::add(['find' => 'encontrado']);
                $dataCart[]=["administrator_id"=>$order->administrator_id,
                            "amount"=>$order->amount,
                            "created_at"=>$order->created_at,
                            "id"=>$order->id,
                            "name_payment_status"=>$order->name_payment_status,
                            "name_status"=>$order->name_status,
                            "net_taxes"=>$order->net_taxes,
                            "order_status_id"=>$order->order_status_id,
                            "order_types_id"=>$order->order_types_id,
                            "payment_status_id"=>$order->payment_status_id,
                            "products_count"=>$order->products_count,
                            "subtotal"=>$order->subtotal,
                            "taxes"=>$order->taxes,
                            "updated_at"=>$order->updated_at,
                            "user_address_id"=>$order->user_address_id,
                            "products_count"=>$order->products_count,
                            "user_id"=>$order->user_id,
                            "paymentSchedule"=>1    ];
                //$dataCart[]=$find;
                //$orders=Arr::add($orders,'find','1');
            }else{
                $dataCart[]=["administrator_id"=>$order->administrator_id,
                            "amount"=>$order->amount,
                            "created_at"=>$order->created_at,
                            "id"=>$order->id,
                            "name_payment_status"=>$order->name_payment_status,
                            "name_status"=>$order->name_status,
                            "net_taxes"=>$order->net_taxes,
                            "order_status_id"=>$order->order_status_id,
                            "order_types_id"=>$order->order_types_id,
                            "payment_status_id"=>$order->payment_status_id,
                            "products_count"=>$order->products_count,
                            "subtotal"=>$order->subtotal,
                            "taxes"=>$order->taxes,
                            "updated_at"=>$order->updated_at,
                            "user_address_id"=>$order->user_address_id,
                            "products_count"=>$order->products_count,
                            "user_id"=>$order->user_id,
                            "paymentSchedule"=>0    ];
            }
        }
        return response()->json(["orders"=>$orders, "id_user"=>$userId,"products"=>$dataCart]);
    }

    public function listViewOrdersProducts(Request $request){
        $userId=$request->oid;
        //$userId=$request->oid?$request->oid:20;
        $orders=ViewOrderProducts::where("order_id",$userId)->get();
        return response()->json(["orders"=>$orders, "id_user"=>$userId]);
    }

    public function productWithStock(Request $request){
        /* CREATE VALIDATION HERE 
        --
        --
        --
        */
        $productList=ViewOrderProducts::where(['order_id'=>$request->oid])->get();
        $list=[];
        foreach($productList as $p){
            $warehouses=ViewWarehousesStock::where(['id_product'=>$p->product_id])->orderBy('stock','DESC')->get();
            $list[]=['product'=>$p,'warehouses'=>$warehouses];
        }

        return response()->json(['list'=>$list]);
    }

    public function listOrderTypes(Request $request){
        return response()->json(['orderTypes'=>OrderTypes::where('id','>','1')->where(['active'=>1])->get()]);
    }

    public function selectInventoryList(Request $request){
        $validator=Validator::make($request->all(),[
            'oid'=>'required|exists:orders,id'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Orden de compra no encontrada'],400);
        }
        $list=ViewOrderProducts::where(['id_order'=>$request->oid])->get();
        $listWithAvailability=[];
        foreach($list as $l){
            $available=ViewWarehousesStock::where(['id_product'=>$l->id_product,'id_warehouse'=>$l->id_warehouse])->where('stock','>',$l->quantity)->first();
            $listWithAvailability[]=['product'=>$l,'available'=>$available];
        }
        return response()->json(['products'=>$listWithAvailability]);
    }

    /* SELECT ORDER PAYMENT */
    public function selectOrderPayment(Request $request){
        $validator=Validator::make($request->all(),[
            'oid'=>'required|exists:orders,id'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Orden de compra no encontrada'],400);
        }
        $payments=ViewPaymentsOrderLog::where(['id_order'=>$request->oid])
        ->get();


        return response()->json(['payments'=>$payments]);
    }
    
    public function selectPaymentSchedules(Request $request){
        $validator=Validator::make($request->all(),[
            'oid'=>'required|exists:orders,id'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Orden de compra no encontrada'],400);
        }
        $payments=VwOrderPaymentSchedules::where(['order_id'=>$request->oid])->get();


        return response()->json(['payments'=>$payments]);
    }

    public function selectPaymentHistory(Request $request){
        $validator=Validator::make($request->all(),[
            'osid'=>'required|exists:vw_order_payment_schedules,id'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Orden de compra no encontrada'],400);
        }
        $payments=VwOrderPayments::where(['order_payment_schedules_id'=>$request->osid])->get();


        return response()->json(['payments'=>$payments]);
    }

    public function insert(Request $request){
        $validator=Validator::make($request->all(),[
            'productList'=>'required',
            'paymentMethod'=>'required|numeric|exists:payment_types,id'
        ]);
        if($validator->fails()){
            if(isset($validator->failed()['uid']['\\App\\Rules\\State'])){
                return response()->json(['message'=>'Usuario no está activo'],419);
            }
            return response()->json(['message'=>'Petición no válida'],400);
        }
        if(!is_array($request->productList)){
            return response()->json(['message'=>'No se encontraron productos añadidos, no se crea la orden']);
        }
        $net_taxes=($request->subtotal*$request->iva);
        $amount=$request->subtotal+$net_taxes;
        $orderFields=[
            'user_id'=>$request->uid,
            'id_administrator'=>$request->aid,
            'order_delivery_periods_id'=>$request->delivery,
            'order_types_id'=>$request->orderType,
            'amount'=>$amount,
            'subtotal'=>$request->subtotal,
            'taxes'=>$request->iva,
            'net_taxes'=>$net_taxes,
            'products_count'=>count($request->productList),
            'user_address_id'=>$request->address_id,
            'administrator_id'=>Auth::user()->id
        ];
        DB::beginTransaction();
        $products=[];
        try{
            $order=Orders::create($orderFields);
            foreach($request->productList as $p){
                $products[]=[
                    'product_id'=>$p['id'],
                    'order_id'=>$order->id,
                    'quantity'=>$p['quantity'],
                    'price'=>$p['price'],
                    'amount'=>$p['price']*$p['quantity'],
                    'ship_quantity'=>$p['quantity']
                ];
            }
            OrderProducts::insert($products);
            if($request->orderType===1 || $request->orderType===2){
                OrderPaymentSchedules::create([
                    'order_id'=>$order->id,
                    'date'=>$request->deliveryDate,
                    'amount'=>$amount,
                    'subtotal'=>$request->subtotal,
                    'taxes'=>$net_taxes,
                    'payment_types_id'=>$request->paymentMethod,
                    'payment_status_id'=>1
                ]);
            }
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(['message'=>'No se pudo ejecutar la acción','e'=>$e,'products'=>$products],400);
        }
            DB::commit();
        return response()->json(['fields'=>$products]);
    }

    public function updateAuthorize(Request $request){
        $validator=Validator::make($request->all(),[
            'pmid'=>'exists:order_payments,id|numeric|required'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Número de orden no válido'],400);
        }
        $orderLog=OrderPayments::find($request->pmid);
        $orderLog->payment_status_id=4;
        $orderLog->administrator_id=Auth::user()->id;
        $orderLog->save();

        $orderLogCount=OrderPayments::where([
            'active'=>1,
            'order_payment_schedules_id'=>$orderLog->order_payment_schedules_id,
            'payment_status_id'=>2])->count();
        $orderSchedule=OrderPaymentSchedules::find($orderLog->order_payment_schedules_id);
        if(!$orderLogCount){
            $orderSchedule=OrderPaymentSchedules::find($orderLog->order_payment_schedules_id);
            if($orderLog->paid_amount >= $orderSchedule->amount){
                $orderSchedule->payment_status_id=4;
                $orderSchedule->save();
                Orders::where(['id'=>$orderSchedule->order_id])->update(['order_status_id'=>3]);
                return response()->json(['message'=>'El pago ha sido aceptado, el cliente ha sido notifcado vía email']); 
            }
            $orderSchedule->payment_status_id=3;
            $orderSchedule->save();
            return response()->json(['message'=>'El pago ha sido aceptado, pero es incompleto, notifique al cliente']); 
        }
        //Orders::where(['id'=>$orderSchedule->orden_id])->update(['order_status_id'=>3]);
        /* Enviar email al cliente aquí */

        return response()->json(['message'=>'El pago ha sido aceptado, el cliente ha sido notifcado vía email']); 
    }

    public function recreate(Request $request){
        $validator=Validator::make($request->all(),[
            'warehouses'=>'required',
            'wid'=>'required|numeric|exists:warehouses,id',
            'pid'=>'required|numeric|exists:products,id',
            'oid'=>'required|numeric|exists:orders,id',
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Petición no válida'],400);
        }
        $product=Products::where(['id'=>$request->pid])->first();
        foreach($request->warehouses as $w){
            $item=ViewWarehouseProducts::where(['active'=>1,'id_warehouse'=>$w['wid'],'id_product'=>$product->id])->first();
            if(!$item){
                $warehouse=Warehouses::where(['active'=>1,'id'=>$w['wid']])->select(['id','name'])->first();
                return response()->json(['message'=>"El producto $product->name no tiene existencias en el almacén $warehouse->name",'product'=>$product],400);
            }
            if($item->stock<$w['quantity']){
                $warehouse=Warehouses::where(['active'=>1,'id'=>$w['wid']])->select(['id','name'])->first();
                return response()->json(['message'=>"El almacén $warehouse->name no tiene suficientes existencias para $product->name"],400);
            }
        }

        $currentProduct=OrderProducts::where(['active'=>1,'id_warehouse'=>$request->wid,'id_product'=>$request->pid,'id_order'=>$request->oid])->first();
        if(!$currentProduct){
            return response()->json(['message'=>'No existe el producto en la órden solicitada'],400);
        }
        $currentProduct->active=0;
        $currentProduct->save();
        
        foreach($request->warehouses as $w){
            $item=OrderProducts::firstOrNew(['id_warehouse'=>$w['wid'],'id_product'=>$request->pid,'id_order'=>$request->oid,'active'=>1]);
            $item->quantity=($item->quantity+$w['quantity']);
            $item->price=$currentProduct->price;
            $item->save();
        }
        $currentProduct->delete();
        return response()->json(['message','Orden recreada']);
    }

    public function validateInventory(Request $request){
        $validator=Validator::make($request->all(),[
            'oid'=>'required|numeric'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Orden no válida'],400);
        }

        $order=Orders::find($request->oid);
        if(!$order){
            return response()->json(['message'=>'No existe la orden de compra'],400);
        }
        $order->id_order_state=$order->id_order_state+1;
        $order->save();
        $orderState=ViewOrders::where(['id'=>$order->id])->select(['order_state','id_order_state'])->first();
        return response()->json(['message'=>'Orden actualizada, pasa a siguiente status','nextState'=>$orderState->id_order_state,'titleState'=>$orderState->order_state]);

    }

    public function deleteOrderReceipt(Request $request){
        $validator=Validator::make($request->all(),[
            'pmid'=>'required|numeric'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Orden no válida','fields'=>$validator->failed()],400);
        }
        $orderLog=OrderPayments::find($request->pmid);
        $filename=$orderLog->receipt;
        $orderLog->payment_status_id=5;
        $orderLog->save();
        $paymentsCount=OrderPayments::where(['order_payment_schedules_id'=>$orderLog->order_payment_schedules_id])->where(function($query)use($orderLog){
            $query->where('payment_status_id','=','2')->orWhere('payment_status_id','=','3');
        })->count();
        if(!$paymentsCount){
            $orderSchedule=OrderPaymentSchedules::find($orderLog->order_payment_schedules_id);
            $orderSchedule->payment_status_id=1;
            $orderSchedule->save();
            Orders::where(['id'=>$orderSchedule->order_id])->update(['order_status_id'=>1]);
        }
        OrderDeletedReceipts::create([
            'id_order'=>$orderLog->order_payment_schedules_id,
            'id_payments_orders_log'=>$orderLog->order_payment_schedules_id,
            'id_administrator'=>Auth::user()->id,
            'filename'=>$filename
        ]);
        /* Move file to Trash folder */
        /* To complete needs to add email notification to order's user */
        return response()->json(['message'=>'Comprobante eliminado']);
    }

    public function uploadImage(Request $request){
        $validator=Validator::make($request->all(),[
            'osid'=>'required|numeric|exists:order_payment_schedules,id'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Orden no válida'],400);
        }
        if(!$request->hasFile(('file'))){
            return response()->json(["message"=>"Debe subir una imagen","e_code"=>"file_not_found"],400);
        }
        $ext=strtolower($request->file('file')->extension());
        if(!in_array($ext,['jpg','png'])){
            return response()->json(["message"=>"El archivo no es válido, solamente se aceptan imágenes (PNG,JPG) y archivos PDF","e_code"=>"file_invalid"],400);
        }
        $filename=Storage::disk('public')->put("receipts/images" , $request->file('file'));
        $filename=explode("/",$filename);
        
        $payment=OrderPayments::create([
            'order_payment_schedules_id'=>$request->osid,
            'paid_amount'=>$request->amount,
            'payment_type_id'=>$request->pmid,
            'receipt'=>$filename[count($filename)-1]
        ]);
        $orderSchedule=OrderPaymentSchedules::find($request->osid);
        $orderSchedule->payment_status_id=2;
        $orderSchedule->save();
        Orders::where(['id'=>$orderSchedule->order_id])->update(['order_status_id'=>2]);
        return response()->json(['message'=>'Comprobrante de pago guardado','payment'=>$payment]);
    }

    public function uploadFile(Request $request){
        $validator=Validator::make($request->all(),[
            'pmid'=>'required|numeric|exists:orders,id'
        ]);
        if($validator->fails()){
            return response()->json(['message'=>'Orden no válida'],400);
        }
        if(!$request->hasFile(('file'))){
            return response()->json(["message"=>"Debe subir una imagen","e_code"=>"file_not_found"],400);
        }
        $ext=strtolower($request->file('file')->extension());
        if(!in_array($ext,['pdf'])){
            return response()->json(["message"=>"El archivo no es válido, solamente se aceptan imágenes (PNG,JPG) y archivos PDF","e_code"=>"file_invalid"],400);
        }
        $filename=Storage::disk('public')->put("receipts/documents" , $request->file('receipt'));
        $filename=explode("/",$filename);
        
        $payment=OrderPayments::create([
            'order_payment_schedules_id'=>$request->osid,
            'paid_amount'=>$request->amount,
            'payment_type_id'=>$request->pmid,
            'receipt'=>$filename[count($filename)-1]
        ]);
        $orderSchedule=OrderPaymentSchedules::find($request->osid);
        $orderSchedule->payment_status_id=2;
        $orderSchedule->save();
        Orders::where(['id'=>$orderSchedule->order_id])->update(['order_status_id'=>2]);
        return response()->json(['message'=>'Comprobrante de pago guardado','payment'=>$payment]);
    }
}
