<?php

namespace App\Http\Controllers;

use App\Models\ProductDatasheet;
use App\Models\ProductImages;
use App\Models\Products;
use App\Models\Categories;
use App\Models\Units;
use App\Models\Brands;
use App\Models\ViewProducts;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Imports\ProductsImport;
use Excel;

class productsController extends Controller
{
    
    public function list(Request $request){
        $type=$request->type?$request->type:'BEST_SELLERS';
        switch($type){
            case 'OFFERS': return $this->bestSellers(4);
            case 'RELATED': return $this->related($request->productId,4);
        }
    }

    public function listProductsBrand(Request $request){
        $cid=$request->brandID;
        $products = ViewProducts::where(['active'=>1])
                                ->where("id_brand",$cid)
                                ->orderBy("name")
                                ->paginate(2);
        //return response($cid);
        return response()->json(['products'=>$products]);
    }

    public function listCategories(Request $request){
        $cid=$request->cid;
        $orden=$request->orden?$request->orden:"asc";
        $minPrice=$request->minPrice?$request->minPrice:0;
        $maxPrice=$request->maxPrice?$request->maxPrice:5000;
        $subOrden=$request->subOrden;
        if($subOrden){
            $products = ViewProducts::where(['active'=>1])
                                ->where("id_departament",$cid)
                                ->where("id_subdepartament",$subOrden)
                                ->whereBetween('net_sale_price',[$minPrice,$maxPrice])
                                ->orderBy("net_sale_price",$orden)->paginate(8);
        } else{
            $products = ViewProducts::where(['active'=>1])
                                ->where("id_departament",$cid)
                                ->whereBetween('net_sale_price',[$minPrice,$maxPrice])
                                ->orderBy("net_sale_price",$orden)->paginate(8);
        }
        //return response($cid);
        return response()->json(['products'=>$products,'orden'=>$cid,'max'=>$maxPrice,'min'=>$minPrice]);
    }

    private function bestSellers($limit){
        $products = ViewProducts::where(['active'=>1])->limit($limit)->get();
        return response()->json(['products'=>$products]);
    }

    private function related($productId,$limit){
        $product=Products::find($productId);
        $products = ViewProducts::where(['active'=>1])->where('id','<>',$productId)->where(function($query)use($product){
            $query->where(['id_departament'=>$product->id_departament])
            ->orWhere(['id_brand'=>$product->id_brand]);
        })->limit($limit)->orderBy('SALES','DESC')->get();
        return response()->json(['products'=>$products]);
    }

    public function details(Request $request){
        
        $product_id=$request->product_id?$request->product_id:0;
        
        $product=ViewProducts::find($product_id);
        $images=ProductImages::where(['active'=>1,'id_product'=>$product_id])->orderBy('main','DESC')->get();
        return response()->json(['product'=>$product,'php_slug'=>Str::slug($product->name),'images'=>$images]);
    }

    public function datasheet(Request $request){
        $datashet=[];
        $productId=$request->productId?$request->productId:0;
        if($productId){
            $datashet=ProductDatasheet::where(['id_product'=>$productId])->get();
        }
        return response()->json(['datasheet'=>$datashet,'pid'=>$request->productId]);
    }

    public function importXLS(Request $request) {
        if(!$request->hasFile('product_import')) {
            return response()->json(['Seleccione el archivo que desea importar', 'e'=>1]);
        }

        $ext=strtolower($request->file('product_import')->extension());
        if(!in_array($ext,['csv','xls','xlsx'])){
            return response()->json(["message"=>"Solo se permite seleccionar un CSV, xls o xlsx","e"=>1]);
        }

        $file = $request->file('product_import');
        Excel::import(new ProductsImport, $file);
        
        return response()->json(['message'=>'Importación de productos completada', 'e'=>0]);
        

    }
}
