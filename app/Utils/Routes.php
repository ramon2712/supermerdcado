<?php

namespace App\Utils;

class Routes{

    public function allRoutes(){

        return [
            'post'=>$this->post_routes(),
            'get'=>$this->get_routes(),
            'imageRoute'=>asset('/').'api/files/images/thumbnails/',
            'receiptImage'=>asset('/').'api/auth/files/receipt/images/',
            'receiptDocument'=>asset('/').'api/auth/files/receipt/files/',
            'imagePayments'=>asset('/').'api/auth/files/receipts/images/',
            'docPayments'=>asset('/').'api/auth/files/receipts/documents/',
        ];

    }

    private function get_routes(){
        return [

        ];
    }

    private function post_routes(){
        return [
            'login'=>'login/attempt',
            'logout'=>'auth/logout',
            'select'=>[
                'menus'=>'auth/select/menus',
                'products'=>'auth/select/products',
                'productsOne'=>'auth/select/productsOne',
                'productOneWithCode'=>'auth/select/productOneWithCode',
                'productImages'=>'auth/select/productImages',
                'productData'=>'auth/select/productData',
                'productPrices'=>'auth/select/productPrices',
                'categories'=>'auth/select/categories',
                'categoriesOne'=>'auth/select/categoriesOne',
                'roles'=>'auth/select/roles',
                'rolesOne'=>'auth/select/rolesOne',
                'adminusers'=>'auth/select/adminusers',
                'adminusersOne'=>'auth/select/adminusersOne',
                'freighters'=>'auth/select/freighters',
                'freightersOne'=>'auth/select/freightersOne',
                'payments'=>'auth/select/payments',
                'paymentsOne'=>'auth/select/paymentsOne',
                'warehouses'=>'auth/select/warehouses',
                'warehousesOne'=>'auth/select/warehousesOne',
                'warehousesWithStock'=>'auth/select/warehousesWithStock',
                'warehousesWithStockOne'=>'auth/select/warehousesWithStockOne',
                'brands'=>'auth/select/brands',
                'brandsOne'=>'auth/select/brandsOne',
                'departaments'=>'auth/select/departaments',
                'departamentsOne'=>'auth/select/departamentsOne',
                'orders'=>'auth/select/orders',
                'ordersOne'=>'auth/select/ordersOne',
                'orderPayment'=>'auth/select/orderPayment',
                'orderPaymentSchedules'=>'auth/select/orderPaymentSchedules',
                'orderInventoryList'=>'auth/select/orderInventoryList',
                'paymentHistory'=>'auth/select/orderPaymentHistory',
                'userAddresses'=>'auth/select/userAddresses',
                //desde aqui todo es de marce XD
                'productDatasheet' => 'auth/select/productDatasheet',//echo por marce
                'productDatasheetOne' => 'auth/select/productDatasheetOne',//echo por marce
                'viewProductSecs' => 'auth/select/viewProductSecs',//echo por marce
                'productSecs' => 'auth/select/productSecs',//echo por marce
                'productSecsOne' => 'auth/select/productSecsOne',//echo por marce
                'secsValues' => 'auth/select/secsValues',//echo por marce
                //hasta aqui todo es de marce XD


                'list'=>[
                    'subcategories'=>'auth/select/list/subcategories',
                    'brands'=>'auth/select/list/brands',
                    'permissions'=>'auth/select/list/permissions',
                    'roles'=>'auth/select/list/roles',
                    'categories'=>'auth/select/list/categories',
                    'orders'=>'auth/select/list/orders',
                    'clients'=>'auth/select/list/clients',
                    'paymentTypes'=>'auth/select/list/paymentTypes',
                    'deliveryTimes'=>'auth/select/list/deliveryTimes',
                    'orderTypes'=>'auth/select/list/orderTypes',
                    'productWithStock'=>'auth/select/list/productWithStock',
                ],
                'utils'=>[
                    'getAddress'=>'auth/select/utils/getAddress'
                ],
            ],
            'insert'=>[
                'product'=>'auth/create/product',
                'products'=>'auth/create/products',
                'categories'=>'auth/create/categories',
                'roles'=>'auth/create/roles',
                'adminuser'=>'auth/create/adminuser',
                'freighters'=>'auth/create/freighters',
                'payments'=>'auth/create/payments',
                'products'=>'auth/create/products',
                'warehouses'=>'auth/create/warehouses',
                'brands'=>'auth/create/brands',
                'departaments'=>'auth/create/departaments',
                'orders'=>'auth/create/orders',
                'userAddress'=>'auth/create/userAddress',
                //desde aqui todo es de marce XD
                'productDatasheet' => 'auth/create/productDatasheet',//echo por marce
                'productSecs' => 'auth/create/productSecs',//echo por marce
                'secsValues' => 'auth/create/secsValues',//echo por marce
                //hasta aqui todo es de marce XD
            ],
            'set'=>[
                'product'=>'auth/set/product',
                'products'=>'auth/set/products',
                'productPrices'=>'auth/set/productPrices',
                'permissions'=>'auth/set/rolesPermissions',
                'categories'=>'auth/set/categories',
                'roles'=>'auth/set/roles',
                'adminuser'=>'auth/set/adminuser',
                'freighters'=>'auth/set/freighters',
                'payments'=>'auth/set/payments',
                'products'=>'auth/set/products',
                'warehouses'=>'auth/set/warehouses',
                'warehouseStock'=>'auth/set/warehouseStock',
                'brands'=>'auth/set/brands',
                'departaments'=>'auth/set/departaments',
                'orders'=>'auth/set/orders',
                'orderAuthorize'=>'auth/set/orderAuthorize',
                'orderRecreate'=>'auth/set/orderRecreate',
                'orderValidateInventory'=>'auth/set/orderValidateInventory',
                'userAddress'=>'auth/set/userAddress',
                //desde aqui todo es de marce XD
                'productDatasheet' => 'auth/set/productDatasheet',//echo por marce
                'productSecs' => 'auth/set/productSecs',//echo por marce
                'secsValues' => 'auth/set/secsValues',//echo por marce
                //hasta aqui todo es de marce XD
            ],
            'upload'=>[
                'productImage'=>'auth/upload/productImage',
                'orderFileReceipt'=>'auth/upload/orderFileReceipt',
                'orderImageReceipt'=>'auth/upload/orderImageReceipt',
            ],
            'delete'=>[
                'productImage'=>'auth/delete/productImage',
                'secValue'=>'auth/delete/secValue',
                'categories'=>'auth/delete/categories',
                'roles'=>'auth/delete/roles',
                'adminuser'=>'auth/delete/adminuser',
                'freighters'=>'auth/delete/freighters',
                'payments'=>'auth/delete/payments',
                'products'=>'auth/delete/products',
                'warehouses'=>'auth/delete/warehouses',
                'brands'=>'auth/delete/brands',
                'departaments'=>'auth/delete/departaments',
                'orders'=>'auth/delete/orders',
                'orderReceipt'=>'auth/delete/orderReceipt',
                //desde aqui todo es de marce XD
                'productDatasheet' => 'auth/delete/productDatasheet',//echo por marce
                'productSecs' => 'auth/delete/productSecs',//echo por marce
                'secsValues' => 'auth/delete/secsValues',//echo por marce
                //hasta aqui todo es de marce XD
            ],
        ];
    }
 
}