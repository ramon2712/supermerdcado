<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class State implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    private $model;

    public function __construct($model)
    {
        $this->model=$model;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $result=$this->model::where(['id'=>$value,'active'=>1])->first();
        
        if(!$result){
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ':attribute no está activo, verifique.';
    }
}
