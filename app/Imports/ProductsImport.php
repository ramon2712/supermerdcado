<?php

namespace App\Imports;

use App\Models\Categories;
use App\Models\Units;
use App\Models\Brands;
use App\Models\Products;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;

class ProductsImport implements ToModel, WithHeadingRow
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $id_category = Categories::where('name', '=', $row['linea'])->value('id');
        $id_brand = Brands::where('name', '=', $row['marca'])->value('id');
        $unit_id = Units::where('abbrev', '=', $row['medida'])->value('id');

        return new Products([
            'sku'           => $row['clave'],
            'name'          => $row['nombre'],
            'id_category'   => $id_category,
            'unit_id'       => $unit_id,
            'sat_code'      => $row['clave_sat'],
            'description'   => $row['descripcion'],
            'upc'           => $row['upc'],
            'id_brand'      => $id_brand,
            'active'        => ($row['estatus'] == 'Activo') ? 1 : 0,
            'weight'        => $row['peso'],
            'large'         => $row['largo'],
            'height'        => $row['altura'],
            'wide'          => $row['ancho'],
            'volume_weight' => $row['peso_volumetrico'],
        ]);
    }

    public function rules(): array {
        return [
            'sku'           => 'required',
            'name'          => 'required',
            'id_category'   => 'required',
            'unit_id'       => 'required',
            'sat_code'      => 'required',
            'description'   => 'required',
            'id_brand'      => 'required',
            'active'        => 'required',
            'weight'        => 'numeric',
            'large'         => 'numeric',
            'height'        => 'numeric',
            'wide'          => 'numeric',
            'volume_weight' => 'numeric',
        ];
    }
}
