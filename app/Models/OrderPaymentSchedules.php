<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderPaymentSchedules extends Model
{
    use HasFactory;

    protected $table="order_payment_schedules";
    protected $fillable=['order_id','date','amount','subtotal','taxes'];

    public $timestamps=false;

}
