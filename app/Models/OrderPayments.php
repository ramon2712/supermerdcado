<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderPayments extends Model
{
    use HasFactory;
    protected $table="order_payments";
    protected $fillable=['order_payment_schedules_id','payment_status_id','payment_type_id','paid_amount','receipt','active'];
}
