<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewOrders extends Model
{
    use HasFactory;
    protected $table = 'ViewOrders';
    protected $fillable=[
        'id',
        'user_id',
        'administrator_id',
        'order_status_id',
        'order_delivery_periods_id',
        'order_types_id',
        'user_address_id',
        'amount',
        'subtotal',
        'taxes',
        'net_taxes',
        'products_count',
        'created_at',
        'name_status'
    ];
}
