<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewAdministrators extends Model
{
    use HasFactory;

    protected $table='ViewAdministrators';
    protected $fillable=['username','name','lastname','personal_id','active','department_name','role_name','searchText'];
}
