<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    use HasFactory;
    
    protected $table="product_images";
    protected $fillable=["filename","main","active","id_product"];
}
