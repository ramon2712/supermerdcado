<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductPrices extends Model
{
    use HasFactory;

    protected $table="product_prices";
    protected $fillable = ['id_product','sale_price','purchase_price','profit_margin'];
}
