<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminDepartaments extends Model
{
    use HasFactory;

    protected $table='admin_departaments';
    protected $fillable=['name','description','active'];

    public $timestamps = false;
}
