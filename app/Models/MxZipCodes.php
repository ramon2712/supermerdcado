<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MxZipCodes extends Model
{
    use HasFactory;

    protected $table='mx_zip_codes';
}
