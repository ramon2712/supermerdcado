<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewPaymentsOrderLog extends Model
{
    use HasFactory;
    protected $table ="ViewPaymentsOrderLog";
}
