<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewSubmenuPermissions extends Model
{
    use HasFactory;
    protected $table="viewSubmenuPermissions";
    protected $fillable=['id','id_submenu','id_role','submenu_name','role_name'];
}
