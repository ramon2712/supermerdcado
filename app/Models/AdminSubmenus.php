<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminSubmenus extends Model
{
    use HasFactory;
    protected $table="admin_submenus";
    protected $fillable=['name','href','component','id_admin_menu','active'];
}
