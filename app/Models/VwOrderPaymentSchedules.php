<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VwOrderPaymentSchedules extends Model
{
    use HasFactory;
    protected $table="vw_order_payment_schedules";
}
