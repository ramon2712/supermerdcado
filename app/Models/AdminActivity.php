<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminActivity extends Model
{
    use HasFactory;
    protected $table='adminActivity';
    protected $fillable=['id_administrator','component','admin_ip','city','country'];
}
