<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddressShippings extends Model
{
    use HasFactory;
    protected $table = 'user_addresses';
    protected $fillable = ['user_id','email', 'phone', 'street', 'colonia', 'mx_zip_code_cp', 'full_name', 
    'description', 'active', 'main'];
}