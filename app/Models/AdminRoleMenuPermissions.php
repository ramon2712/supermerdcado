<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminRoleMenuPermissions extends Model
{
    use HasFactory;

    protected $table="admin_role_menu_permissions";

    protected $fillable=['id_menu','id_role'];

    public $timestamps = false;
}
