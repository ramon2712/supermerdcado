<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewMenuPermissions extends Model
{
    use HasFactory;
    protected $table="viewMenuPermissions";
    protected $fillable=['id','id_menu','id_role','menu_name','role_name'];
}
