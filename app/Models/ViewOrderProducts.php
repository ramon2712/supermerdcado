<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewOrderProducts extends Model
{
    use HasFactory;
    protected $table='ViewOrderProducts';
    protected $fillable= [
        'id',
        'product_id',
        'order_id',
        'quantity',
        'price',
        'active',
        'amount',
        'created_at',
        'product_name',
        'sku',
        'id_user'
    ];
}
