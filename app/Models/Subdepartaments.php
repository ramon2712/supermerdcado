<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subdepartaments extends Model
{
    use HasFactory;
    protected $table='subdepartaments';
    protected $fillable=['id','name','description','id_departament','imagen'];
}
