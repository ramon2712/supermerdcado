<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VwOrderPayments extends Model
{
    use HasFactory;
    protected $table="vw_order_payments";
}
