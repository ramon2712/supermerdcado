<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class deliveryTimes extends Model
{
    use HasFactory;
    
    protected $table = "delivery_time";
    protected $fillable = [
        'time','active','created_at','updated_at'
    ];
}
