<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SecValues extends Model
{
    use HasFactory;

    protected $table="sec_values";
    protected $fillable=['name','value','id_sec','active'];
}
