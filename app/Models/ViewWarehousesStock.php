<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewWarehousesStock extends Model
{
    use HasFactory;
    protected  $table="ViewWarehousesStock";
}
