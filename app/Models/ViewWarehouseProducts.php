<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewWarehouseProducts extends Model
{
    use HasFactory;
    protected $table='view_warehouse_products';
}
