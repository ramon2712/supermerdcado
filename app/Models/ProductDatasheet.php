<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductDatasheet extends Model
{
    use HasFactory;
    protected $table="product_datasheets";
    protected $fillable=['name','value','id_product','active'];
}
