<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Freighters extends Model
{
    use HasFactory;
    protected $table='freighters';
    protected $fillable=['name','phone','address','main','active'];

    public $timestamps=false;
}
