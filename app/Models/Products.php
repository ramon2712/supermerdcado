<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;

    protected $table ="products";
    protected $fillable=["name","sku","description", "unit_id", "upc", "id_category", "id_brand", "weight", "large", "height", "wide", "volume_weight", "sat_code", "active"];

}
