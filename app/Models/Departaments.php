<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Departaments extends Model
{
    use HasFactory;

    protected $table='departaments';
    protected $fillable=['id','name','description','image'];

    public $timestamps=false;
}
