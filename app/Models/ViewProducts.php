<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewProducts extends Model
{
    use HasFactory;

    protected $table="viewProductsNew";
    protected $fillable=['id','name','sku','description','id_departament','id_subdepartament','subdepartament_name','id_brand','departament_name','brand_name','searchText','sales','net_sale_price'];

    public function scopeActive($query) {
        return $query->where('active', '=', 1);
    }

    public function scopeSearch($query, $searchText) {
        if($searchText) {
            return $query->where('name', 'like', "%$searchText%");
        }
    }

    public function scopeBrand($query, $id_brand) {
        if($id_brand) {
            return $query->where('id_brand', '=', "$id_brand");
        }
    }

    public function scopeDepartament($query, $id_category) {
        if($id_category) {
            return $query->where('id_departament', '=', "$id_category");
        }
    }

    public function scopeMin($query, $min_price) {
        if($min_price) {
            return $query->where('net_sale_price', '>=', "$min_price");
        }
    }

    public function scopeMax($query, $max_price) {
        if($max_price) {
            return $query->where('net_sale_price', '<=', "$max_price");
        }
    }

    public function scopeCookie($query, array $cookie) {
        if($cookie) {
            return $query->whereNotIn('id', $cookie);
        }
    }
}
