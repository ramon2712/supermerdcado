<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDeletedReceipts extends Model
{
    use HasFactory;
    protected $table='order_deleted_receipts';
    protected $fillable=['id_order','id_user','filename'];
}
