<?php

namespace App\Models\Web;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['name','description','id_category', 'active'];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function ScopeSearchProduct($query, $name){
        return $query->where('name','LIKE', $name);
    }

    public function ScopeProduct($query, $id){
        return $query->where('id','LIKE', $id);
    }
}
