<?php

namespace App\Models\Web;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    use HasFactory;
    protected $table = 'cart_products';
    protected $fillable = ['id_product', 'quantity', 'id_user', 'status'];
    //protected $hidden = ['created_at', 'updated_at', 'status'];

    public function cart(){
        return $this->belongTo(Product::Class);
    }
}
