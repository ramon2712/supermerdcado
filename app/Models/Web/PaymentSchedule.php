<?php

namespace App\Models\Web;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentSchedule extends Model
{
    use HasFactory;
    protected $table = 'order_payment_schedules';
    protected $fillable = ['order_id', 'payment_status_id', 'payment_types_id', 'date', 'amount', 'subtotal', 'taxes', ];
}
