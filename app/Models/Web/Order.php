<?php

namespace App\Models\Web;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $table = 'orders';
    protected $fillable = ['id_user', 'administrator_id', 'order_status_id', 'order_delivery_periods_id', 'order_types_id', 'user_address_id',
    'amount', 'subtotal', 'taxes'];
    //protected $hidden = ['created_at', 'updated_at', 'status'];
}
