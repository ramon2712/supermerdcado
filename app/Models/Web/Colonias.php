<?php

namespace App\Models\Web;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Colonias extends Model
{
    use HasFactory;
    protected $table = 'mx_zip_codes';
    protected $fillable = ['cp', 'colonia', 'municipio', 'estado'];
}
