<?php

namespace App\Models\Web;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddressShipping extends Model
{
    use HasFactory;
    protected $table = 'address_shipping';
    protected $fillable = ['name','lastname', 'address', 'description', 'city', 'state', 'zip', 'phone'];
}