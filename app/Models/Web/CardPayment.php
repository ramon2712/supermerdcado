<?php

namespace App\Models\Web;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CardPayment extends Model
{
    use HasFactory;
    protected $table = 'cards_payment';
    protected $fillable = ['digits', 'payments_id', 'id_payments_orders', 'paid_at'];
}
