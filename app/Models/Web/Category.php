<?php

namespace App\Models\Web;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $table = 'categories';
    protected $fillable = ['name'];
    protected $hidden = ['created_at','updated_at'];

    public function products(){
        return $this->hasMany(Product::class,'id_category');
    }

    public function scopeSearchCategory($query, $name){
        return $query->where('name', '=', $name);
    }
}
