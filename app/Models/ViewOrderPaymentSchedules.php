<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewOrderPaymentSchedules extends Model
{
    use HasFactory;
    protected $table = 'vw_order_payment_schedules';
    protected $fillable  = [
        'id',
        'order_id',
        'payment_status_id',
        'payment_types_id',
        'date',
        'amount',
        'subtotal',
        'taxes',
        'created_at',
        'update_at',
        'payment_status_name',
        'payment_type_name'
    ];

}
