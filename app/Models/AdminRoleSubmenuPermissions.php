<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminRoleSubmenuPermissions extends Model
{
    use HasFactory;

    protected $table="admin_role_submenu_permissions";

    protected $fillable=['id_submenu','id_role'];

    public $timestamps = false;
}
